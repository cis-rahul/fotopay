<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reorderjob extends Model
{
    protected $table = "fp_reorderjobs";

    public function scopeSearchByKeyword($query, $keyword)
    {
        if ($keyword!='') {
            $query->where(function ($query) use ($keyword) {
                $query->where("job_code", "LIKE","%$keyword%")
                ->orWhere("fp_package_profile.package_profile_name", "LIKE","%$keyword%");
            });
        }
        return $query;
    }
}


