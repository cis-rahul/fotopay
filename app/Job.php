<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $table = "fp_jobs";

    public function scopeSearchByKeyword($query, $keyword)
    {
        if ($keyword!='') {
            $query->where(function ($query) use ($keyword) {
                $query->where("job_code", "LIKE","%$keyword%")
                ->orWhere("fp_schools.name", "LIKE","%$keyword%");
            });
        }
        return $query;
    }
}


