<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = "fp_message";

       public function scopeSearchByKeyword($query, $keyword)
    {
        if ($keyword!='') {
            $query->where(function ($query) use ($keyword) {
                $query->where("message_title", "LIKE","%$keyword%")
                ->orWhere("message_content", "LIKE","%$keyword%");
            });
        }
        return $query;
    }
}
