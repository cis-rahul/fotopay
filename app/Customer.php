<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = "users";

       public function scopeSearchByKeyword($query, $keyword)
    {
        if ($keyword!='') {
            $query->where(function ($query) use ($keyword) {
                $query->where("firstname", "LIKE","%$keyword%")
                ->orWhere("lastname", "LIKE","%$keyword%")
                ->orWhere("email", "LIKE","%$keyword%")
                ->orWhere("contact", "LIKE","%$keyword%");
            });
        }
        return $query;
    }
}
