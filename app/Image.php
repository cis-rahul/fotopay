<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = "fp_imagetype";

     public function scopeSearchByKeyword($query, $keyword)
    {
        if ($keyword!='') {
            $query->where(function ($query) use ($keyword) {
                $query->orWhere("image_title", "LIKE","%$keyword%");
                
            });
        }
        return $query;
    }
}
