<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Packageprofile extends Model
{
    protected $table = "fp_package_profile";

    public function scopeSearchByKeyword($query, $keyword)
    {
        if ($keyword!='') {
            $query->where(function ($query) use ($keyword) {
                $query->where("package_profile_name", "LIKE","%$keyword%");                
            });
        }
        return $query;
    }
}
