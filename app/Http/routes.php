<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/*
 * Home Page
 * Created by Saurabh Kadam
 * Created Date 13-Oct-2016
 */

Route::get('/', ['uses'=>'HomeController@index', 'as' => 'home']);
Route::get('prepay', ['uses'=>'PrepayController@index', 'as' => 'prepay']);
Route::post('prepaystore', ['uses'=>'PrepayController@store', 'as' => 'prepay.store']);
Route::post('prepaysignin', ['uses'=>'PrepayController@signin', 'as' => 'prepay.signin']);
Route::get('prepaydashboard', ['uses'=>'PrepayController@dashboard', 'as' => 'prepay.dashboard']);
Route::post('prepayjob', ['uses'=>'PrepayController@checkjob_code', 'as' => 'prepay.job']);
//Route::get('prepayphoto', ['uses'=>'PrepayController@dashboard', 'as' => 'prepay.photo']);


Route::group(["namespace" => 'Admin', "prefix" => "admin"], function() {
    Route::get('/', ['uses'=>'SiteController@index', 'as' => 'home']);
    Route::get('signin', ['uses'=>'SiteController@index', 'as' => 'admin.signin']);
    Route::post('signin', ['uses'=>'SiteController@signin', 'as' => 'admin.signin']);
    Route::get('logout', ['uses'=>'SiteController@logout', 'as' => 'admin.logout']);    
    Route::get('admin',['uses'=>'SiteController@adminDashboard', 'as' => 'admin']);
    Route::get('dashboard', ['uses'=>'SiteController@dashboard', 'as' => 'admin.dashboard']);    

    /*
     * School Management
     * Created by Rahul Gupta
     * Created Date 01-Oct-2016
     */

    route::resource('school', 'SchoolController', ['except' => ['destroy']]);
    route::get('school/delete/{school}', ['uses'=>'SchoolController@destroy' , 'as'=>'admin.school.destroy']);

    /*
     * Grade Management
     * Created by Rahul Gupta
     * Created Date 04-Oct-2016
     */
    route::resource('student', 'StudentController', ['except' => ['destroy']]);
    route::get('student/delete/{student}', ['uses'=>'StudentController@destroy' , 'as'=>'admin.student.destroy']);
    route::get('student/studentlist/{grade_id}', ['uses'=>'StudentController@studentListByGradeID' , 'as'=>'admin.student.studentlist']);
    /*
     * Prepay Job Management
     * Created by Rahul Gupta
     * Created Date 06-Oct-2016
     */
    route::resource('prepayjob', 'PrepayjobController', ['except' => ['destroy']]);
    route::get('prepayjob/delete/{job}', ['uses'=>'PrepayjobController@destroy' , 'as'=>'admin.prepayjob.destroy']);
    
    
    /*
     * Re-Order Job Management
     * Created by Rahul Gupta
     * Created Date 06-Oct-2016
     */
    route::resource('reorderjob', 'ReorderjobController', ['except' => ['destroy']]);
    route::get('reorderjob/delete/{job}', ['uses'=>'ReorderjobController@destroy' , 'as'=>'admin.reorderjob.destroy']);
    
    /*
     * Miscellaneous Job Management
     * Created by Rahul Gupta
     * Created Date 06-Oct-2016
     */
    route::resource('miscellaneousjob', 'MiscellaneousjobController', ['except' => ['destroy']]);
    route::get('miscellaneousjob/delete/{job}', ['uses'=>'miscellaneousjob@destroy' , 'as'=>'admin.miscellaneousjob.destroy']);
    
    /*
     * Job Management
     * Created by Rahul Gupta
     * Created Date 06-Oct-2016
     */
    route::resource('job', 'JobController', ['except' => ['destroy']]);
    route::get('job/delete/{job}', ['uses'=>'JobController@destroy' , 'as'=>'admin.job.destroy']);
    

    /*
     * Grade Management
     * Created by Rahul Gupta
     * Created Date 04-Oct-2016
     */
    route::resource('grade', 'GradeController', ['except' => ['destroy']]);
    route::get('grade/delete/{grade}', ['uses'=>'GradeController@destroy' , 'as'=>'admin.grade.destroy']);    
    route::get('grade/gradelist/{school_id}', ['uses'=>'GradeController@gradeListBySchoolID' , 'as'=>'admin.grade.gradelist']);

    /*
     * School Management
     * Created by Saurabh Kadam
     * Created Date 01-Oct-2016
     */
    Route::resource('message','MessageController');
    
    /*
     * Package Management
     * Created by Saurabh Kadam
     * Created Date 03-Oct-2016
     */
    Route::resource('package','PackageController');
    Route::resource('packageprofile','PackageprofileController');

    /*
     * Image Management
     * Created by Saurabh Kadam
     * Created Date 04-Oct-2016
     */
    Route::resource('image','ImageController');    

    /*
     * Item Management
     * Created by Saurabh Kadam
     * Created Date 04-Oct-2016
     */
    Route::resource('item','ItemController');
    /*
     * Contact Management
     * Created by Saurabh Kadam
     * Created Date 05-Oct-2016
     */
    Route::resource('contact','ContactController');
    /*
     * Job Management
     * Created by Rahul Gupta
     * Created Date 07-Oct-2016
     */
    route::resource('uploadImage', 'UploadImageController', ['except' => ['destroy']]);
    route::get('uploadImage/delete/{job}', ['uses'=>'UploadImageController@destroy' , 'as'=>'admin.uploadImage.destroy']);
    route::post('uploadImage/upload', ['uses'=>'UploadImageController@upload' , 'as'=>'admin.uploadImage.upload']);
    route::post('uploadImage/remove', ['uses'=>'UploadImageController@removeImage' , 'as'=>'admin.uploadImage.remove']);
    route::get('uploadImage/getImages/{uploadImage}', ['uses'=>'UploadImageController@getImage' , 'as'=>'admin.uploadImage.get']);


     /*
     * My Profile
     * Created by Saurabh Kadam
     * Created Date 20-Oct-2016
     */
    Route::resource('profile','ProfileController'); 
    Route::post('profilepassword', ['uses'=>'ProfileController@password', 'as' => 'admin.profile.password']);  
    //route::post('ProfileController/password', ['uses'=>'ProfileController@password' , 'as'=>'admin.profile.password']);



    /*
     * My Profile
     * Created by Saurabh Kadam
     * Created Date 21-Oct-2016
     */
    Route::resource('customer','CustomerController'); 
    //Route::post('profilepassword', ['uses'=>'ProfileController@password', 'as' => 'admin.profile.password']);  
});