<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\GradeRequest;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use App\Grade;
use App\School;
use View;

class GradeController extends MainController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {                
        parent::__construct();
    }
    public function index(Request $request)
    {
        $keyword = $request->input('search_keyword');
        $this->data['grades'] = Grade::SearchByKeyword($keyword)->Select('fp_grades.*', 'fp_schools.name')->Join('fp_schools', 'fp_grades.school_id', '=', 'fp_schools.id')->orderBy('id','desc')->paginate(10);
        return View::make('admin.grade.list', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {        
        $this->data['schools'] = School::Select('*')->where('status','1')->get();
        return View::make('admin.grade.create',$this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GradeRequest $request)
    {
        $objGrade = new Grade();
        $objGrade->school_id = $request->input('school_name');
        $objGrade->title = $request->input('title');
        if($objGrade->save())
            return Redirect::route('admin.grade.index')->with(array('flash_message'=>'Grade successfully added!', 'flash_action'=>'success'));
        else
            return Redirect::route('admin.grade.crate')->with(array('flash_message'=>'Grade not added successfully!', 'flash_action'=>'danger'));
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['schools'] = School::Select('*')->where('status','1')->get();
        $this->data['grade'] = Grade::find($id);
        return View::make('admin.grade.create', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(GradeRequest $request, $id)
    {
        $objGrade = Grade::find($id);
        $objGrade->school_id = $request->input('school_name');
        $objGrade->title = $request->input('title');
        $objGrade->status = $request->input('status');
        $objGrade->updated_at = Date('Y-m-d H:i:s');
        if($objGrade->save())
            return Redirect::route('admin.grade.index')->with(array('flash_message'=>'Grade successfully added!', 'flash_action'=>'success'));
        else
            return Redirect::route('admin.grade.edit', $id)->with(array('flash_message'=>'Grade not added successfully!', 'flash_action'=>'danger'));
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    /**     
     *  This method return the grade list of school
     *  Created By Rahul Gupta
     *  Created Date : 05-Oct-2016
     */
    public function gradeListBySchoolID(Request $request, $id)
    {
        if($request->ajax()){
            $gradeList = Grade::Where('school_id',$id)->get()->toArray();
            $status = false;
            if(!empty($gradeList)){                
                $status = true;
            }
            echo json_encode(array('status'=>$status,'data'=>$gradeList));
        }
    }
}
