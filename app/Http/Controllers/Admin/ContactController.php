<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\ContactRequest;
use Illuminate\Support\Facades\Redirect;
use App\Contact;
use View;
use Auth;
use Pagination;


class ContactController extends MainController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {                
        parent::__construct();
    }
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['contact_data'] = Contact::Select('*')->paginate(10);        
        return View::make('admin.contact.create',$this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContactRequest $request)
    {

        $id = '1';
        $contact_data = Contact::find($id);       
        //$contact_data = new Contact();       
        $contact_data ->address    = $request->input('contact_physical_address1');
        $contact_data ->address_two    = $request->input('contact_physical_address2');
        $contact_data ->address_three    = $request->input('contact_physical_address3');
        $contact_data ->suburb    = $request->input('contact_physical_suburb');
        $contact_data ->post_code    = $request->input('contact_physical_postcode');
        $contact_data ->phone_no    = $request->input('contact_physical_phone');
        $contact_data ->email    = $request->input('contact_physical_email');
        $contact_data ->postal_address    = $request->input('contact_postal_address1');
        $contact_data ->postal_address_two    = $request->input('contact_postal_address2');
        $contact_data ->postal_address_three    = $request->input('contact_postal_address3');
        $contact_data ->postal_suburb    = $request->input('contact_postal_suburb');
        $contact_data ->postal_postcode    = $request->input('contact_postal_postcode');
        $contact_data ->postal_phone_number    = $request->input('contact_postal_phonenumber');
        $contact_data ->postal_email    = $request->input('contact_postal_email');

        if($contact_data->save())
            return Redirect::route('admin.contact.create')->with(array('flash_message'=>'Contact successfully Updated', 'flash_action'=>'success'));
        else
            return Redirect::route('admin.contact.create')->with(array('flash_message'=>'Contact not updated successfully!', 'flash_action'=>'danger'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ContactRequest $request)
    {
        die("hii");
        $contact_data = new Contact();       
        $contact_data ->image_title    = $request->input('image_type');
        if($contact_data->save())
            return Redirect::route('admin.contact.store')->with(array('flash_message'=>'Image successfully added', 'flash_action'=>'success'));
        else
            return Redirect::route('admin.contact.create')->with(array('flash_message'=>'Image not saved successfully!', 'flash_action'=>'danger'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
