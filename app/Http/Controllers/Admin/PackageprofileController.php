<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\PackageprofileRequest;
use Illuminate\Support\Facades\Redirect;
use App\Packageprofile;
use App\Package;
use File;
use Validator;
use View;
use Auth;
use Input;

class PackageprofileController extends MainController {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $keyword = $request->input('search_keyword');
        $this->data['packageprofile_data'] = Packageprofile::SearchByKeyword($keyword)->Select('id', 'package_profile_name', 'image')->orderBy('id', 'DESC')->paginate(10);
        return View::make('admin.packageprofile.list', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return View::make('admin.packageprofile.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PackageprofileRequest $request) {
        $packageprofile_data = new Packageprofile();
        $packageprofile_data->package_profile_name = $request->input('packageprofile_name');
        $path = base_path() . '/public/uploads/demo';
        $fileName = "";
        if (!empty(Input::file('packageprofile_image')) && Input::file('packageprofile_image')->getError() == 0)
            $fileName = $this->fileUpload(Input::file('packageprofile_image'), $path);
        $packageprofile_data->image = $fileName;
        if ($packageprofile_data->save()) {
            if (!empty($request->input('package'))) {
                $i = 1;
                foreach ($request->input('package') as $objPackage) {
                    $rules = array('package_name' => 'required', 'package_price' => 'required|integer');
                    $file = array('package_name' => $objPackage['package_name'], 'package_price' => $objPackage['package_price']);
                    $validator = Validator::make($file, $rules);
                    if (!$validator->fails()) {
                        $package_data = new Package();
                        $package_data->profile_id = $packageprofile_data->id;
                        $package_data->name = $objPackage['package_name'];
                        $package_data->price = $objPackage['package_price'];
                        $package_data->description = $objPackage['package_desc'];
                        $package_data->post_handling = $objPackage['ph'];
                        $package_data->image = $this->fileUpload(Input::file('package_image_' . $i), $path);
                        $package_data->save();
                    }
                    $i++;
                }
            }
            return Redirect::route('admin.packageprofile.store')->with(array('flash_message' => 'Packageprofile successfully added', 'flash_action' => 'success'));
        } else {
            return Redirect::route('admin.packageprofile.create')->with(array('flash_message' => 'Packageprofile not saved successfully!', 'flash_action' => 'danger'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $packageprofile = Packageprofile::find($id);
        $packages = Package::where('profile_id', $id)->get();
        $data = array(
            'packageprofile' => $packageprofile,
            'packages' => $packages,
            'id' => $id
        );
        return View::make('admin.packageprofile.create')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PackageprofileRequest $request) {
        $id = $request->input('id');
        $packageprofile_data = Packageprofile::find($id);
        $packageprofile_data->package_profile_name = $request->input('packageprofile_name');
        $path = base_path() . '/public/uploads/demo';
        if (!empty(Input::file('packageprofile_image')) && Input::file('packageprofile_image')->getError() == 0) {
            $fileName = $this->fileUpload(Input::file('packageprofile_image'), $path, $packageprofile_data->image);
        }
        if ($packageprofile_data->save()) {
            if (!empty($request->input('package'))) {
                $i = 1;
                foreach ($request->input('package') as $objPackage) {
                    $rules = array('package_name' => 'required', 'package_price' => 'required|integer');
                    $file = array('package_name' => $objPackage['package_name'], 'package_price' => $objPackage['package_price']);
                    $validator = Validator::make($file, $rules);
                    if (!$validator->fails()) {
                        if (!empty($objPackage['id'])) {
                            $package_data = Package::find($objPackage['id']);                            
                            if (!empty(Input::file('package_image_' . $i)) && Input::file('package_image_' . $i)->getError() == 0) {
                                $fileName = $this->fileUpload(Input::file('package_image_' . $i),$path, $package_data->image);
                            }                            
                        } else {
                            $package_data = new Package();
                            $fileName = "";
                            if (!empty(Input::file('package_image_' . $i)) && Input::file('package_image_' . $i)->getError() == 0) {
                                $fileName = $this->fileUpload(Input::file('package_image_' . $i), $path);
                            }
                            $package_data->image = $fileName;
                        }
                        $package_data->profile_id = $packageprofile_data->id;
                        $package_data->name = $objPackage['package_name'];
                        $package_data->price = $objPackage['package_price'];
                        $package_data->description = $objPackage['package_desc'];
                        $package_data->post_handling = $objPackage['ph'];
                        $package_data->save();
                    }
                    $i++;
                }
            }
            return Redirect::route('admin.packageprofile.index')->with(array('flash_message' => 'Package successfully Updated!', 'flash_action' => 'success'));
        } else {
            return Redirect::route('admin.packageprofile.edit', $id)->with(array('flash_message' => 'Package not updated successfully!', 'flash_action' => 'danger'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        die("hiii");
    }

}
