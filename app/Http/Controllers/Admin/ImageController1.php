<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\ImageRequest;
use Illuminate\Support\Facades\Redirect;
use App\Image;
use View;
use Auth;
use Input;

class ImageController extends MainController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {                
        parent::__construct();
    }
    public function index()
    {
        $this->data['image_data'] = Image::Select('id','image_title','status','created_at')->orderBy('id', 'DESC')->paginate(10);        
        return View::make('admin.image.list',$this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
            return View::make('admin.image.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ImageRequest $request)
    {
        $image_data = new Image();       
        $image_data ->image_title    = $request->input('image_type');
        if($image_data->save())
            return Redirect::route('admin.image.store')->with(array('flash_message'=>'Image successfully added', 'flash_action'=>'success'));
        else
            return Redirect::route('admin.image.create')->with(array('flash_message'=>'Image not saved successfully!', 'flash_action'=>'danger'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $image = Image::find($id);
        $data = array(
                      'image'  => $image,
                       'id'=>$id
                );
        return View::make('admin.image.create')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ImageRequest $request)
    {
        $id = $request->input('id');
        $image_data = Image::find($id);       
        $image_data ->image_title    = $request->input('image_type');
        $image_data ->status    = $request->input('status');
        if($image_data->save())
            return Redirect::route('admin.image.index')->with(array('flash_message'=>'Image successfully updated!', 'flash_action'=>'success'));
        else
            return Redirect::route('admin.image.edit', $id)->with(array('flash_message'=>'Image not updated successfully!', 'flash_action'=>'danger'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }        
}
