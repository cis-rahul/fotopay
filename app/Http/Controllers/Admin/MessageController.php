<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\Message;
use App\Http\Requests\MessageRequest;
use Illuminate\Support\Facades\Redirect;
use View;
use Auth;
use Pagination;

class MessageController extends MainController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct() {                
        parent::__construct();
    }
    public function index(Request $request)
    {
        $keyword = $request->input('search_keyword');
        $this->data['message_data'] = Message::SearchByKeyword($keyword)->Select('id','message_title','message_content')->paginate(10);        
        
        //print_r($this->data['message_data']);die;
        return View::make('admin.messagebox.list', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function create() {        
        if (Auth::check()) {             
            return View::make('admin.messagebox.create');
        } else {
            return Redirect::route('admin.signin');
        }
    }

    public function message_list()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MessageRequest $request)
    {
        //
        $message_data = new Message();       
        $message_data ->message_title    = $request->input('message_title');
        $message_data ->message_content   = $request->input('message_content');                    
        if($message_data->save())
            return Redirect::route('admin.message.store')->with(array('flash_message'=>'Message successfully added', 'flash_action'=>'success'));
        else
            return Redirect::route('admin.message.create')->with(array('flash_message'=>'Message not saved successfully!', 'flash_action'=>'danger'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //$this->data['id'] = $id;
        $message = Message::find($id);
         $data = array(
                      'message'  => $message,
                       'id'=>$id
                );
        

        return View::make('admin.messagebox.create')->with($data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MessageRequest $request)
    {
        $id = $request->input('id');
        $message_data = Message::find($id);       
        $message_data ->message_title    = $request->input('message_title');
        $message_data ->message_content   = $request->input('message_content');                    
        if($message_data->save())
            return Redirect::route('admin.message.index')->with(array('flash_message'=>'Message successfully updated!', 'flash_action'=>'success'));
        else
            return Redirect::route('admin.message.edit', $id)->with(array('flash_message'=>'Message not updated successfully!', 'flash_action'=>'danger'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $message = Message::find($id);

        $message->delete();

        //Session::flash('flash_message', 'Task successfully deleted!');

        return Redirect::route('admin.message.index', $id)->with(array('flash_message'=>'Message not saved successfully!', 'flash_action'=>'danger'));
    }
}
