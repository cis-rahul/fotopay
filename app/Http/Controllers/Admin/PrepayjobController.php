<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\PrepayjobRequest;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use App\Prepayjob;
use App\School;
use App\Grade;
use App\Packageprofile;
use View;

class PrepayjobController extends MainController
{    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        parent::__construct();
    }
    
    public function index(Request $request)
    {
        $keyword = $request->input('search_keyword');
        $this->data['prepayjobs'] = Prepayjob::SearchByKeyword($keyword)->Select('fp_package_profile.package_profile_name','fp_prepayjobs.*')->Join('fp_package_profile', 'fp_prepayjobs.packageprofile_id', '=', 'fp_package_profile.id')->orderBy('id', 'desc')->paginate(10);
        return View::make('admin.prepayjob.list', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$this->data['schools'] = School::Select('*')->where('status','1')->get();
        $this->data['packageprofile'] = Packageprofile::Select('*')->where('status','1')->get();
        return View::make('admin.prepayjob.create',$this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PrepayjobRequest $request)
    {
        $objPrepayjob = new Prepayjob();
        $objPrepayjob->packageprofile_id = $request->input('packageprofile_id');        
        $objPrepayjob->job_name = $request->input('job_name');
        $objPrepayjob->job_date = $request->input('job_date');
        $objPrepayjob->finished_date = $request->input('finished_date');
        $objPrepayjob->family_discount = $request->input('family_discount');
        
        $objPrepayjob->created_at = date('Y-m-d H:i:s');
        if($objPrepayjob->save()){
            if(!empty($request->input('grade'))){
                foreach($request->input('grade') as $objGrd){
                    $objGrade = new  Grade();
                    $objGrade->title = strtolower($objGrd);
                    $objGrade->prepayjob_id = $objPrepayjob->id;
                    $objGrade->created_at = date('Y-m-d H:i:s');                    
                    $objGrade->save();
                }
            }
            return Redirect::route('admin.prepayjob.index')->with(array('flash_message'=>'Prepayjob successfully added!', 'flash_action'=>'success'));
        }else{
            return Redirect::route('admin.job.crate')->with(array('flash_message'=>'Prepayjob not added successfully!', 'flash_action'=>'danger'));
        }    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['packageprofile'] = Packageprofile::Select('*')->where('status','1')->get();
        $this->data['grades'] = Grade::Select('title','id')->where(array('status'=>'1','prepayjob_id'=>$id))->get();        
        $this->data['prepayjob'] = Prepayjob::find($id);
        return View::make('admin.prepayjob.create', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PrepayjobRequest $request, $id)
    {
        $objPrepayjob = Prepayjob::find($id);
        $objPrepayjob->packageprofile_id = $request->input('packageprofile_id');
        $objPrepayjob->job_code = $request->input('job_code');
        $objPrepayjob->job_name = $request->input('job_name');
        $objPrepayjob->job_date = $request->input('job_date');
        $objPrepayjob->finished_date = $request->input('finished_date');
        $objPrepayjob->family_discount = $request->input('family_discount');        
        $objPrepayjob->created_at = date('Y-m-d H:i:s');
        if($objPrepayjob->save()){
            Grade::where('prepayjob_id', $id)->delete();
            if(!empty($request->input('grade'))){
                foreach($request->input('grade') as $objGrd){
                    $objGrade = new  Grade();
                    $objGrade->title = strtolower($objGrd);
                    $objGrade->prepayjob_id = $objPrepayjob->id;
                    $objGrade->created_at = date('Y-m-d H:i:s');                    
                    $objGrade->save();
                }
            }
            return Redirect::route('admin.prepayjob.index')->with(array('flash_message'=>'Prepay job successfully added!', 'flash_action'=>'success'));
        }else{
            return Redirect::route('admin.prepayjob.edit', $id)->with(array('flash_message'=>'Prepay job not added successfully!', 'flash_action'=>'danger'));
        }    
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        echo $id; die;
    }    
}
