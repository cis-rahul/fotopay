<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\ProfileRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use App\Profile;
use View;
use Input;

class ProfileController extends MainController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct() {
        parent::__construct();
    }

    public function index()
    {
        $this->data['profile'] = Profile::Select('*')->where('role', 0)->first();
        return View::make('admin.profile.list',$this->data);
    }



     public function password(ProfileRequest $request)
    {
        $profileinfo = Profile::Select('*')->where('role', 0)->first();
        if(!empty($profileinfo)){
        $profile_data = $profileinfo;       

        $profile_data->password   = bcrypt($request->input('password'));
        if($profile_data->save())
            return Redirect::route('admin.profile.index')->with(array('flash_message'=>'Password successfully changed', 'flash_action'=>'success'));
        else
            return Redirect::route('admin.profile.index')->with(array('flash_message'=>'Please enter password!!', 'flash_action'=>'danger'));
        }else
            return Redirect::route('admin.profile.create')->with(array('flash_message'=>'Package Profile not available!', 'flash_action'=>'danger'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProfileRequest $request)
    {
        $profileinfo = Profile::Select('*')->where('role', 0)->first();
        if(!empty($profileinfo)){
        $profile_data = $profileinfo;       

        $path = base_path() . '/public/uploads/demo';

        $fileName = $this->fileUpload(Input::file('admin_image'),$path);
        $profile_data ->image   = $fileName;
        if($profile_data->save())
            return Redirect::route('admin.profile.index')->with(array('flash_message'=>'password successfully changed', 'flash_action'=>'success'));
        else
            return Redirect::route('admin.profile.index')->with(array('flash_message'=>'Please enter password!', 'flash_action'=>'danger'));
        }else
            return Redirect::route('admin.profile.create')->with(array('flash_message'=>'Package Profile not available!', 'flash_action'=>'danger'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        die("hii");
        $id = $request->input('id');
        $message_data = Message::find($id);       
        $message_data ->message_title    = $request->input('message_title');
        $message_data ->message_content   = $request->input('message_content');                    
        if($message_data->save())
            return Redirect::route('admin.profile.index')->with(array('flash_message'=>'Message successfully updated!', 'flash_action'=>'success'));
        else
            return Redirect::route('admin.profile.edit', $id)->with(array('flash_message'=>'Message not updated successfully!', 'flash_action'=>'danger'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
