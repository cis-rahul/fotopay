<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CustomerRequest;
use Illuminate\Support\Facades\Redirect;
use App\Customer;
use View;

class CustomerController extends MainController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct() {
        parent::__construct();
    }

    public function index(Request $request)
    {
        $keyword = $request->input('search_keyword');
        $this->data['customers'] = Customer::SearchByKeyword($keyword)->Select('*')->where('role', 1)->orderBy('id', 'desc')->paginate(10);  
        //$this->data['customer'] = Customer::Select('*')->where('role', 0)->first();
        return View::make('admin.customer.list',$this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //$this->data['customers'] = School::Select('*')->where('status','1')->get();
        $this->data['customer'] = Customer::find($id);
        return View::make('admin.customer.create', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objCustomer = Customer::find($id);
        $objCustomer->firstname = $request->input('fname');
        $objCustomer->lastname = $request->input('lname');
        $objCustomer->email = $request->input('email');
        $objCustomer->contact = $request->input('contact');
        $objCustomer->status = $request->input('status');
        if($objCustomer->save())
            return Redirect::route('admin.customer.index')->with(array('flash_message'=>'Customer successfully updated!', 'flash_action'=>'success'));
        else
            return Redirect::route('admin.customer.edit', $id)->with(array('flash_message'=>'Customer not updated successfully!', 'flash_action'=>'danger'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        die("hii");
    }
}
