<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ReorderjobRequest;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use App\Reorderjob;
use App\School;
use App\Packageprofile;
use View;

class ReorderjobController extends MainController
{    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        parent::__construct();
    }
    
    public function index(Request $request)
    {
        $keyword = $request->input('search_keyword');
        $this->data['jobs'] = Reorderjob::SearchByKeyword($keyword)->Select('fp_schools.name','fp_jobs.*')->Join('fp_schools', 'fp_jobs.school_id', '=', 'fp_schools.id')->orderBy('id', 'desc')->paginate(10);  
        return View::make('admin.job.list', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$this->data['schools'] = School::Select('*')->where('status','1')->get();
        $this->data['packageprofile'] = Packageprofile::Select('*')->where('status','1')->get();
        return View::make('admin.job.create',$this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReorderjobRequest $request)
    {
        $objReorderjob = new Reorderjob();
        $objReorderjob->school_id = $request->input('school_id');
        $objReorderjob->job_code = $request->input('job_code');
        $objReorderjob->job_title = $request->input('job_title');
        $objReorderjob->job_date = $request->input('job_date');
        $objReorderjob->finished_date = $request->input('finished_date');
        $objReorderjob->created_at = date('Y-m-d H:i:s');
        if($objReorderjob->save())
            return Redirect::route('admin.job.index')->with(array('flash_message'=>'Reorderjob successfully added!', 'flash_action'=>'success'));
        else
            return Redirect::route('admin.job.crate')->with(array('flash_message'=>'Reorderjob not added successfully!', 'flash_action'=>'danger'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['schools'] = School::Select('*')->where('status','1')->get();
        $this->data['job'] = Reorderjob::find($id);
        return View::make('admin.job.create', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ReorderjobRequest $request, $id)
    {
        $objReorderjob = Reorderjob::find($id);
        $objReorderjob->school_id = $request->input('school_id'); 
        $objReorderjob->job_title = $request->input('job_title');       
        $objReorderjob->job_date = $request->input('job_date');
        $objReorderjob->finished_date = $request->input('finished_date');
        $objReorderjob->status = $request->input('status');
        if($objReorderjob->save())
            return Redirect::route('admin.job.index')->with(array('flash_message'=>'Reorder job successfully added!', 'flash_action'=>'success'));
        else
            return Redirect::route('admin.job.edit', $id)->with(array('flash_message'=>'Reorder job not added successfully!', 'flash_action'=>'danger'));
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        echo $id; die;
    }    
}
