<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Redirect;
use Illuminate\HttpRequest;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use View;
use Auth;
use DB;
use Session;
use Route;
use Input;
use Validator;
//use File;
//use Response;

class MainController extends Controller {
    public $data = "";    
    public function __construct() {                
        parent::__construct();
        if (!Auth::check()) {            
            return Redirect::route('admin.signin')->send(); 
        }
    }


    public function fileUpload($fileInput, $path, $fileName ="") {            
        $destinationPath = $path; // upload path
        $extension = $fileInput->getClientOriginalExtension(); // getting image extension
        $originalName = $fileInput->getClientOriginalName();
        $originalName = explode("." , $originalName);
        if(empty($fileName))
            $fileName = $originalName[0] . "-" . rand('1', '99') . date('YmdHis') . '.' . $extension; // renameing image
        $fileInput->move($destinationPath, $fileName); // uploading file to given path
    	return $fileName;
    }
}
