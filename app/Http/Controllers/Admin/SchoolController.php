<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\SchoolRequest;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use App\School;
use View;

class SchoolController extends MainController
{    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        parent::__construct();
    }
    
    public function index(Request $request)
    {
        $keyword = $request->input('search_keyword');
        $this->data['schools'] = School::SearchByKeyword($keyword)->Select('*')->orderBy('name', 'desc')->paginate(10);
        return View::make('admin.school.list', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View::make('admin.school.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SchoolRequest $request)
    {
        $objSchool = new School();
        $objSchool->name = $request->input('name');
        $objSchool->code = $request->input('code');
        if($objSchool->save())
            return Redirect::route('admin.school.index')->with(array('flash_message'=>'School successfully added!', 'flash_action'=>'success'));
        else
            return Redirect::route('admin.school.crate')->with(array('flash_message'=>'School not added successfully!', 'flash_action'=>'danger'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['school'] = School::find($id);
        return View::make('admin.school.create', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SchoolRequest $request, $id)
    {
        $objSchool = School::find($id);
        $objSchool->name = $request->input('name');
        $objSchool->status = $request->input('status');
        $objSchool->updated_at = Date('Y-m-d H:i:s');
        if($objSchool->save())
            return Redirect::route('admin.school.index')->with(array('flash_message'=>'School successfully added!', 'flash_action'=>'success'));
        else
            return Redirect::route('admin.school.edit', $id)->with(array('flash_message'=>'School not added successfully!', 'flash_action'=>'danger'));
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        echo $id; die;
    }
}
