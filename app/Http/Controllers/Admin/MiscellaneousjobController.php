<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\MiscellaneousjobRequest;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use App\Miscellaneousjob;
use App\School;
use App\Images;
use App\Packageprofile;
use View;

class MiscellaneousjobController extends MainController
{    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        parent::__construct();
    }
    
    public function index(Request $request)
    {
        $keyword = $request->input('search_keyword');
        $this->data['miscellaneousjobs'] = Miscellaneousjob::SearchByKeyword($keyword)->Select('fp_miscellaneousjobs.*')->orderBy('id', 'desc')->paginate(10);
        return View::make('admin.miscellaneousjob.list', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['packageprofile'] = Packageprofile::Select('*')->where('status','1')->get();        
        return View::make('admin.miscellaneousjob.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MiscellaneousjobRequest $request)
    {        
        $temppath = base_path() . '/public/uploads/temp';
        $finalpath = base_path() . '/public/uploads/finalimages';        
        $uploadimages = $request->input('uploadimages');
        
        $objMiscellaneousjob = new Miscellaneousjob();
        $objMiscellaneousjob->packageprofile_id = $request->input('packageprofile_id');
        $objMiscellaneousjob->job_code = $request->input('job_code');
        $objMiscellaneousjob->job_name = $request->input('job_name');
        $objMiscellaneousjob->job_date = $request->input('job_date');                
        $objMiscellaneousjob->created_at = date('Y-m-d H:i:s');
        if($objMiscellaneousjob->save()){
            if (!empty($uploadimages)) {
                foreach ($uploadimages as $objUploadImg) {
                    rename($temppath . "/" . $objUploadImg, $finalpath . "/" . $objUploadImg);
                    $objImages = new Images();
                    $objImages->uploadImage_id = $objMiscellaneousjob->id;
                    $objImages->imagename = $objUploadImg;                    
                    $objImages->created_at = date('Y-m-d H:i:s');
                    $objImages->save();
                }
            }
            return Redirect::route('admin.miscellaneousjob.index')->with(array('flash_message'=>'Miscellaneousjob successfully added!', 'flash_action'=>'success'));
        }else{
            return Redirect::route('admin.miscellaneousjob.crate')->with(array('flash_message'=>'Miscellaneousjob not added successfully!', 'flash_action'=>'danger'));
        }    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {        
        $this->data['miscellaneousjob'] = Miscellaneousjob::find($id);
        $this->data['packageprofile'] = Packageprofile::Select('*')->where('status','1')->get();
        return View::make('admin.miscellaneousjob.create', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MiscellaneousjobRequest $request, $id)
    {
        
        $temppath = base_path() . '/public/uploads/temp';
        $finalpath = base_path() . '/public/uploads/finalimages';        
        $uploadimages = $request->input('uploadimages');
        
        $objMiscellaneousjob = Miscellaneousjob::find($id);
        $objMiscellaneousjob->packageprofile_id = $request->input('packageprofile_id');        
        $objMiscellaneousjob->job_name = $request->input('job_name');
        $objMiscellaneousjob->job_date = $request->input('job_date');                
        $objMiscellaneousjob->created_at = date('Y-m-d H:i:s');
        if($objMiscellaneousjob->save()){
            if (!empty($uploadimages)) {
                Images::where('uploadImage_id', $id)->delete();
                foreach ($uploadimages as $objUploadImg) {
                    if(!file_exists($finalpath . "/" . $objUploadImg))
                        rename($temppath . "/" . $objUploadImg, $finalpath . "/" . $objUploadImg);
                    $objImages = new Images();
                    $objImages->uploadImage_id = $objMiscellaneousjob->id;
                    $objImages->imagename = $objUploadImg;                    
                    $objImages->created_at = date('Y-m-d H:i:s');
                    $objImages->save();
                }
            }            
            return Redirect::route('admin.miscellaneousjob.index')->with(array('flash_message'=>'Miscellaneous job successfully added!', 'flash_action'=>'success'));
        }else{
            return Redirect::route('admin.miscellaneousjob.edit', $id)->with(array('flash_message'=>'Miscellaneous job not added successfully!', 'flash_action'=>'danger'));
        }    
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        echo $id; die;
    }    
}
