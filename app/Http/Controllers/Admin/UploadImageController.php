<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\UploadImageRequest;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use App\UploadImage;
use App\School;
use App\Grade;
use App\Images;
use View;
use Input;

class UploadImageController extends MainController {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $this->data['uploadImages'] = uploadImage::Select('fp_uploadImage.*', 'fp_schools.name as school_name', 'fp_grades.title as grade_name', 'fp_students.firstname', 'fp_students.lastname')->Join('fp_schools', 'fp_uploadImage.school_id', '=', 'fp_schools.id')->Join('fp_students', 'fp_uploadImage.student_id', '=', 'fp_students.id')->Join('fp_grades', 'fp_uploadImage.grade_id', '=', 'fp_grades.id')->where('fp_uploadImage.status', '1')->paginate('10');
        return View::make('admin.uploadImage.list', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $this->data['schools'] = School::Select('*')->where('status', '1')->get();
        return View::make('admin.uploadImage.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UploadImageRequest $request) {
        $temppath = base_path() . '/public/uploads/temp';
        $finalpath = base_path() . '/public/uploads/finalimages';        
        $uploadimages = $request->input('uploadimages');

        $objUploadImage = new UploadImage();
        $objUploadImage->school_id = $request->input('school_id');
        $objUploadImage->grade_id = $request->input('grade_id');
        $objUploadImage->student_id = $request->input('student_id');
        $objUploadImage->created_at = date('Y-m-d H:i:s');
        if ($objUploadImage->save()) {
            if (!empty($uploadimages)) {
                foreach ($uploadimages as $objUploadImg) {
                    rename($temppath . "/" . $objUploadImg, $finalpath . "/" . $objUploadImg);
                    $objImages = new Images();
                    $objImages->uploadImage_id = $objUploadImage->id;
                    $objImages->imagename = $objUploadImg;                    
                    $objImages->created_at = date('Y-m-d H:i:s');
                    $objImages->save();
                }
            }
            return Redirect::route('admin.uploadImage.index')->with(array('flash_message' => 'UploadImage successfully added!', 'flash_action' => 'success'));
        }else{
            return Redirect::route('admin.uploadImage.index')->with(array('flash_message' => 'Please select image first!', 'flash_action' => 'warning'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $this->data['schools'] = School::Select('*')->where('status', '1')->get();
        $this->data['uploadImage'] = UploadImage::find($id);
        $this->data['images'] = Images::find($this->data['uploadImage']->id);        
        return View::make('admin.uploadImage.create', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UploadImageRequest $request, $id) {
        $objUploadImage = UploadImage::find($id);
        $objUploadImage->school_id = $request->input('school_name');
        $objUploadImage->title = $request->input('title');
        $objUploadImage->status = $request->input('status');
        $objUploadImage->updated_at = Date('Y-m-d H:i:s');
        if ($objUploadImage->save())
            return Redirect::route('admin.uploadImage.index')->with(array('flash_message' => 'UploadImage successfully added!', 'flash_action' => 'success'));
        else
            return Redirect::route('admin.uploadImage.edit', $id)->with(array('flash_message' => 'UploadImage not added successfully!', 'flash_action' => 'danger'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    /**
     *  This method return the uploadImage list of school
     *  Created By Rahul Gupta
     *  Created Date : 05-Oct-2016
     */
    public function uploadImageListByID(Request $request, $id) {
        if ($request->ajax()) {
            $uploadImageList = UploadImage::Where('school_id', $id)->get()->toArray();
            $status = false;
            if (!empty($uploadImageList)) {
                $status = true;
            }
            echo json_encode(array('status' => $status, 'data' => $UploadImageList));
        }
    }

    public function upload(Request $request) {        
        if ($request->ajax()) {
            $path = base_path() . '/public/uploads/temp';
            return $fileName = $this->fileUpload(Input::file('file'), $path);
        }
    }

    public function removeImage(Request $request) {
        if ($request->ajax()) {
            $path = base_path() . '/public/uploads/temp/';
            if($request->input('status') != "success")
                $path = base_path() . '/public/uploads/finalimages/';                
            $imageName = $_POST['imgNm'];            
            $status = false;            
            if (file_exists($path . $imageName)) {                
                unlink($path . $imageName);
                $status = true;
            }
            echo json_encode(array(
                'status' => $status
            ));
        }
    }    
    public function getImage(Request $request, $id){
        if($request->ajax()){
            $images = Images::where('uploadImage_id', $id)->get()->toArray();
            $status = false;
            $data = array();
            $i = 1;
            if(!empty($images)){
                foreach($images as $objImg){                    
                    $data[$i]["FileName"] = $objImg['imagename'];
                    $data[$i]["Path"] = route('home') . "/../public/uploads/finalimages/{$objImg['imagename']}";
                    $data[$i]['size'] = 0;
                    if(file_exists("public/uploads/finalimages/" . $objImg['imagename']))
                        $data[$i]['size'] = filesize("public/uploads/finalimages/" . $objImg['imagename']);                        
                    $i++;
                }
                $status = true;    
            }            
            echo json_encode(array(
                'status' => $status,
                'data' => $data
            ));
        }
    }

}
