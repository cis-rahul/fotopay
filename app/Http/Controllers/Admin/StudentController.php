<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\StudentRequest;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use App\Student;
use App\School;
use View;

class StudentController extends MainController
{
    public function __construct() {
        parent::__construct();
    }
    
    public function index()
    {
        $this->data['students'] = Student::Select('*')->orderBy('id', 'desc')->paginate(10);
        return View::make('admin.student.list', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['schools'] = School::Select('*')->where('status','1')->get();
        return View::make('admin.student.create',$this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StudentRequest $request)
    {
        $objStudent = new Student();
        $objStudent->school_id = $request->input('school_id');
        $objStudent->grade_id = $request->input('grade_id');
        $objStudent->firstname = $request->input('firstname');
        $objStudent->lastname = $request->input('lastname');
        $objStudent->phone_no = $request->input('phoneno');
        $objStudent->email_id = $request->input('email');
        $objStudent->created_at = date('Y-m-d H:i:s');
        if($objStudent->save())
            return Redirect::route('admin.student.index')->with(array('flash_message'=>'Student successfully added!', 'flash_action'=>'success'));
        else
            return Redirect::route('admin.student.crate')->with(array('flash_message'=>'Student not added successfully!', 'flash_action'=>'danger'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['schools'] = School::Select('*')->where('status','1')->get();
        $this->data['student'] = Student::find($id);
        return View::make('admin.student.create', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StudentRequest $request, $id)
    {
        $objStudent = Student::find($id);
        $objStudent->school_id = $request->input('school_id');
        $objStudent->grade_id = $request->input('grade_id');
        $objStudent->firstname = $request->input('firstname');
        $objStudent->lastname = $request->input('lastname');
        $objStudent->phone_no = $request->input('phoneno');
        $objStudent->email_id = $request->input('email');
        $objStudent->created_at = date('Y-m-d H:i:s');
        $objStudent->status = $request->input('status');
        if($objStudent->save())
            return Redirect::route('admin.student.index')->with(array('flash_message'=>'Student successfully added!', 'flash_action'=>'success'));
        else
            return Redirect::route('admin.student.edit', $id)->with(array('flash_message'=>'Student not added successfully!', 'flash_action'=>'danger'));
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        echo $id; die;
    }
    
    /**     
     *  This method return the grade list of school
     *  Created By Rahul Gupta
     *  Created Date : 05-Oct-2016
     */
    public function studentListByGradeID(Request $request, $id)
    {
        if($request->ajax()){
            $studentList = Student::Where('grade_id',$id)->get()->toArray();
            $status = false;
            if(!empty($studentList)){                
                $status = true;
            }
            echo json_encode(array('status'=>$status,'data'=>$studentList));
        }
    }
}
