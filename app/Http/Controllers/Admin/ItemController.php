<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Item;
use App\Package;
use App\Image;
use App\Http\Requests\ItemRequest;
use Illuminate\Support\Facades\Redirect;
use View;
use Auth;
use Pagination;

class ItemController extends MainController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {                
        parent::__construct();
    }
    public function index()
    {
        
        $this->data['item_data'] = Item::Select('fp_item.id','fp_item.package_id','fp_item.image_id','fp_item.width','fp_item.height','fp_item.quantity','fp_package.name','fp_imagetype.image_title')->join('fp_package', 'fp_item.package_id', '=', 'fp_package.id')->join('fp_imagetype', 'fp_item.image_id', '=', 'fp_imagetype.id')->paginate(10);        
        
        return View::make('admin.item.list',$this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
            $this->data['packages'] = Package::Select('id','name')->get();                     
            $this->data['img_type'] = Image::Select('id','image_title')->get();                     
            return View::make('admin.item.create',$this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ItemRequest $request)
    {
        $item_data = new Item();       
        $item_data ->package_id    = $request->input('package_name');
        $item_data ->image_id   = $request->input('img_type');
        $item_data ->width   = $request->input('width');
        $item_data ->height   = $request->input('height');
        $item_data ->quantity   = $request->input('quantity');
        if($item_data->save())
            return Redirect::route('admin.item.store')->with(array('flash_message'=>'Item successfully added', 'flash_action'=>'success'));
        else
            return Redirect::route('admin.item.create')->with(array('flash_message'=>'Item not saved successfully!', 'flash_action'=>'danger'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Item::find($id);
        $this->data['packages'] = Package::Select('id','name')->get();
        $this->data['img_type'] = Image::Select('id','image_title')->get();                  
        $data = array(
                      'item'  => $item,
                       'id'=>$id
                );
        return View::make('admin.item.create',$this->data)->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ItemRequest $request)
    {
        $id = $request->input('id');
        $item_data = Item::find($id);       
        
        $item_data ->package_id   = $request->input('package_name');
        $item_data ->image_id     = $request->input('img_type');
        $item_data ->width        = $request->input('width');
        $item_data ->height       = $request->input('height');
        $item_data ->quantity     = $request->input('quantity'); 
        $item_data ->status    = $request->input('status');                   
        if($item_data->save())
            return Redirect::route('admin.item.index')->with(array('flash_message'=>'Item successfully Updated!', 'flash_action'=>'success'));
        else
            return Redirect::route('admin.item.edit', $id)->with(array('flash_message'=>'Item not update successfully!', 'flash_action'=>'danger'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
