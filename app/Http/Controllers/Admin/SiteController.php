<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\MessageRequest;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use App\User;
use App\Message;
use View;
use Auth;
use Session;
use Input;


class SiteController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        parent::__construct();         
    }
    public function index() {        
        if (Auth::check()) {            
            return Redirect::route('admin.dashboard')->send();            
        } else {            
            return View::make('admin.login'); 
        }
    }    

    public function signin(LoginRequest $request) {                
        $credential_email = array('email' => $request->input('email'), 'password' => $request->input('password'));
        $credential_username = array('username' => $request->input('email'), 'password' => $request->input('password'));
        if (Auth::attempt($credential_email) || Auth::attempt($credential_username)) {                                   
            return Redirect::route('admin.dashboard');
        }else {
            return Redirect::route('admin.signin')->with(array('flash_alert_notice' => 'Wrong Email or Password. Try again!', 'flash_action' => 'danger'))->withInput();
        }
    }    

    public function dashboard() {        
        if (Auth::check()) {             
            return View::make('admin.dashboard');
        } else {
            return Redirect::to('admin.signin');
        }
    }    
   
    
    public function logout(Request $request) {        
        Auth::logout();
        $request->session()->flush();
        return Redirect::route('admin.signin');
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }
}
