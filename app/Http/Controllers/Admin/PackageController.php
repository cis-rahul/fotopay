<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\PackageRequest;
use Illuminate\Support\Facades\Redirect;
use App\Package;
use App\Packageprofile;
use File;
use Validator;
use View;
use Auth;
use Input;

class PackageController extends MainController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {                
        parent::__construct();
    }
    public function index(Request $request)
    {

        $keyword = $request->input('search_keyword');
        if(!empty($request->input('pp')))
            $this->data['package_data'] = Package::SearchByKeyword($keyword)->Select('id','name','price','image')->where('profile_id', $request->input('pp'))->orderBy('id', 'DESC')->paginate(10);        
        else
            $this->data['package_data'] = Package::SearchByKeyword($keyword)->Select('id','name','price','image')->orderBy('id', 'DESC')->paginate(10);        
        return View::make('admin.package.list',$this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::check()) {             
            return View::make('admin.package.create');
        } else {
            return Redirect::route('admin.signin');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PackageRequest $request)
    {                
        $packageprofileinfo = Packageprofile::find($request->input('profile_id'));
        if(!empty($packageprofileinfo)){
            $package_data = new Package();       
            $package_data ->profile_id    = $request->input('profile_id');
            $package_data ->name    = $request->input('package_name');
            $package_data ->price   = $request->input('package_price');                            
            $package_data ->description   = $request->input('package_desc');
            $package_data ->post_handling   = $request->input('ph');
            $path = base_path() . '/public/uploads/demo';
            $fileName = $this->fileUpload(Input::file('package_image'),$path);
            $package_data ->image   = $fileName;
            if($package_data->save())
                return Redirect::route('admin.package.store')->with(array('flash_message'=>'Package successfully added', 'flash_action'=>'success'));
            else
                return Redirect::route('admin.package.create')->with(array('flash_message'=>'Package not saved successfully!', 'flash_action'=>'danger'));
        }else
            return Redirect::route('admin.package.create')->with(array('flash_message'=>'Package Profile not available!', 'flash_action'=>'danger'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $package = Package::find($id);
         $data = array(
                      'package'  => $package,
                       'id'=>$id
                );
        

        return View::make('admin.package.create')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PackageRequest $request)
    {        
        $id = $request->input('id');
        $package_data = package::find($id);       
        $package_data->name    = $request->input('package_name');
        $package_data->price   = $request->input('package_price');
        $package_data->description   = $request->input('package_desc');          
        
        if(Input::file('package_image')->getError() == 0){
            $path = base_path() . '/public/uploads/demo';
            $fileName = $this->fileUpload(Input::file('package_image'),$path,$package_data->image);            
        }
        if($package_data->save())
            return Redirect::route('admin.package.index')->with(array('flash_message'=>'Package successfully Updated!', 'flash_action'=>'success'));
        else
            return Redirect::route('admin.package.edit', $id)->with(array('flash_message'=>'Package not updated successfully!', 'flash_action'=>'danger'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
