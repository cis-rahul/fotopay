<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use DB;
use App\User;
use App\Prepayjob;
use App\Message;
use App\Package;
use App\Http\Requests;
use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\PrepayRequest;
use App\Http\Controllers\Controller;
use Response;

use Auth;
use View;

class PrepayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return View::make('prepay.login'); 
        
    }




    public function signin(Request $request) {        
              
        $credential_email = array('email' => $request->input('username'), 'password' => $request->input('password'),'role' => 1);
        
        $credential_username = array('username' => $request->input('username'), 'password' => $request->input('password'),'role' => 1);
        if (Auth::attempt($credential_email) || Auth::attempt($credential_username)) {                                   
            return Redirect::route('prepay.dashboard');
        }else {
            return Redirect::route('prepay')->with(array('flash_alert_notice' => 'Wrong Email or Password. Try again!', 'flash_action' => 'danger'))->withInput();
        }
    }   


     public function dashboard() { 
    
        $this->data['package_data'] = Package::Select('*')->get();
        
        return View::make('prepay.prepayPhoto',$this->data); 
    } 
    
    
    
     public function checkjob_code(Request $request)
    {
        $job = $request->input('job');
        $current_date = date('Y-m-d h:i:s');        
        $job_code = Prepayjob::Select('*')->where('job_code', $job)->first();
        $message = Message::Select('*')->where('id', 8)->first();
        $msg ="";
        if(!empty($message)){
            $msg = $message['message_content'];
        }
        
        $status = 0;
        if(!empty($job_code)){
            $status = 1;
            if($job_code->finished_date < $current_date){
                $status = 2;
            }
           // $status = 1;
        }

        return Response::json(array('status'=>$status,'message'=>$msg));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PrepayRequest $request)
    {
        
        $prepay_data = new User();  

        $prepay_data ->username      = $request->input('fname');
        $prepay_data ->firstname     = $request->input('fname');
        $prepay_data ->lastname      = $request->input('lname');
        $prepay_data ->email         = $request->input('email');
        $prepay_data ->password      = bcrypt($request->input('contact'));
        $prepay_data ->created_at    = Date('Y-m-d H:i:s');
        if($prepay_data->save())
            return Redirect::route('prepay')->with(array('flash_message'=>'Account successfully created', 'flash_action'=>'success'));
        else
            return Redirect::route('prepay')->with(array('flash_message'=>'Account not created!', 'flash_action'=>'danger'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
