<?php namespace App\Http\Middleware;
//use Illuminate\Routing\Route;
use Route;
use Session;
// First copy this file into your middleware directoy
use Closure;
use Auth;
class CheckRole{
	/**
	 * Handle an incoming request.	 
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{               
            //Agreement Controller Function
            $manageUser = array('index','show','inactive_list','active_list','create','store','inactive','active','dashboard');
            
            
            $agreement['add'] = array('create','store','uploadDocument','entityPropertyRelation');
            $agreement['edit'] = array('edit','update','updateUploadDocument','updateEntityPropertyRelation');
            $agreement['view'] = array('index','show','agreeListView', 'showUploadDocument');
            $agreement['delete'] = array('destroy');
            
            //Entity Controller Function
            $entity['add'] = array('create','store','uploadDocument','entityRelation', 'entityPropertyRelation', 'uploadDocument','smslogs','mail','cellphone');
            $entity['edit'] = array('update','inactive','active','addEntityAddress','addEntityBanking','updateEntityEntityRelation', 'updateEntityPropertyRelation','updateUploadDocument');
            $entity['view'] = array('index','show','inactive_list','active_list','getEntityEntityRelation','getEntityPropertyRelation','showUploadDocument','viewDocument');
            $entity['delete'] = array('destroy');
            
            //business Controller Function
            $business['add'] = array('create','store','busInvite','uploadDocument');
            $business['edit'] = array('update','updateBusInvite','addBusAddress','addBusBanking','updateUploadDocument','updateBusRestriction');
            $business['view'] = array('index','show','BusList','showUploadDocument');
            $business['delete'] = array('destroy','delInvitation');
            
            //contact Controller Function
            $contact['add'] = array('create','store');
            $contact['edit'] = array('edit','update');
            $contact['view'] = array('index','show','frm_contact');
            $contact['delete'] = array('destroy');
            
            //task Controller Function
            $task['add'] = array('create','store');
            $task['edit'] = array('edit','update');
            $task['view'] = array('index','show');
            $task['delete'] = array('destroy');
            
            //user Controller Function
            $user['add'] = array('create','store');
            $user['edit'] = array('edit','update');
            $user['view'] = array('index','show','userInfoByID','updateManageUserByID');
            $user['delete'] = array('destroy');
            
            //userDetail Controller Function
            $userDetail['add'] = array('create','store');
            $userDetail['edit'] = array('edit','update');
            $userDetail['view'] = array('index','show');
            $userDetail['delete'] = array('destroy');
            
            
            $rest_curr_bus = $request->session()->get('current_bus_restriction');                        
            $actions = $request->route()->getAction();
            $replace_str = $actions['namespace'] . "\\";
            $cont_action = str_replace($replace_str, "", $actions['controller']) ;
            $cont_action_arr = explode('@', $cont_action);            
            $status = true;
            
            $user_role = $request->session()->get('role_id');
            if(!empty($user_role) && $user_role == "2"){
                if($cont_action_arr['0'] != "EntityController"){
                    $status = false;
                }else{
                    if(in_array($cont_action_arr['1'], $manageUser)){
                        $status = false;
                    }                     
                }
            }else if(!empty($rest_curr_bus)){
                if($cont_action_arr['0'] == "EntityController"){                    
                    if(in_array('1',$rest_curr_bus)){                
                        if(in_array($cont_action_arr['1'], $entity['add'])){
                            $status = false;
                        }                                    
                    }
                    if(in_array('2',$rest_curr_bus)){
                        if(in_array($cont_action_arr['1'], $entity['add']) || in_array($cont_action_arr['1'], $entity['edit']) || in_array($cont_action_arr['1'], $entity['delete'])){
                            $status = false;
                        }                
                    } 
                }
                if($cont_action_arr['0'] == "BusinessController"){
                    if(in_array('1',$rest_curr_bus)){                
                        if(in_array($cont_action_arr['1'], $business['add'])){
                            $status = false;
                        }                                    
                    }
                    if(in_array('2',$rest_curr_bus)){
                        if(in_array($cont_action_arr['1'], $business['add']) || in_array($cont_action_arr['1'], $business['edit']) || in_array($cont_action_arr['1'], $business['delete'])){
                            $status = false;
                        }                
                    } 
                }
                if($cont_action_arr['0'] == "ContactController"){
                    if(in_array('1',$rest_curr_bus)){                
                        if(in_array($cont_action_arr['1'], $contact['add'])){
                            $status = false;
                        }                                    
                    }
                    if(in_array('2',$rest_curr_bus)){
                        if(in_array($cont_action_arr['1'], $contact['add']) || in_array($cont_action_arr['1'], $contact['edit']) || in_array($cont_action_arr['1'], $contact['delete'])){
                            $status = false;
                        }                
                    } 
                }
                if($cont_action_arr['0'] == "TaskController"){
                    if(in_array('1',$rest_curr_bus)){                
                        if(in_array($cont_action_arr['1'], $task['add'])){
                            $status = false;
                        }                                    
                    }
                    if(in_array('2',$rest_curr_bus)){
                        if(in_array($cont_action_arr['1'], $task['add']) || in_array($cont_action_arr['1'], $task['edit']) || in_array($cont_action_arr['1'], $task['delete'])){
                            $status = false;
                        }                
                    } 
                }
                if($cont_action_arr['0'] == "UserController"){
                    if(in_array('1',$rest_curr_bus)){                
                        if(in_array($cont_action_arr['1'], $user['add'])){
                            $status = false;
                        }                                    
                    }
                    if(in_array('2',$rest_curr_bus)){
                        if(in_array($cont_action_arr['1'], $user['add']) || in_array($cont_action_arr['1'], $user['edit']) || in_array($cont_action_arr['1'], $user['delete'])){
                            $status = false;
                        }                
                    } 
                }
//                if($cont_action_arr['0'] == "UserDetailsController"){
//                    if(in_array('1',$rest_curr_bus)){                
//                        if(in_array($cont_action_arr['1'], $userDetail['add'])){
//                            $status = false;
//                        }                                    
//                    }
//                    if(in_array('2',$rest_curr_bus)){
//                        if(in_array($cont_action_arr['1'], $userDetail['add']) || in_array($cont_action_arr['1'], $userDetail['edit']) || in_array($cont_action_arr['1'], $userDetail['delete'])){
//                            $status = false;
//                        }                
//                    } 
//                }
            }
            if($status == true){                
                return $next($request);
            }else{                
                return \Illuminate\Support\Facades\Redirect::back();
            }    
	}	     
}
