<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Route;

class PrepayRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

     public function rules()
    {
        return [
            'fname'      => 'required',
            'lname'      => 'required',
            'email'      => 'required|email',
            'confirm_email'      => 'required',
            'contact'      => 'numeric',

        ];
    }


        public function attributes()
    {
        return[
            'fname' => 'first name', //This will replace any instance of 'username' in validation messages with 'email'
            'lname' => 'last name',
            'confirm_email' => 'confirm',
            'contact' => 'contact',
            
        ];
    }
//    public function messages()
//    {
//        return [
//            'name.required' => 'Please provide a brief link description',
//            'url.required' => 'Please provide a URL',
//            'url.url' => 'A valid URL is required',
//            'category.required' => 'Please associate this link with a category',
//            'category.min' => 'Please associate this link with a category'
//        ];
//    }
}
