<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PackageRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $action = $_POST['action'];

        $rules = [
            'Create' => [
                'package_name'    => 'required',
                'package_price'   => 'required|integer',
                'package_desc'   => 'required',
                'profile_id'   => 'required',
                'package_image'   => 'required|mimes:png.jpg,jpeg'
            ],            
            'Edit' => [
                'package_name'    => 'required',
                'package_price'   => 'required|integer',
                'package_desc'   => 'required',
                'profile_id'   => 'required',
                
            ],            
        ];
        return $rules[$action];
    }
}
