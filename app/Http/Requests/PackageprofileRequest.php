<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PackageprofileRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
        ];
        $action = $_POST['action'];
        
        $rules = [
            'Create' => [
                'packageprofile_name'    => 'required',            
                'packageprofile_image'   => 'required|mimes:png,jpg,jpeg,gif'
            ],            
            'Edit' => [
                'packageprofile_name'    => 'required',
            ],            
        ];        
        return $rules[$action];
    }
}
