<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Route;

class SchoolRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules() {
        $action = $_POST['action'];
        
        $rules = [
            'Create' => [
                'name'       => 'required',
                'code'       => 'required|unique:fp_schools',
                'jobdate'    => 'required',
                'finishdate' => 'required'
            ],            
            'Edit' => [
                'name'       => 'required',
                'jobdate'    => 'required',
                'finishdate' => 'required'
            ],            
        ];
        
        return $rules[$action];
    }
//    public function messages()
//    {
//        return [
//            'name.required' => 'Please provide a brief link description',
//            'url.required' => 'Please provide a URL',
//            'url.url' => 'A valid URL is required',
//            'category.required' => 'Please associate this link with a category',
//            'category.min' => 'Please associate this link with a category'
//        ];
//    }
}
