<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Route;

class ContactRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
//    public function rules()
//    {
//        
//        return [
//            'name'      => 'required',
//            'code'   => 'required|unique:fp_schools'
//        ];
//    }
   public function rules()
    {
        return [
            'contact_physical_address1'  => 'required',
            'contact_physical_suburb'    => 'required',
            'contact_physical_postcode'  => 'required|integer',
            'contact_physical_phone'     => 'required|regex:/\d{5}([- ]*)\d{5}/',
            'contact_physical_email'     => 'required|email',
            'contact_postal_address1'    => 'required',
            'contact_postal_suburb'      => 'required',
            'contact_postal_postcode'    => 'required|integer',
            'contact_postal_phonenumber' => 'required|min:11|integer',
            'contact_postal_email'       => 'required|email',
        ];
    }

    public function attributes()
    {
        return[
            'contact_postal_postcode' => 'Post Code', //This will replace any instance of 'username' in validation messages with 'email'
            'contact_postal_suburb' => 'Suburb',
            'contact_postal_email' => 'Email',
            'contact_physical_postcode' => 'Post Code', 
            'contact_physical_suburb' => 'Suburb',
            'contact_physical_email' => 'Email',
        ];
    }

    // public function messages()
    // {
    //     return[
    //         'contact_postal_postcode.required' => 'Pnbhnvbbnbost Code', //This will replace any instance of 'username' in validation messages with 'email'
    //         //'anyinput' => 'Nice Name',
    //     ];
    // }


}
