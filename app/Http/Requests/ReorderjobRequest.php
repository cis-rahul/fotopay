<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Route;

class ReorderjobRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules() {
        $action = $_POST['action'];        
        $rules = [
            'Create' => [
                'job_name'      => 'required',
                'job_code'       => 'required|unique:fp_reorderjobs',
                'job_date'       => 'required',                
                'packageprofile_id' => 'required'
            ],            
            'Edit' => [
                'job_name'      => 'required',                
                'job_date'       => 'required',                
                'packageprofile_id' => 'required'
            ],            
        ];        
        return $rules[$action];        
    }
//    public function messages()
//    {
//        return [
//            'name.required' => 'Please provide a brief link description',
//            'url.required' => 'Please provide a URL',
//            'url.url' => 'A valid URL is required',
//            'category.required' => 'Please associate this link with a category',
//            'category.min' => 'Please associate this link with a category'
//        ];
//    }
}
