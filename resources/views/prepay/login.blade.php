@extends('layouts.innermaster')
{{-- Page title --}}
@section('title')
    Message
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')    
@stop
{{-- Page content --}}
@section('content')
 <!--header end-->
	    <!--breadcrumb start-->
	    <div class="breadcrumb">   
		<div class="container">
		    <div class="row">

			<ul>
			    <li><a href="">Home</a></li>
			    <li><a href="{{ route('prepay') }}">Pre Pay</a></li>
			</ul>    

		    </div>
		</div>
	    </div>
	    <!--breadcrumb end-->

	    <!--page title start-->
	    <form id="frm" method="post" class="ajax" action=""> 
	    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
	    <div class="page-title">   
		<div class="container">
		    <div class="row">

			<h1>PrePay Online</h1>
			<div class="job-code-box">
			    <label>Please enter the job code</label>
			    <div class="job-code-input">
				<input type="text" placeholder="Enter job code" name="job" value="">
			    </div>
			    <div class="job-code-button">
				<button class="add-order-button job_code" type="button">Enter</button>
			    </div>
			</div>

		    </div>
		</div>
	    </div>
	    </form>
	    <!--page title end-->

	    <!--detail photo start-->
	    <form action="{{route('prepay.signin')}}" class="omb_loginForm"  autocomplete="off" method="POST">
	    <div class="detail-photo">   
		<div class="container">
		    <div class="row">

			<p>The payment envelope must be returned completed with the receipt number.</p>
			
				<div class="customer-box">
				    <div class="customer-title">Existing Customer <span>Login Here</span></div>
				
				    <div class="customer-input">
					<label>Username</label>
					<input type="email" name="username" placeholder="Enter username" value="">
				    </div>

				    <div class="customer-input">
					<label>Password</label>
					<input type="password" name="password" placeholder="Enter password" value="">
				    </div>

				    <div class="customer-button">
					<button class="add-order-button" type="submit">login</button>
				    </div>
				</div>

		    </div>
		</div>
	    </div>
	    </form>
	    <!--detail photo end-->
	    <!--content start-->
	    <div class="inner-contnet">
		<div class="container">
		@if (session('flash_message'))
	        <div class="flash-message">
	            <div class="alert alert-{{session('flash_action')}}">
	                <p>{{session('flash_message')}}</p>
	            </div>
	        </div>
        @endif
		<form action="{{route('prepay.store')}}" method="POST" role="form">
            <!-- <input type="hidden" name="_token" value="{{ csrf_token() }}" /> -->
            <div class="row">

			<div class="create-input-box">
			    <div class="row">

				<div class="col-sm-12 user-text">
				    Create a account is your are a first time user.
				</div>

				<div  class="col-sm-6 create-input form-group required {{ $errors->first('fname', 'has-error') }}">
				    <label>First Name<span>*</span></label>
				    <input type="text" name="fname" placeholder="Enter your first name" value="{{Input::old('fname')}}">
				    {!! $errors->first('fname', '<span class="help-block">:message</span>') !!}
				</div>

				<div class="col-sm-6 create-input form-group required {{ $errors->first('lname', 'has-error') }}">
				    <label>Last Name<span>*</span></label>
				    <input type="text" name="lname" placeholder="Enter your last name" value="{{Input::old('lname')}}">
				    {!! $errors->first('lname', '<span class="help-block">:message</span>') !!}
				</div>

				<div class="col-sm-6 create-input form-group required {{ $errors->first('email', 'has-error') }}">
				    <label>Email Address<span>*</span></label>
				    <input type="email" name="email" placeholder="Enter your email ID" value="{{Input::old('email')}}">
				    <label class="create-error">( This will be your <span>Username</span> )</label>
				    {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
				</div>

				<div class="col-sm-6 create-input form-group required {{ $errors->first('confirm_email', 'has-error') }}">
				    <label>Confirm Email Address<span>*</span></label>
				    <input type="email" name="confirm_email" placeholder="Re-enter your email ID" value="{{Input::old('confirm_email')}}">
				    {!! $errors->first('confirm_email', '<span class="help-block">:message</span>') !!}
				</div>

				<div class="col-sm-6 create-input form-group required {{ $errors->first('contact', 'has-error') }}">
				    <label>Contact Number<span>*</span></label>
				    <input type="number" name="contact" maxlength="10" placeholder="Enter your contact number" value="{{Input::old('contact')}}">
				    <label class="create-error">( 10 digit number. This will be your <span>password</span> )</label>
				    {!! $errors->first('contact', '<span class="help-block">:message</span>') !!}
				</div>

			    </div>

			    <div class="create-button">            
				<!-- <button class="add-order-button" type="button">Create</button> -->
				<input type="submit" class="add-order-button" value="Create">
				<label>A copy will be emailed to the address your nominated.</label>            
			    </div>

			    <div class="create-content">
				Your Username (email address) and Password (Phone Number) are used to view your orders and make new orders in future. Please keep your username and password in a safe please.
			    </div>

			</div>    	

		    </div>
		    </form>
		</div>
	    </div>
	    <!--content end-->    
@endsection


{{-- page level scripts --}}
@section('footer_scripts')    
   <script type="text/javascript">
$(document).ready(function(){
	
  $('.job_code').click(function(){            
    $.ajax({
      url: "{{route('prepay.job')}}",
      type: "post",
      data: {'job':$('input[name=job]').val(),'_token':"{{csrf_token()}}"},
      success: function(res){
      	console.log(res);
      	if(res.status==2){
      		alert(res.message);
      	}else if(res.status==1){
      		alert("Job Not available");
      	}
      	
      }
    });      
  }); 
});
</script>
@endsection
