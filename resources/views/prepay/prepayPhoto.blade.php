@extends('layouts.innermaster')
{{-- Page title --}}
@section('title')
    Message
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')    
@stop
{{-- Page content --}}
@section('content')
 <!--header end-->
	    <!--breadcrumb start-->
    <div class="breadcrumb">   
    <div class="container">
    <div class="row">
    
    	<ul>
        	<li><a href="{{ route('prepay') }}">Home</a></li>
            <li><a href="{{ route('prepay.dashboard') }}">PrePay Photos</a></li>
        </ul>    
    
    </div>
    </div>
    </div>
    <!--breadcrumb end-->
    
    <!--page title start-->
    <div class="page-title">   
    <div class="container">
    <div class="row">
    
    	<h1>PrePay Photos</h1>
    
    </div>
    </div>
    </div>
    <!--page title end-->
    
    <!--detail photo start-->
    <div class="detail-photo">   
    <div class="container">
    <div class="row">
    
    	<div class="detail-photo-left">
        	<h2>Nyngan High School</h2>
			<p>Wednesday, 05th October 2016</p>
        </div>
        
        <div class="detail-photo-right">
        	<button type="button" class="add-order-button">Add Order</button>
        </div>
    
    </div>
    </div>
    </div>
    <!--detail photo end-->
    
    
    
    
    <!--content start-->
    <div class="inner-contnet">
    
    <div class="prepay-tab">

        
        <div class="prepay-tab-content">
        <div class="container">
        <div class="row">
        
            <div class="miscellaneous-box">
            <div class="row">
            
                <div class="col-sm-7">
                  <!-- Tab panes -->
                  <div class="tab-box">
                          <div class="prepay-tab-nav">

          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Portrait Packages</a></li>
            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Family Packages</a></li>
          </ul>        
     
     
        </div>
                  <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="home">
                        
                        <div class="packages-list-box">
                            @if(!empty($package_data))                                                               
                            @foreach($package_data as $objpackage)
                            <div class="packages-list">
                                <div class="packages-list-left">
                                    <h3 class="package-title">{{$objpackage['name']}} <span>${{$objpackage['price']}}</span></h3>
                                    <ul class="package-size">
                                        <li>{{$objpackage['description']}}</li>
                                       </ul>                                   
                                    <div class="package-image">
                                        <a href="images/package1.jpg" data-lightbox="example-set"><img src="{{ asset('public/assets/images/package1.jpg') }}" alt="" />  </a>
                                    </div>
                                </div>
                                
                                <div class="packages-list-right">
                                    <div class="numbers-box"><input type="text" name="french-hens" id="french-hens" value="1"></div>
                                </div>
                            </div>
                            @endforeach                            
                            @endif 
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="profile">
                        
                        <div class="packages-list-box">
                        
                            <div class="packages-list">
                                <div class="packages-list-left">
                                    <h3 class="package-title">Pack A <span>$56</span></h3>
                                    <ul class="package-size">
                                        <li>5” x 7” (3)</li>
                                        <li>2 1/2” x 3 1/2” (2)</li>
                                        <li>2 1/2” x 1 3/4” (4)</li>
                                        <li>6” x 4” (2)</li>
                                        <li>2” x 3” (4)</li>
                                        <li>3/4” x 1” (5)</li>
                                        <li>10” x 8” (1)</li>
                                    </ul>                                   
                                    <div class="package-image">
                                        <a href="images/package1.jpg" data-lightbox="example-set"><img src="{{ asset('public/assets/images/package1.jpg') }}" alt="" />  </a>
                                    </div>
                                </div>
                                
                                <div class="packages-list-right">
                                    <div class="numbers-box"><input type="text" name="french-hens" id="french-hens" value="1"></div>
                                </div>
                            </div>
                            
                            <div class="packages-list">
                                <div class="packages-list-left">
                                    <h3 class="package-title">Pack A <span>$56</span></h3>
                                    <ul class="package-size">
                                        <li>5” x 7” (3)</li>
                                        <li>2 1/2” x 3 1/2” (2)</li>
                                        <li>2 1/2” x 1 3/4” (4)</li>
                                        <li>6” x 4” (2)</li>
                                        <li>2” x 3” (4)</li>
                                        <li>3/4” x 1” (5)</li>
                                        <li>10” x 8” (1)</li>
                                    </ul>                                   
                                    <div class="package-image">
                                        <a href="images/package2.jpg" data-lightbox="example-set"><img src="{{ asset('public/assets/images/package2.jpg') }}" alt="" />  </a>
                                    </div>
                                </div>
                                
                                <div class="packages-list-right">
                                    <div class="numbers-box"><input type="text" name="french-hens" id="french-hens" value="1"></div>
                                </div>
                            </div>
                            
                            <div class="packages-list">
                                <div class="packages-list-left">
                                    <h3 class="package-title">Pack A <span>$56</span></h3>
                                    <ul class="package-size">
                                        <li>5” x 7” (3)</li>
                                        <li>2 1/2” x 3 1/2” (2)</li>
                                        <li>2 1/2” x 1 3/4” (4)</li>
                                        <li>6” x 4” (2)</li>
                                        <li>2” x 3” (4)</li>
                                        <li>3/4” x 1” (5)</li>
                                        <li>10” x 8” (1)</li>
                                    </ul>                                   
                                    <div class="package-image">
                                        <a href="images/package3.jpg" data-lightbox="example-set"><img src="{{ asset('public/assets/images/package3.jpg') }}" alt="" />  </a>
                                    </div>
                                </div>
                                
                                <div class="packages-list-right">
                                    <div class="numbers-box"><input type="text" name="french-hens" id="french-hens" value="1"></div>
                                </div>
                            </div>
                            
                            <div class="packages-list">
                                <div class="packages-list-left">
                                    <h3 class="package-title">Pack A <span>$56</span></h3>
                                    <ul class="package-size">
                                        <li>5” x 7” (3)</li>
                                        <li>2 1/2” x 3 1/2” (2)</li>
                                        <li>2 1/2” x 1 3/4” (4)</li>
                                        <li>6” x 4” (2)</li>
                                        <li>2” x 3” (4)</li>
                                        <li>3/4” x 1” (5)</li>
                                        <li>10” x 8” (1)</li>
                                    </ul>                                   
                                    <div class="package-image">
                                        <a href="images/package4.jpg" data-lightbox="example-set"><img src="{{ asset('public/assets/images/package4.jpg') }}" alt="" />  </a>
                                    </div>
                                </div>
                                
                                <div class="packages-list-right">
                                    <div class="numbers-box"><input type="text" name="french-hens" id="french-hens" value="1"></div>
                                </div>
                            </div>
                            
                            <div class="packages-list">
                                <div class="packages-list-left">
                                    <h3 class="package-title">Pack A <span>$56</span></h3>
                                    <ul class="package-size">
                                        <li>5” x 7” (3)</li>
                                        <li>2 1/2” x 3 1/2” (2)</li>
                                        <li>2 1/2” x 1 3/4” (4)</li>
                                        <li>6” x 4” (2)</li>
                                        <li>2” x 3” (4)</li>
                                        <li>3/4” x 1” (5)</li>
                                        <li>10” x 8” (1)</li>
                                    </ul>                                   
                                    <div class="package-image">
                                        <a href="images/package5.jpg" data-lightbox="example-set"><img src="{{ asset('public/assets/images/package5.jpg') }}" alt="" />  </a>
                                    </div>
                                </div>
                                
                                <div class="packages-list-right">
                                    <div class="numbers-box"><input type="text" name="french-hens" id="french-hens" value="1"></div>
                                </div>
                            </div>
                            
                            <div class="packages-list">
                                <div class="packages-list-left">
                                    <h3 class="package-title">Pack A <span>$56</span></h3>
                                    <ul class="package-size">
                                        <li>5” x 7” (3)</li>
                                        <li>2 1/2” x 3 1/2” (2)</li>
                                        <li>2 1/2” x 1 3/4” (4)</li>
                                        <li>6” x 4” (2)</li>
                                        <li>2” x 3” (4)</li>
                                        <li>3/4” x 1” (5)</li>
                                        <li>10” x 8” (1)</li>
                                    </ul>                                   
                                    <div class="package-image">
                                        <a href="images/package6.jpg" data-lightbox="example-set"><img src="{{ asset('public/assets/images/package6.jpg') }}" alt="" />  </a>
                                    </div>
                                </div>
                                
                                <div class="packages-list-right">
                                    <div class="numbers-box"><input type="text" name="french-hens" id="french-hens" value="1"></div>
                                </div>
                            </div>
                        
                        </div>
                        
                    </div>
                  </div>
                  </div>
                </div>
                
                <div class="col-sm-5">
                    <div class="input-box">
                        <label>Fuill Name</label>
                        <input type="text" placeholder="Enter your full name" value="">
                    </div>
                           <div class="input-box">
                        <label>Grade</label>
                        <div class="select-box">
                            <select>
                                <option>Select the Grade</option>
                                <option>Select the Grade</option>
                                <option>Select the Grade</option>
                            </select>
                        </div>
                    </div>
                  <div class="input-box">
                        <label>Message</label>
                        <textarea rows="3" cols="5">Enter the message</textarea>
                    </div>
                    
                          <div class="input-box">
                        <label>School Name</label>
                        <input type="text" placeholder="Enter school name" value="">
                    </div>
                    
                    <div class="input-box">
                        <label>Job Date</label>
                        <input type="text" placeholder="Enter job date" value="">
                    </div>
                    
                    <div class="id-text">
                        <span>Note</span> : You must return a family envelope on photo day so we know your children required a family photo. If you don not have one you can print a family order form found on the page.
                    </div>
                    
                    <div class="input-box">
                        <button type="button" class="cart-button">Add to cart</button>
                    </div>
                    
                    <h3 class="order-title">Shopping Cart</h3>
                    <div class="order-table miscellaneous-table">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Photo</th>
                                    <th>Size</th>
                                    <th>Quantity</th>
                                    <th>Price</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Athletics</td>
                                    <td>6” x 4”</td>
                                    <td>1</td>
                                    <td>$5</td>
                                    <td>
                                        <a href="javascript:void(0)" class="pencil-button"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                        <a href="javascript:void(0)" class="trash-button"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td>Athletics</td>
                                    <td>8” x 10”</td>
                                    <td>2</td>
                                    <td>$30</td>
                                    <td>
                                        <a href="javascript:void(0)" class="pencil-button"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                        <a href="javascript:void(0)" class="trash-button"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                    </td>                                
                                </tr>
                                
                                <tr>
                                    <td>Boys Hockey</td>
                                    <td>5” x 7”</td>
                                    <td>1</td>
                                    <td>$10</td>
                                    <td>
                                        <a href="javascript:void(0)" class="pencil-button"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                        <a href="javascript:void(0)" class="trash-button"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td>Debating</td>
                                    <td>6” x 4”</td>
                                    <td>1</td>
                                    <td>$5</td>
                                    <td>
                                        <a href="javascript:void(0)" class="pencil-button"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                        <a href="javascript:void(0)" class="trash-button"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="total" colspan="3">Credit Card Processing Fee</td>
                                    <td class="total" colspan="2">$1</td>
                                </tr>
                                
                                <tr>
                                    <td class="total" colspan="3">Shopping cart total</td>
                                    <td class="total" colspan="2">$151</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    
                    <div class="input-button">
                    <button type="button">Proceed to Checkout</button>
                    </div>
                
                </div>
            
            </div>
            </div>
        
        </div>          
        </div>
        </div>        
        
    </div>
    
    
    </div>
    <!--content end--> 
@stop

{{-- page level scripts --}}
@section('footer_scripts')    
    
@stop
