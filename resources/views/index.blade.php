@extends('layouts.login')
@section('content')
<div class="content-box">
    <div class="container">
        <h1 class="title">Sign In</h1>
        <div class="login-form">

          {!! Form::open(array('route'=>'dashboard')) !!}

                            <div class="form-group">
                                {!! Form::label('E-mail') !!}
                                {!! Form::text('email', null, 
                                array('required', 
                                'class'=>'form-control', 
                                'placeholder'=>'email')) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('Password') !!}
                                {!! Form::input('password', 'password', null, 
                                array('required', 
                                'class'=>'form-control', 
                                'placeholder'=>'Password')) !!}
                            </div>

                           <!--  <div class="login-button text-center">
                                {!! Form::submit('Login', 
                                array('class'=>'btn btn-primary')) !!}
                            </div> -->

                             <!-- <div class="remember-box">
                               {!! Form::label('Remember me') !!}
                                {!! Form::input('password', 'password', null, 
                                array('required', 
                                'class'=>'form-control', 
                                'placeholder'=>'Password')) !!}
                            </div> -->

                             <div class="login-button-box">
                               {!! Form::submit('Login', 
                                array('class'=>'send-button')) !!}
                                
                            </div>
                           {{ csrf_field() }}
                            
                             <div class="login-button-box">
                               {!! Form::submit('Facebook', array('class' => 'facebook-button button-social-login-facebook')) !!}
                            </div>

                            {!! Form::close() !!}

         <!--    <div class="login-input-box">
                <label>Username/Email</label>
                <div class="login-input">
                    <span><i class="fa fa-user" aria-hidden="true"></i></span>
                    <input type="text" placeholder="">
                </div>
            </div>
            <div class="login-input-box">
                <label>Password</label>
                <div class="login-input">
                    <span><i class="fa fa-gear" aria-hidden="true"></i></span>
                    <input type="text" placeholder="">
                </div>
            </div>
            <div class="remember-box">
                <input type="checkbox">	Remember me
            </div>
            <div class="forgot-box">
                <a href="javscript:void(0)"><i class="fa fa-unlock-alt" aria-hidden="true"></i>Forgot your password?</a>
            </div>
            <div class="login-button-box">
                <button type="button" class="send-button">Sign In</button>
                <button type="button" class="facebook-button button-social-login-facebook">Facebook </button>
            </div> -->                        
        </div>
    </div>
</div>
@endsection
<script>
    
    //Load the Facebook JS SDK
(function (d) { 
    var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement('script');
    js.id = id;
    js.async = true;
    js.src = "http://connect.facebook.net/en_US/all.js";
    ref.parentNode.insertBefore(js, ref);
}(document));

// Init the SDK upon load
window.fbAsyncInit = function () {
    FB.init({
        appId: '213702215678816', // App ID
        status: true, // check login status
        cookie: true, // enable cookies to allow the server to access the session
        xfbml: true, // parse XFBML
    });


// Specify the extended permissions needed to view user data
// The user will be asked to grant these permissions to the app (so only pick those that are needed)
    var permissions = [
        'email',
//        'user_likes',
//        'friends_likes',
        'user_about_me',
//        'friends_about_me',
//        'user_birthday',
//        'friends_birthday',
//        'user_education_history',
//        'friends_education_history',
//        'user_hometown',
//        'friends_hometown',
//        'user_relationships',
//        'friends_relationships',
//        'user_relationship_details',
//        'friends_relationship_details',
//        'user_location',
//        'friends_location',
//        'user_religion_politics',
//        'friends_religion_politics',
//        'user_website',
//        'friends_website',
//        'user_work_history',
//        'friends_work_history'
    ].join(',');

// Specify the user fields to query the OpenGraph for.
// Some values are dependent on the user granting certain permissions
    var fields = [
        'id',
        'name',
        'first_name',
        'middle_name',
        'last_name',
        'gender',
        //'locale',
        //'languages',
        'link',
//        'third_party_id',
//        'installed',
//        'timezone',
//        'updated_time',
//        'verified',
//        'age_range',
        'bio',
//        'birthday',
//        'cover',
//        'currency',
//        'devices',
//        'education',
        'email',
//        'hometown',
//        'interested_in',
//        'location',
//        'political',
//        'payment_pricepoints',
//        'favorite_athletes',
//        'favorite_teams',
//        'picture',
//        'quotes',
//        'relationship_status',
//        'religion',
//        'significant_other',
//        'video_upload_limits',
//        'website',
//        'work'
    ].join(',');

    function showDetails() {
        FB.api('/me', {fields: fields}, function (details) {
             var base_path= 'http://localhost/collectit/';
            var fb_url = base_path +"test";
            alert(fb_url);
            details['_token'] = "SOzQlGn9G70x4MUkMYR4xHI8MxlLwUiiMIdIMukV";
          alert(details['email']);
            $.ajax({
                        url: fb_url, 
                        asys:false,
                        data: details, 
                        type:"GET",                                          
                        success: function (data) { 
                           window.location.href = fb_url;
                        }
                    });

            $('#fb-login').attr('style', 'display:none;');
        });
    }


    //$('.button-social-login-facebook').click(function () {
     $(document).on('click', '.button-social-login-facebook', function () {
        //initiate OAuth Login
        FB.login(function (response) {
            // if login was successful, execute the following code
            if (response.authResponse) {
                showDetails();
            }
        }, {scope: permissions});
    });

};

// $(function () {
//     $(document).on('click', '.button-social-login-facebook', function () {
//         alert("hi")
//         $('#fb-login').trigger("click");
//         //alert('a');
//     });
// });

</script>