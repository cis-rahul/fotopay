@extends('layouts/master')

{{-- Page title --}}
@section('title')
Business
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/vendors/datatables/extensions/ColReorder/css/colReorder.bootstrap.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/vendors/datatables/extensions/Scroller/css/scroller.bootstrap.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/vendors/datatables/extensions/RowReorder/css/rowReorder.bootstrap.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/vendors/datatables/extensions/TableTools/css/dataTables.tableTools.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/vendors/datatables/extensions/Responsive/css/responsive.dataTables.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/vendors/datatables/extensions/bootstrap/dataTables.bootstrap.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/css/pages/tables.css') }}" />
@stop

{{-- Page content --}}
@section('content')
<!-- content -->
<!-- Trigger the modal with a button -->
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">        
        <h4 class="modal-title">Business</h4>
      </div>
      <div class="modal-body">
        <div class="panel panel-primary filterable">             
                <div class="panel-heading clearfix">
                    <div class="caption pull-left">
                        <i class="livicon" data-name="list" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Business List
                    </div>
                    <div class="btn-group pull-right">
                        <a href="{{route('business.create')}}" id="add_new_business" name="add_new_business" class=" btn btn-warning">Add New Business</a>                                
                    </div>
                </div>
                <div class="panel-body table-responsive">
                    @if (session('flash_alert_notice'))
                    <div class="flash-message">
                        <div class="alert alert-{{session('flash_action')}}">
                            <p>{{session('flash_alert_notice')}}</p>
                        </div>
                    </div>
                    @endif                    

                    <table class="table table-striped table-bordered" id="table_business">
                        <thead>
                            <tr>                                
                                <th style="width: 70%;">Business Name</th>
                                <th style="text-align: center;width: 30%;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($business))                                
                                @foreach($business as $objbus)                                
                                <tr>                                
                                    <td><a href="{{route("bus_select", $objbus['bus_id'])}}">{{$objbus['bus_trading_name']}}</a></td>                                    
                                    <td style="text-align: center;">
                                        <a href="{{route("business.edit", $objbus['bus_id'])}}" class="label label-sm label-success">Edit</a>
                                    </td>
                                </tr>                                
                                @endforeach                            
                            @endif
                        </tbody>
                    </table>
                </div>
                <!-- END SAMPLE TABLE PORTLET-->
            </div>
      </div>      
    </div>

  </div>
</div>
@stop
{{-- page level scripts --}}
@section('footer_scripts')
<script type="text/javascript" src="{{ asset('public/assets/vendors/datatables/js/jquery.dataTables.min.js') }}" ></script>
<script type="text/javascript" src="{{ asset('public/assets/vendors/datatables/extensions/TableTools/js/dataTables.tableTools.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/assets/vendors/datatables/extensions/Scroller/js/dataTables.scroller.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/assets/vendors/datatables/extensions/RowReorder/js/dataTables.rowReorder.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/assets/vendors/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/assets/vendors/datatables/extensions/bootstrap/dataTables.bootstrap.min.js') }}" ></script>
<script type="text/javascript" src="{{ asset('public/assets/vendors/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#myModal').modal({backdrop: 'static', keyboard: false})  
    });    
    $("#table_business").dataTable({                                                                                        
        "dom":' <"search"f><"top"l>rt<"bottom"ip><"clear">'
    });    
</script>
@stop