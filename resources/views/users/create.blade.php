@extends('layouts/master')
{{-- Page title --}}
@section('title')
User
@parent
@stop
{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/vendors/x-editable/css/bootstrap-editable.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/vendors/magnifier/css/bootstrap-magnify.min.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/vendors/iCheck/skins/all.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/css/pages/user_profile.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/vendors/datatables/extensions/ColReorder/css/colReorder.bootstrap.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/vendors/datatables/extensions/Scroller/css/scroller.bootstrap.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/vendors/datatables/extensions/RowReorder/css/rowReorder.bootstrap.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/vendors/datatables/extensions/TableTools/css/dataTables.tableTools.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/vendors/datatables/extensions/Responsive/css/responsive.dataTables.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/vendors/datatables/extensions/bootstrap/dataTables.bootstrap.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/css/pages/tables.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/vendors/multiselect/css/bootstrap-multiselect.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/css/bootstrap-datetimepicker.css') }}"/>
<input type="hidden" id="csrf_token" name="csrf_token" value="{{ csrf_token() }}"/>
<style type="text/css">
    #inactive_user_table_filter .input-sm, #inactive_user_table2_filter .input-sm, #inactive_user_table3_filter .input-sm, #pending_task_filter .input-sm{
        width: 85px !important;
    }
    .paging_numbers, .dataTables_info {        
        text-align: center !important;
    }
    .form-group.required .control-label:after { 
        content:"*";
        color:red;
    }    
    input#profile_picture {
        height: 33px !important;
        border: 1px solid #ccc;

        width: 100%;
        border-radius: 5px;
        -webkit-padding-before: 5px !important;
        -webkit-padding-start: 5px !important;
        padding: 0px;
    } 
</style>
@stop
{{-- Page content --}}
@section('content')
<?php
$userprofile = (session('tab') == "Userprofile") ? 'active' : "";
$account = (session('tab') == "Account") ? 'active' : "";
if (empty($account)) {
    $userprofile = 'active';
}
?>
<section class="content-header">    
    <h1>User Detail</h1>    
</section>
<section class="content">
    <div  class="row ">
        <div class="col-md-12">
            <div class="row ">                
                <div class="col-md-12">
                    <ul class="nav nav-tabs ul-edit responsive">                        
                        <li class="{{$userprofile}}">
                            <a href="#tab-userprofile" data-toggle="tab">
                                User Profile
                            </a>
                        </li>
                        <li class="{{$account}}">
                            <a href="#tab-account" data-toggle="tab">
                                Business Accounts
                            </a>
                        </li>                        
                    </ul>                    
                    <div class="tab-content">
                        <div id="tab-userprofile" class="tab-pane fade in {{$userprofile}}">

                            @if (!empty($errors->all()))                                            
                            <div class="flash-message">
                                <div class="alert alert-warning">
                                    @foreach ($errors->all() as $error)
                                    <p>{{ $error }}</p>
                                    @endforeach
                                </div>
                            </div>
                            @endif
                            @if (session('flash_alert_notice'))
                            <div class="flash-message">
                                <div class="alert alert-{{session('flash_action')}}">
                                    <p>{{session('flash_alert_notice')}}</p>
                                </div>
                            </div>
                            @endif
                            <div class="row">
                                <form action="{{route('userprofile.update', $userid)}}" method="POST" role="form" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                    <input type="hidden" name="account_id" value="{{$userid}}" />
                                    <input type="hidden" name="_method" value="PUT" />
                                    <div class="row">
                                        <div class="col-xs-12 col-md-4">
                                            <div class="form-group {{ $errors->first('username', 'has-error') }}">
                                                <label class="control-label">User name</label>
                                                <input type="text" name="username" id="username" class="form-control input-md" placeholder="User Name" tabindex="4" value="{{$user_details['username']}}"/>
                                                {!! $errors->first('username', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-4">
                                            <div class="form-group {{ $errors->first('email', 'has-error') }}">
                                                <label class="control-label">Email</label>
                                                <input type="text" name="email" id="email" class="form-control input-md" placeholder="Email" tabindex="4" value="{{$user_details['email']}}"/>
                                                {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-4">
                                            <div class="form-group {{ $errors->first('role', 'has-error') }}">
                                                <label class="control-label">Role</label>
                                                <select id="role" name="role" class="form-control">
                                                    <option value="">Please select</option>
                                                    @if(!empty($user_roles))
                                                    @foreach($user_roles as $objUserRoles)
                                                    @if($objUserRoles->id == $user_details['role_id'])
                                                    <option value="{{$objUserRoles->id}}" selected="selected">{{$objUserRoles->role_name}}</option>
                                                    @else
                                                    <option value="{{$objUserRoles->id}}">{{$objUserRoles->role_name}}</option>
                                                    @endif
                                                    @endforeach    
                                                    @endif
                                                </select>                                        
                                                {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-4">
                                            <div class="form-group required {{ $errors->first('firstname', 'has-error') }}">
                                                <label class="control-label">First Name</label>
                                                <input type="text" name="firstname" id="firstname" class="form-control input-md" placeholder="First Name" tabindex="4" value="{{$user_details['firstname']}}"/>
                                                {!! $errors->first('firstname', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-4">
                                            <div class="form-group {{ $errors->first('lastname', 'has-error') }}">
                                                <label class="control-label">Last Name</label>
                                                <input type="text" name="lastname" id="lastname" class="form-control input-md" placeholder="Last Name" tabindex="4" value="{{$user_details['lastname']}}"/>
                                                {!! $errors->first('lastname', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-4">
                                            <div class="form-group required {{ $errors->first('id_number', 'has-error') }}">
                                                <label class="control-label">ID Number</label>
                                                <input type="text" name="id_number" id="id_number" class="form-control input-md" placeholder="ID Number" tabindex="4" value="{{$user_details['id_number']}}"/>
                                                {!! $errors->first('id_number', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-4">
                                            <div class="form-group {{ $errors->first('dob', 'has-error') }}">
                                                <label class="control-label">Date of birth</label>
                                                <input type="text" name="dob" id="dob" class="form-control input-md" placeholder="Date of birth" tabindex="4" value="{{$user_details['dateofbirth']}}"/>
                                                {!! $errors->first('dob', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-4">
                                            <div class="form-group {{ $errors->first('cell', 'has-error') }}">
                                                <label class="control-label">Mobile No.</label>
                                                <input type="text" name="cell" id="cell" class="form-control input-md" placeholder="Mobile No." tabindex="4" value="{{$user_details['cell']}}"/>
                                                {!! $errors->first('cell', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-4">
                                            <div class="form-group {{ $errors->first('landline', 'has-error') }}">
                                                <label class="control-label">Landline</label>
                                                <input type="text" name="landline" id="landline" class="form-control input-md" placeholder="Landline" tabindex="4" value="{{$user_details['landline']}}"/>
                                                {!! $errors->first('landline', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-4">
                                            <div class="form-group {{ $errors->first('suburb', 'has-error') }}">
                                                <label class="control-label">Suburb</label>
                                                <input type="text" name="suburb" id="suburb" class="form-control input-md auto" placeholder="Suburb" tabindex="4" value="{{$user_details['suburb']}}"/>
                                                {!! $errors->first('suburb', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-4">
                                            <div class="form-group {{ $errors->first('town', 'has-error') }}">
                                                <label class="control-label">Town</label>
                                                <input type="text" name="town" id="town" class="form-control input-md" placeholder="Town" tabindex="4" value="{{$user_details['town']}}"/>
                                                {!! $errors->first('town', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-4">
                                            <div class="form-group {{ $errors->first('country', 'has-error') }}">
                                                <label class="control-label">Country</label>
                                                <input type="text" name="country" id="country" class="form-control input-md" placeholder="Country" tabindex="4" value="{{$user_details['country']}}"/>
                                                {!! $errors->first('country', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-4">
                                            <div class="form-group {{ $errors->first('postalcode', 'has-error') }}">
                                                <label class="control-label">Postal Code</label>
                                                <input type="text" name="postalcode" id="postalcode" class="form-control input-md" placeholder="Postal Code" tabindex="4" value="{{$user_details['postalcode']}}"/>
                                                {!! $errors->first('postalcode', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-4">
                                            <div class="form-group {{ $errors->first('gender', 'has-error') }}">
                                                <label class="control-label">Gender</label>
                                                <select id="gender" name="gender" class="form-control input-md">
                                                    <option value="">Please select</option>
                                                    <option value="m" <?php echo ($user_details['gender'] == 'm') ? "selected='selected'" : ""; ?>>Male</option>
                                                    <option value="f" <?php echo ($user_details['gender'] == 'f') ? "selected='selected'" : ""; ?>>Female</option>
                                                </select>
                                                {!! $errors->first('gender', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-4">
                                            <div class="form-group {{ $errors->first('picture', 'has-error') }}">
                                                <label class="control-label">Picture</label>
                                                <input type="file" name="profile_picture" id="profile_picture" class="input-md" placeholder="Picture" tabindex="4"/>
                                                {!! $errors->first('profile_picture', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">                                
                                        <div class="col-xs-12 col-md-8">
                                            <div class="form-group {{ $errors->first('T&Cs', 'has-error') }}">
                                                <label class="control-label">Terms and Conditions</label> <br/>
                                                <input type="checkbox" name="term" value="1" <?php echo ($user_details['termsaccepted'] == 1) ? "checked='checked'" : "" ?>> I acknowledge that I have read and understand the Terms and Conditions
                                                {!! $errors->first('T&Cs', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>                                                                
                                        <div class="col-xs-12 col-md-4">
                                            <div class="form-group {{ $errors->first('password', 'has-error') }}">
                                                <label class="control-label">New Password</label>
                                                <input type="text" name="new_password" id="new_password" class="input-md form-control" placeholder="New Password" tabindex="4"/>
                                                {!! $errors->first('new_password', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>                         
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-4 col-md-4">
                                        </div>
                                        <div class="col-xs-4 col-md-4">
                                            <input type="submit" class="btn btn-primary btn-block btn-md btn-responsive" value="Submit">
                                        </div>
                                        <div class="col-xs-4 col-md-4">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div id="tab-account" class="tab-pane fade in {{$account}}">
                            <div class="panel-body table-responsive">
                                @if (session('flash_alert_notice'))
                                <div class="flash-message">
                                    <div class="alert alert-{{session('flash_action')}}">
                                        <p>{{session('flash_alert_notice')}}</p>
                                    </div>
                                </div>
                                @endif
                                <table class="table table-striped table-bordered" id="table_business">
                                    <thead>
                                        <tr>                                
                                            <th style="width: 70%;">Business Name</th>
                                            <th style="text-align: center;width: 30%;">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(!empty($business))                                
                                        @foreach($business as $objbus)
                                        <tr>
                                            <td><a href="{{route("bus_select", $objbus['bus_id'])}}" class="select-anc">{{$objbus['bus_trading_name']}}</a></td>                                    
                                            <td style="text-align: center;">
                                                @if(!isset($restriction[$objbus['bus_id']]) || !is_array($restriction[$objbus['bus_id']]))
                                                <a href="{{route("business.edit", $objbus['bus_id'])}}" class="label label-sm label-success edit-anc">Edit</a>
                                                @elseif(in_array('4', $restriction[$objbus['bus_id']]) || in_array('5', $restriction[$objbus['bus_id']]))

                                                @else
                                                <a href="{{route("business.edit", $objbus['bus_id'])}}" class="label label-sm label-success edit-anc">Edit</a> 
                                                @endif

                                            </td>
                                        </tr>                                
                                        @endforeach                            
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>                                               
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop
{{-- page level scripts --}}
@section('footer_scripts')
<script type="text/javascript" src="{{ asset('public/assets/vendors/multiselect/js/bootstrap-multiselect.js') }}" ></script>
<script type="text/javascript" src="{{ asset('public/assets/js/bootstrap-datepicker.js')}}" ></script>
<script type="text/javascript" src="{{ asset('public/assets/js/moment-with-locales.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/assets/js/bootstrap-datetimepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/assets/vendors/datatables/js/jquery.dataTables.min.js') }}" ></script>
<script type="text/javascript" src="{{ asset('public/assets/vendors/datatables/extensions/TableTools/js/dataTables.tableTools.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/assets/vendors/datatables/extensions/Scroller/js/dataTables.scroller.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/assets/vendors/datatables/extensions/RowReorder/js/dataTables.rowReorder.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/assets/vendors/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/assets/vendors/datatables/extensions/bootstrap/dataTables.bootstrap.min.js') }}" ></script>
<script type="text/javascript" src="{{ asset('public/assets/vendors/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/assets/js/dashboard.js') }}"></script>
<script type='text/javascript'>
$(document).ready(function() {
    $("#table_business").dataTable({                                                                                        
        "dom":' <"search"f><"top"l>rt<"bottom"ip><"clear">'
    });
    $("#dob").datepicker({
        format: 'yyyy-mm-dd'
    });
    $(".auto").autocomplete({
        source: "{{route('get_suburb')}}",
        minLength: 1,
        change: function() {
            $(".auto").change();
        }
    });
    $(".auto").change(function() {
        $("#town").val("");
        $("#postalcode").val("");
        $("#country").val("");
        var url = "{{route('suburb_search')}}";
        var csrf_token = '{{ csrf_token() }}';
        $.ajax({
            url: url,
            type: 'POST',
            data: ({suburb_name: $(this).val(), _token: csrf_token}),
            dataType: 'JSON',
            success: function(res) {
                console.log(res['data']);
                if (res['status']) {
                    $("#suburb").val(res['data']['suburb_name']);
                    $("#town").val(res['data']['city_name']);
                    $("#postalcode").val(res['data']['postal_code']);
                    $("#country").val(res['data']['country_name']);
                }
            },
            error: function(res) {

            }
        });
    });

    $("#country").change(function() {
        $("#region").html("<option value=''>Please select region</option>");
        var country_id = $(this).val();
        var urlroute = "{{URL::to('getregionbyctyid')}}";
        var url = urlroute + "/" + country_id;
        $.ajax({
            url: url,
            data: {},
            dataType: 'JSON',
            success: function(res) {
                if (res['status']) {
                    var html = "<option value=''>Please select region</option>";
                    $.each(res.data, function(key, val) {
                        console.log(val);
                        html += "<option value='" + key + "'>" + val + "</option>";
                    });
                    $("#region").html(html);
                }
            },
            error: function() {

            }
        });
    });
});
</script>
<style type="text/css">
    #pending_task_wrapper{
        margin-top: 10px;
    }
    .additional_search{
        text-align: right;
    }
</style>
@stop