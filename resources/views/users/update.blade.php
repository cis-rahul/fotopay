@extends('layouts/master')

{{-- Page title --}}
@section('title')
    Business
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')

    <link href="{{ asset('public/assets/css/pages/form2.css') }}" rel="stylesheet"/>
    <link href="{{ asset('public/assets/css/pages/form3.css') }}" rel="stylesheet"/>
    <link href="{{ asset('public/assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('public/assets/vendors/intl-tel-input/build/css/intlTelInput.css') }}" rel="stylesheet"/>
    <link href="{{ asset('public/assets/vendors/validation/dist/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
    <style type='text/css'>
        .form-group.required .control-label:after { 
            content:"*";
            color:red;
        }
    </style>
@stop

{{-- Page content --}}
@section('content')
    <section class="content-header">
        <!--section starts-->
        <h1>Business</h1>
<!--        <ol class="breadcrumb">
            <li>
                <a href="{{ route('dashboard') }}">
                    <i class="livicon" data-name="home" data-size="14" data-loop="true"></i> Dashboard
                </a>
            </li>
            <li>
                <a href="{{route('business.index')}}">Business</a>
            </li>
            <li class="active">Update</li>
        </ol>-->
    </section>
    <!--section ends-->
    <section class="content">
        <!--main content-->
        <div class="row">            
            <div class="col-md-12">                
                <div class="panel panel-warning">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="livicon" data-name="rocket" data-size="16" data-loop="true" data-c="#fff"
                               data-hc="white"></i>
                            Update
                        </h3>
                        <span class="pull-right clickable">
                            <i class="glyphicon glyphicon-chevron-up"></i>
                        </span>
                    </div>
                    <div class="panel-body">
                        <div class="row">                            
                            <form action="{{URL::to('business')}}/{{$business_data['bus_id']}}" method="POST" role="form">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                <input name="_method" type="hidden" value="PUT">
                                <div class="row">
                                    <div class="col-xs-12 col-md-4">                                        
                                        <div class="form-group {{ $errors->first('global_group', 'has-error') }}">
                                            <label class="control-label">Global Group</label>
                                            <select id="global_group" name="global_group" class="form-control input-md">
                                                <option value="">Please select global group</option>
                                                @foreach($global_groups as $objGroups)
                                                <?php if($objGroups['glogroup_id'] == $business_data['bus_global_group_id']){$selected='selected="selected"';}else{$selected='';}  ?>
                                                    <option value="{{@$objGroups['glogroup_id']}}" <?php echo $selected; ?> >{{@$objGroups['glogroup_title']}}</option>
                                                @endforeach
                                            </select>
                                            {!! $errors->first('global_group', '<span class="help-block">:message</span>') !!}                                                                                       
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-4">
                                        <div class="form-group {{ $errors->first('country', 'has-error') }}">
                                            <label class="control-label">Country</label>
                                            <select id="country" name="country" class="form-control input-md">
                                                <option value="">Please select country</option>
                                            </select>
                                            {!! $errors->first('country', '<span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-4">
                                        <div class="form-group {{ $errors->first('region', 'has-error') }}">
                                            <label class="control-label">Region</label>
                                            <select id="region" name="region" class="form-control input-md">
                                                <option value="">Please select region</option>
                                            </select>
                                            {!! $errors->first('region', '<span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-4">
                                        <div class="form-group required {{ $errors->first('trading', 'has-error') }}">
                                            <label class="control-label">Trading / Individual</label>
                                            <input type="text" name="trading" id="trading" class="form-control input-md" placeholder="Trading / Individual" value="{{@$business_data['bus_trading_name']}}">
                                            {!! $errors->first('trading', '<span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>                                    
                                    <div class="col-xs-12 col-md-4">
                                        <div class="form-group {{ $errors->first('business_type', 'has-error') }}">
                                            <label class="control-label">Business Type</label>
                                            <select id="business_type" name="business_type" class="form-control input-md">
                                                <option value="">Please select business type</option>                                                
                                                @foreach($business_type as $objBusinessType)                                                
                                                    <?php if($objBusinessType['bus_type_id'] == $business_data['bus_type_id']){ $selected='selected="selected"';}else{$selected = "";} ?>
                                                    <option value="{{@$objBusinessType['bus_type_id']}}" <?php echo @$selected; ?>>{{@$objBusinessType['bus_type_name']}}</option>
                                                @endforeach                                                
                                            </select>
                                            {!! $errors->first('business_type', '<span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-4">
                                        <div class="form-group {{ $errors->first('registration_number', 'has-error') }}">
                                            <label class="control-label">Registration Number</label>
                                            <input type="text" name="registration_number" id="registration_number" class="form-control input-md" placeholder="Registration Number" value="{{@$business_data['bus_registration_number']}}">
                                            {!! $errors->first('registration_number', '<span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-4">
                                        <div class="form-group required {{ $errors->first('registered_name', 'has-error') }}">
                                            <label class="control-label">Registered Name</label>
                                            <input type="text" name="registered_name" id="registered_name" class="form-control input-md" placeholder="Registered Name" value="{{@$business_data['bus_registered_name']}}">
                                            {!! $errors->first('registered_name', '<span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-4">
                                        <div class="form-group {{ $errors->first('business_sector', 'has-error') }}">
                                            <label class="control-label">Business Sector</label>
                                            <select id="business_type" name="business_sector" class="form-control input-md">
                                                <option value="">Please select business sector</option>
                                                @foreach($business_sector as $objBusinessSector)
                                                
                                                    <?php if($objBusinessSector['bus_sector_id'] == $business_data['bus_sector_id']){ $selected="selected='selected'";}else{$selected = "";} ?>
                                                    <option value="{{$objBusinessSector['bus_sector_id']}}" <?php echo @$selected; ?>>{{$objBusinessSector['bus_sector_name']}}</option>
                                                @endforeach
                                            </select>
                                            {!! $errors->first('business_sector', '<span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-4">
                                        <div class="form-group {{ $errors->first('vat_number', 'has-error') }}">
                                            <label class="control-label">Vat Number</label>
                                            <input type="text" name="vat_number" id="vat_number" class="form-control input-md" placeholder="Vat Number" value="{{@$business_data['bus_vat_number']}}">
                                            {!! $errors->first('vat_number', '<span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-4">
                                        <div class="form-group required {{ $errors->first('mobile_1', 'has-error') }}">
                                            <label class="control-label">Mobile 1</label>
                                            <input type="text" name="mobile_1" id="mobile_1" class="form-control input-md" placeholder="Mobile 1" value="{{@$business_data['bus_mobile1']}}">
                                            {!! $errors->first('mobile_1', '<span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-4">
                                        <div class="form-group required {{ $errors->first('email_1', 'has-error') }}">
                                            <label class="control-label">Email 1</label>
                                            <input type="text" name="email_1" id="email_1" class="form-control input-md" placeholder="Email 1" value="{{@$business_data['bus_email1']}}">
                                            {!! $errors->first('email_1', '<span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-4">
                                        <div class="form-group {{ $errors->first('land_line', 'has-error') }}">
                                            <label class="control-label">Land Line</label>
                                            <input type="text" name="land_line" id="land_line" class="form-control input-md" placeholder="Land Line" value="{{@$business_data['bus_landline']}}">
                                            {!! $errors->first('land_line', '<span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-4">
                                        <div class="form-group {{ $errors->first('mobile_2', 'has-error') }}">
                                            <label class="control-label">Mobile 2</label>
                                            <input type="text" name="mobile_2" id="mobile_2" class="form-control input-md" placeholder="Mobile 2" value="{{@$business_data['bus_mobile2']}}">
                                            {!! $errors->first('mobile_2', '<span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-4">
                                        <div class="form-group {{ $errors->first('email_2', 'has-error') }}">
                                            <label class="control-label">Email 2</label>
                                            <input type="text" name="email_2" id="email_2" class="form-control input-md" placeholder="Email 2" value="{{@$business_data['bus_email2']}}">
                                            {!! $errors->first('email_2', '<span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-4">
                                        <div class="form-group {{ $errors->first('fax', 'has-error') }}">
                                            <label class="control-label">Fax</label>
                                            <input type="text" name="fax" id="fax" class="form-control input-md" placeholder="Fax" value="{{@$business_data['bus_fax']}}">
                                            {!! $errors->first('fax', '<span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-4">
                                        <div class="form-group ">                                            
                                            <div class="row ">
                                                <div class="col-md-12 {{ $errors->first('website_url', 'has-error') }}" style='padding-left: 0px; padding-right: 0px;'>
                                                    <label class="control-label">Web Url</label>
                                                    <input type="text" name="website_url" id="website_url" class="form-control input-md" placeholder="Web Url" value="{{@$business_data['bus_website']}}">
                                                    {!! $errors->first('website_url', '<span class="help-block">:message</span>') !!}
                                                </div>
                                                <div class="col-md-2" style='padding-left: 0px; padding-right: 0px;'>                                                    
                                                    <div class="radio">
                                                        <label>                                                             
                                                            <input type="radio" value="sms" name="notification" <?php if($business_data['bus_notifysms']=='1'){echo 'checked="checked"';} ?>>
                                                            SMS
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" value="email" name="notification" <?php if($business_data['bus_notifyemail']=='1'){echo 'checked="checked"';}?>>
                                                            Email
                                                        </label>
                                                    </div>
                                                </div>
                                                {!! $errors->first('notification', '<span class="help-block">:message</span>') !!}
                                            </div>                                            
                                        </div>
                                    </div>                                    
                                    <div class="col-xs-12 col-md-8">
                                        <div class="form-group {{ $errors->first('comment', 'has-error') }}">
                                            <label class="control-label">Comments</label>
                                            <textarea id="comment" name="comment" class="form-control input-md" row="4" placeholder="Comment">{{@$business_data['bus_comments']}}</textarea>
                                            {!! $errors->first('comment', '<span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>                                
                                <div class="row">
                                    <div class="col-xs-4 col-md-4">
                                    </div>
                                    <div class="col-xs-4 col-md-4">
                                        <input type="submit" class="btn btn-primary btn-block btn-md btn-responsive" value="Update">
                                    </div>
                                    <div class="col-xs-4 col-md-4">
                                    </div>
                                </div>
                            </form>
                        </div>                        
                    </div>
                </div>                
            </div>
        </div>
        <!--row ends-->
    </section>
    <!-- content -->
    </aside>
    <!-- right-side -->
    </div>
    <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Return to top"
       data-toggle="tooltip" data-placement="left">
        <i class="livicon" data-name="plane-up" data-size="18" data-loop="true" data-c="#fff" data-hc="white"></i>
    </a>
    <!-- global js -->
    <script src="{{ asset('public/assets/js/jquery-1.11.1.min.js') }}" type="text/javascript"></script>
<!--    <script src="{{ asset('public/assets/js/bootstrap.min.js') }}" type="text/javascript"></script>
    livicons
    <script src="{{ asset('public/assets/vendors/livicons/minified/raphael-min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/livicons/minified/livicons-1.4.min.js') }}"></script>
    <script src="{{ asset('public/assets/js/josh.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/assets/js/metisMenu.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/assets/vendors/holder-master/holder.js') }}"></script>-->
@stop

{{-- page level scripts --}}
@section('footer_scripts')    
    <script src="{{ asset('public/assets/vendors/jasny-bootstrap/js/jasny-bootstrap.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('public/assets/vendors/validation/dist/js/bootstrapValidator.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('public/assets/vendors/intl-tel-input/build/js/intlTelInput.min.js') }}"
            type="text/javascript"></script>
    {{--<script src="{{ asset('//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js') }}"></script>--}}
    <script src="{{ asset('public/assets/js/pages/validation.js') }}" type="text/javascript"></script>
    {{-- <script>
         $("#phone").intlTelInput();
     </script>
     --}}
     
    <script type='text/javascript'>
        $(document).ready(function(){
           var globalgroupid = "{{$business_data['bus_global_group_id']}}";
           var country_id = "{{$business_data['bus_country']}}";
           var region_id = "{{$business_data['bus_glogroup_region_id']}}";           
           $("#global_group").change(function(){
               $("#country").html("<option value=''>Please select country</option>");
               $("#region").html("<option value=''>Please select region</option>");               
               var urlroute = "{{URL::to('getcountrybygloid')}}";
               var globalgroupid = $(this).val();
               var url = urlroute + "/" + globalgroupid;               
               $.ajax({
                  url:url,
                  data:{},
                  dataType:'JSON',                  
                  success:function(res){                      
                      if(res['status']){
                          var html = "<option value=''>Please select country</option>";
                          $.each(res.data, function(key, val){
                              if(key == country_id){
                                  html += "<option value='" + key +"' selected='selected'>"+ val['country_name'] +"</option>";
                              }else{
                                  html += "<option value='" + key +"'>"+ val['country_name'] +"</option>";
                              }                              
                          });
                          $("#country").html(html);
                          $("#country").change();
                      }
                  },
                  error:function(){
                      
                  }
               });
           });
           $("#country").change(function(){
               $("#region").html("<option value=''>Please select region</option>");
               var country_id = $(this).val();
               var urlroute = "{{URL::to('getregionbyctyid')}}";
               var url = urlroute + "/" + country_id;               
               $.ajax({
                  url:url,
                  data:{},
                  dataType:'JSON',                  
                  success:function(res){                      
                      if(res['status']){
                          var html = "<option value=''>Please select region</option>";
                          $.each(res.data, function(key, val){
                              if(region_id == key){
                                html += "<option value='" + key +"' selected='selected'>"+ val +"</option>";  
                              }else{
                                html += "<option value='" + key +"'>"+ val +"</option>";
                              }
                          });
                          $("#region").html(html);                          
                      }
                  },
                  error:function(){
                      
                  }
               });
           });
           if(globalgroupid != ""){               
               $("#global_group").trigger('change');
           }
        });
    </script>
     
     
@stop
