@extends('layouts/master')
{{-- Page title --}}
@section('title')
Users
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link href="{{ asset('public/assets/css/pages/form2.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/css/pages/form3.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/vendors/intl-tel-input/build/css/intlTelInput.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/vendors/validation/dist/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
<style type='text/css'>
    .form-group.required .control-label:after { 
        content:"*";
        color:red;
    }    
    input#profile_picture {
        height: 33px !important;
        border: 1px solid #ccc;

        width: 100%;
        border-radius: 5px;
        -webkit-padding-before: 5px !important;
        -webkit-padding-start: 5px !important;
        padding: 0px;
    }          
</style>
@stop

{{-- Page content --}}
@section('content')

<section class="content-header">
    <!--section starts-->
    <h1>Register User</h1>
<!--    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="livicon" data-name="home" data-size="14" data-loop="true"></i> Dashboard
            </a>
        </li>
        <li>
            Register User Detail
        </li>        
    </ol>-->
</section>
<!--section ends-->
<section class="content">
    <!--main content-->
    <div class="row">            
        <div class="col-md-12">                
            <div class="panel panel-warning">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="livicon" data-name="user" data-size="16" data-loop="true" data-c="#fff"
                           data-hc="white"></i>
                        Enable or Disable User Account
                    </h3>
                    <span class="pull-right clickable">
                        <i class="glyphicon glyphicon-chevron-up"></i>
                    </span>
                </div>
                <div class="panel-body">
                    @if (!empty($errors->all()))                                            
                    <div class="flash-message">
                        <div class="alert alert-warning">
                            @foreach ($errors->all() as $error)
                            <p>{{ $error }}</p>
                            @endforeach
                        </div>
                    </div>
                    @endif
                    @if (session('flash_alert_notice'))
                    <div class="flash-message">
                        <div class="alert alert-{{session('flash_action')}}">
                            <p>{{session('flash_alert_notice')}}</p>
                        </div>
                    </div>
                    @endif
                    <div class="row">
                        <form action="{{route('user_update')}}" method="POST" role="form" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <input type="hidden" id="model_id" name="id" value="{{$userid}}"/>
                            <div class="row">
                                <div class="col-xs-12 col-md-4">
                                    <div class="form-group {{ $errors->first('username', 'has-error') }}">
                                        <label class="control-label">User name</label>
                                        <input type="text" name="username" id="username" class="form-control input-md" placeholder="User Name" tabindex="4" value="{{$user_details['username']}}"/>
                                        {!! $errors->first('username', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <div class="form-group {{ $errors->first('email', 'has-error') }}">
                                        <label class="control-label">Email</label>
                                        <input type="text" name="email" id="email" class="form-control input-md" placeholder="Email" tabindex="4" value="{{$user_details['email']}}"/>
                                        {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <div class="form-group {{ $errors->first('role', 'has-error') }}">
                                        <label class="control-label">Role</label>
                                        <select id="role" name="role" class="form-control">
                                            <option value="">Please select</option>
                                            @if(!empty($user_roles))
                                            @foreach($user_roles as $objUserRoles)
                                                @if($objUserRoles->id == $user_details['role_id'])
                                                <option value="{{$objUserRoles->id}}" selected="selected">{{$objUserRoles->role_name}}</option>
                                                @else
                                                <option value="{{$objUserRoles->id}}">{{$objUserRoles->role_name}}</option>
                                                @endif
                                            @endforeach    
                                            @endif
                                        </select>                                        
                                        {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-4">
                                    <div class="form-group required {{ $errors->first('firstname', 'has-error') }}">
                                        <label class="control-label">First Name</label>
                                        <input type="text" name="firstname" id="firstname" class="form-control input-md" placeholder="First Name" tabindex="4" value="{{$user_details['firstname']}}"/>
                                        {!! $errors->first('firstname', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <div class="form-group {{ $errors->first('lastname', 'has-error') }}">
                                        <label class="control-label">Last Name</label>
                                        <input type="text" name="lastname" id="lastname" class="form-control input-md" placeholder="Last Name" tabindex="4" value="{{$user_details['lastname']}}"/>
                                        {!! $errors->first('lastname', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <div class="form-group required {{ $errors->first('id_number', 'has-error') }}">
                                        <label class="control-label">ID Number</label>
                                        <input type="text" name="id_number" id="id_number" class="form-control input-md" placeholder="ID Number" tabindex="4" value="{{$user_details['id_number']}}"/>
                                        {!! $errors->first('id_number', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-4">
                                    <div class="form-group {{ $errors->first('dob', 'has-error') }}">
                                        <label class="control-label">Date of birth</label>
                                        <input type="text" name="dob" id="dob" class="form-control input-md" placeholder="Date of birth" tabindex="4" value="{{$user_details['dateofbirth']}}"/>
                                        {!! $errors->first('dob', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <div class="form-group {{ $errors->first('cell', 'has-error') }}">
                                        <label class="control-label">Mobile No.</label>
                                        <input type="text" name="cell" id="cell" class="form-control input-md" placeholder="Mobile No." tabindex="4" value="{{$user_details['cell']}}"/>
                                        {!! $errors->first('cell', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <div class="form-group {{ $errors->first('landline', 'has-error') }}">
                                        <label class="control-label">Landline</label>
                                        <input type="text" name="landline" id="landline" class="form-control input-md" placeholder="Landline" tabindex="4" value="{{$user_details['landline']}}"/>
                                        {!! $errors->first('landline', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-4">
                                    <div class="form-group {{ $errors->first('suburb', 'has-error') }}">
                                        <label class="control-label">Suburb</label>
                                        <input type="text" name="suburb" id="suburb" class="form-control input-md auto" placeholder="Suburb" tabindex="4" value="{{$user_details['suburb']}}"/>
                                        {!! $errors->first('suburb', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <div class="form-group {{ $errors->first('town', 'has-error') }}">
                                        <label class="control-label">Town</label>
                                        <input type="text" name="town" id="town" class="form-control input-md" placeholder="Town" tabindex="4" value="{{$user_details['town']}}"/>
                                        {!! $errors->first('town', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <div class="form-group {{ $errors->first('country', 'has-error') }}">
                                        <label class="control-label">Country</label>
                                        <input type="text" name="country" id="country" class="form-control input-md" placeholder="Country" tabindex="4" value="{{$user_details['country']}}"/>
                                        {!! $errors->first('country', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-4">
                                    <div class="form-group {{ $errors->first('postalcode', 'has-error') }}">
                                        <label class="control-label">Postal Code</label>
                                        <input type="text" name="postalcode" id="postalcode" class="form-control input-md" placeholder="Postal Code" tabindex="4" value="{{$user_details['postalcode']}}"/>
                                        {!! $errors->first('postalcode', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <div class="form-group {{ $errors->first('gender', 'has-error') }}">
                                        <label class="control-label">Gender</label>
                                        <select id="gender" name="gender" class="form-control input-md">
                                            <option value="">Please select</option>
                                            <option value="m" <?php echo ($user_details['gender'] == 'm') ? "selected='selected'" : ""; ?>>Male</option>
                                            <option value="f" <?php echo ($user_details['gender'] == 'f') ? "selected='selected'" : ""; ?>>Female</option>
                                        </select>
                                        {!! $errors->first('gender', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <div class="form-group {{ $errors->first('picture', 'has-error') }}">
                                        <label class="control-label">Picture</label>
                                        <input type="file" name="profile_picture" id="profile_picture" class="input-md" placeholder="Picture" tabindex="4"/>
                                        {!! $errors->first('profile_picture', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">                                
                                <div class="col-xs-12 col-md-8 col-sm-12">
                                    <div class="form-group">
                                        <?php                                        
                                        $deleted = "";
                                        $active = "";
                                        $inactive = "";
                                        
                                        if($user_details['deleted']=='1'){
                                            $deleted = "checked='checked'";
                                        }elseif($user_details['status']=='1'){
                                            $active = "checked='checked'";
                                        }else{
                                            $inactive = "checked='checked'";
                                        }
                                        ?>
                                        <label class="control-label"><input type="radio" id="model_status" name="status" value="1" <?php echo $active ?>/> Active</label>
                                        <label class="control-label"><input type="radio" id="model_status" name="status" value="0" <?php echo $inactive ?>/> Inactive</label>
                                        <label class="control-label"><input type="radio" id="model_deleted" name="status" value="2" <?php echo $deleted ?>/> Deleted</label>
                                    </div>                                    
                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <div class="form-group {{ $errors->first('password', 'has-error') }}">
                                        <label class="control-label">New Password</label>
                                        <input type="text" name="new_password" id="new_password" class="input-md form-control" placeholder="New Password" tabindex="4"/>
                                        {!! $errors->first('new_password', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-4 col-md-4">
                                </div>
                                <div class="col-xs-4 col-md-4">
                                    <input type="submit" class="btn btn-primary btn-block btn-md btn-responsive" value="Submit">
                                </div>
                                <div class="col-xs-4 col-md-4">
                                </div>
                            </div>
                        </form>
                    </div>                        
                </div>
            </div>                
        </div>
    </div>
    <!--row ends-->
</section>
<!-- content -->
</aside>
<!-- right-side -->
</div>
<a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Return to top"
   data-toggle="tooltip" data-placement="left">
    <i class="livicon" data-name="plane-up" data-size="18" data-loop="true" data-c="#fff" data-hc="white"></i>
</a>
<!-- global js -->
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<script type="text/javascript" src="{{ asset('public/assets/js/bootstrap-datepicker.js') }}" ></script>
<script type='text/javascript'>
$(document).ready(function() {
    $("#dob").datepicker({
        format: 'yyyy-mm-dd'
    });
    $(".auto").autocomplete({
        source: "{{route('get_suburb')}}",
        minLength: 1,
        change: function() {
            $(".auto").change();
        }
    });
    $(".auto").change(function() {
        $("#town").val("");
        $("#postalcode").val("");
        $("#country").val("");
        var url = "{{route('suburb_search')}}";
        var csrf_token = '{{ csrf_token() }}';
        $.ajax({
            url: url,
            type: 'POST',
            data: ({suburb_name: $(this).val(), _token: csrf_token}),
            dataType: 'JSON',
            success: function(res) {
                console.log(res['data']);
                if (res['status']) {
                    $("#suburb").val(res['data']['suburb_name']);
                    $("#town").val(res['data']['city_name']);
                    $("#postalcode").val(res['data']['postal_code']);
                    $("#country").val(res['data']['country_name']);
                }
            },
            error: function(res) {

            }
        });
    });

    $("#country").change(function() {
        $("#region").html("<option value=''>Please select region</option>");
        var country_id = $(this).val();
        var urlroute = "{{URL::to('getregionbyctyid')}}";
        var url = urlroute + "/" + country_id;
        $.ajax({
            url: url,
            data: {},
            dataType: 'JSON',
            success: function(res) {
                if (res['status']) {
                    var html = "<option value=''>Please select region</option>";
                    $.each(res.data, function(key, val) {
                        console.log(val);
                        html += "<option value='" + key + "'>" + val + "</option>";
                    });
                    $("#region").html(html);
                }
            },
            error: function() {

            }
        });
    });
});
</script>     
@stop