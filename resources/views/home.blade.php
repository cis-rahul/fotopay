@extends('layouts.master')
{{-- Page title --}}
@section('title')
    Message
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')

    
@stop

{{-- Page content --}}
@section('content')
 <div class="container">
    <div class="row">
    
        <h2 class="content-title">Online photo payment for<br/> <span>A-One fotomakers</span> and <span>Kids Pix</span></h2>
        
        <div class="login-list-box">
        <div class="row" id="owl-demo">
        
            <!--login list-->
            <div class="col-sm-4 login-col wow fadeInUp" data-wow-delay="0.2s">
                <div class="login-list">
                    <div class="login-icon">
                        <div class="inner-login-icon">
                            <img src="{{ asset('public/assets/images/icon1.png') }}" alt="" />
                        </div>                
                    </div>
                    
                    <div class="login-content">
                        <h3>Prepay</h3>
                        <p>Pre pay for photo packages. Use the sale code on your prepay envelope</p>
                        <p><a href="javascript:void(0)">Login<i class="fa fa-angle-right"></i></a></p>
                    </div>                    
                </div>
            </div>
            
            <!--login list-->
            <div class="col-sm-4 login-col wow fadeInUp" data-wow-delay="0.4s">
                <div class="login-list">
                    <div class="login-icon">
                        <div class="inner-login-icon">
                            <img src="{{ asset('public/assets/images/icon2.png') }}" alt="" />
                        </div>                
                    </div>
                    
                    <div class="login-content">
                        <h3>Miscellaneous</h3>
                        <p>Order photos such as leaders, sporting, teams and academic groups etc. Use your school job code.</p>
                        <p><a href="javascript:void(0)">Login<i class="fa fa-angle-right"></i></a></p>
                    </div>
                </div>
            </div>
            
            
            <!--login list-->
            <div class="col-sm-4 login-col wow fadeInUp" data-wow-delay="0.6s">
                <div class="login-list">
                    <div class="login-icon">
                        <div class="inner-login-icon">
                            <img src="{{ asset('public/assets/images/icon3.png') }}" alt="" />
                        </div>                
                    </div>
                    
                    <div class="login-content">
                        <h3>Re-Orders</h3>
                        <p>Re-Order photo packages and individual print.</p>
                        <p><a href="javascript:void(0)">Login<i class="fa fa-angle-right"></i></a></p>
                    </div>
                </div>
            </div>
            
            <!--login list-->
            <div class="col-sm-4 login-col wow fadeInUp" data-wow-delay="0.8s">
                <div class="login-list">
                    <div class="login-icon">
                        <div class="inner-login-icon">
                            <img src="{{ asset('public/assets/images/icon4.png') }}" alt="" />
                        </div>                
                    </div>
                    
                    <div class="login-content">
                        <h3>ID Cards</h3>
                        <p>Replacement ID Card</p>
                        <p><a href="javascript:void(0)">Login<i class="fa fa-angle-right"></i></a></p>
                    </div>
                </div>
            </div>
            
            
            <!--login list-->
            <div class="col-sm-4 login-col wow fadeInUp" data-wow-delay="1s">
                <div class="login-list">
                    <div class="login-icon">
                        <div class="inner-login-icon">
                            <img src="{{ asset('public/assets/images/icon5.png') }}" alt="" />
                        </div>                
                    </div>
                    
                    <div class="login-content">
                        <h3>Customer</h3>
                        <p>View your purchase histor</p>
                        <p><a href="javascript:void(0)">Login<i class="fa fa-angle-right"></i></a></p>
                    </div>
                </div>
            </div>
            
            <!--login list-->
            <div class="col-sm-4 login-col wow fadeInUp" data-wow-delay="1.2s">
                <div class="login-list">
                    <div class="login-icon">
                        <div class="inner-login-icon">
                            <img src="{{ asset('public/assets/images/icon6.png') }}" alt="" />
                        </div>                
                    </div>
                    
                    <div class="login-content">
                        <h3>Admin</h3>
                        <p>Administration use only</p>
                        <p><a href="javascript:void(0)">Login<i class="fa fa-angle-right"></i></a></p>
                    </div>
                </div>
            </div>
            
            
        </div>    
        </div>
        
        <div class="other-product">
        <div class="row">
            <div class="col-sm-8 product-left wow fadeInLeft">
                <a href="javascript:void(0)"><img src="{{ asset('public/assets/images/product1.png') }}" alt="" /></a>
            </div>
            
            <div class="col-sm-4 product-right wow fadeInRight">
                <a href="javascript:void(0)"><img src="{{ asset('public/assets/images/product2.png') }}" alt="" /></a>
            </div>
        </div>
        </div>
    
    
    </div>
    </div>         
@stop

{{-- page level scripts --}}
@section('footer_scripts')    
    
@stop
