<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login | Fotopay</title>
    <!--global css starts-->
    <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/css/bootstrap.min.css') }}">
    <link rel="shortcut icon" href="{{ asset('public/assets/images/favicon.png') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('public/assets/images/favicon.png') }}" type="image/x-icon">
    <!--end of global css-->
    <!--page level css starts-->
    <link type="text/css" rel="stylesheet" href="{{asset('public/assets/vendors/iCheck/skins/all.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/css/frontend/login.css') }}">
    <!--end of page level css-->
</head>
<body>
<div class="container">
    <!--Content Section Start -->    
    <div class="row">
        <span class="col-sm-3"></span>
        <span class="col-sm-2"></span>
        <span class="col-sm-2"><img width="160px" src="{{ asset('public/assets/images/logo.png') }}" alt="logo" class="img-responsive mar"></span>        
        <span class="col-sm-2"></span>        
        <span class="col-sm-3"></span>        
    </div>
    <div class="row">
        
        
        <div class="box animation flipInX">
            <div class="box1">
            
            <h3 class="text-primary">Login</h3>
                <!-- Notifications -->                
                @if (session('flash_alert_notice'))
                    <div class="flash-message">
                        <div class="alert alert-{{session('flash_action')}}">
                               {{session('flash_alert_notice')}}
                        </div>
                    </div>
                @endif
                <form action="{{route('admin.signin')}}" class="omb_loginForm"  autocomplete="off" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group {{ $errors->first('email', 'has-error') }}">
                        <label class="sr-only">Email/Username</label>
                        <input type="text" class="form-control" name="email" placeholder="Email/Username" value="{!! Input::old('email') !!}">
                    </div>
                    <span class="help-block">{{ $errors->first('email', ':message') }}</span>
                    <div class="form-group {{ $errors->first('password', 'has-error') }}">
                        <label class="sr-only">Password</label>
                        <input type="password" class="form-control" name="password" placeholder="Password">
                    </div>
                    <span class="help-block">{{ $errors->first('password', ':message') }}</span>
                    <div class="checkbox col-sm-12">
<!--                        <label>
                            <input type="checkbox">  Remember Password
                        </label>-->
                        <label>
                        
                        </label>
                    </div>
                    <input type="submit" class="btn btn-block btn-primary" value="Login">                    
                    Administrator Account<a href="javascript:void(0)"><strong></strong></a>
                </form>
            </div>
        <div class="bg-light animation flipInX">
            <a href="#">Administrator</a>
        </div>
        </div>
    </div>
    <!-- //Content Section End -->
</div>
<!--global js starts-->
<script type="text/javascript" src="{{ asset('public/assets/js/frontend/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/assets/js/frontend/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/assets/vendors/iCheck/icheck.min.js') }}"></script>
<!--global js end-->
<script>
    $(document).ready(function(){
        $("input[type='checkbox']").iCheck({
            checkboxClass: 'icheckbox_minimal-blue'
        });
    });    
</script>
</body>
</html>
