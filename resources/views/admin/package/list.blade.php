@extends('admin.layouts.master')
{{-- Page title --}}
@section('title')
Entity
@parent
@stop

{{-- page level styles --}}
@section('header_styles')

<link href="{{ asset('public/assets/css/pages/form2.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/css/pages/form3.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/vendors/intl-tel-input/build/css/intlTelInput.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/vendors/validation/dist/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
<style type='text/css'>
    .form-group.required .control-label:after { 
        content:"*";
        color:red;
    }
    .myDragClass{
        background: lightskyblue !important;
    }
</style>
@stop

{{-- Page content --}}
@section('content')

<section class="content-header">
    <!--section starts-->
    <h1>Package</h1>
</section>
<!--section ends-->
<section class="content">
    <div class="row">
        <div class="col-md-12">            
            <!-- BEGIN SAMPLE TABLE PORTLET-->
            <div class="panel panel-primary filterable">
                <div class="panel-heading clearfix">
                    <div class="panel-heading clearfix  ">
                        <div class="caption pull-left">
                            <i class="livicon" data-name="list" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                            Package List                        
                        </div>
                        <div class="btn-group pull-right">
                            <a href="{{route('admin.package.create')}}"  class=" btn btn-warning add-anc">Add New Package</a>
                        </div>
                    </div>    
                </div>
                <div class="panel-body table-responsive">
                    @if (session('flash_message'))
                    <div class="flash-message">
                        <div class="alert alert-{{session('flash_action')}}">
                            <p>{{session('flash_message')}}</p>
                        </div>
                    </div>
                    @endif   
                    <form action="{{ route('admin.package.index') }}" method="GET" role="form">                    
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <input type="hidden" name="action" value="" />
                        <div class="row">
                            <div class="col-xs-12 col-md-12">                                        
                                <div class="form-group {{ $errors->first('', 'has-error') }}">
                                    <label class="control-label">Search</label>
                                    <input type="text" name="search_keyword" id="search_keyword" class="form-control" style="width:320px;" placeholder="Search by package name or price" value="{{Request::Input('search_keyword')}}">    
                                    <input type="submit" class="btn btn-primary" style="width:120px;margin-top:-58px;margin-left:404px;" value="Search">
                                </div>
                            </div>
                        </div> 
                    </form>   
                    <table class="table table-striped table-bordered" id="table-entity">
                        <thead>
                            <tr>                                    
                                <th style="width: 10%;text-align:center;">#</th>
                                <th style="width: 20%;text-align:center;">Package Name</th>
                                <th style="width: 10%;text-align:center;">Package Price</th>                                
                                <th style="text-align:center;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($package_data))                                                               
                            @foreach($package_data as $objEntity)
                            <tr>
                                <td style="text-align:center;">{{$objEntity['id']}}</td>                                    
                                <td style="text-align:left;">{{$objEntity['name']}}</td>
                                <td style="text-align:center;">{{$objEntity['price']}}</td>
<!--                                <td style="text-align:center;"><img src="<?php //echo url('/public/uploads/packages').'/'.$objEntity['image'];     ?>" style="width:100px; height:100px;"/></td>-->
                                <td style="width: 30%;text-align:center;">
                                    <a href="{{route('admin.package.edit',$objEntity['id'])}}" id="edit" name="edit" class=" btn btn-warning add-anc">Edit</a>
                                    <a href="{{route('admin.package.destroy',$objEntity['id'])}}"  id="" onclick="return confirm('Are you sure?');" class="btn btn-danger add-anc del-msg">Delete</a>
                                    <a href="javascript:void(0)"  id="show_img" name="show_img" image-name="{{$objEntity['image']}}" class="show_img btn btn-info">View Image</a>
                                </td>
                            </tr>                                
                            @endforeach                            
                            @endif
                        </tbody>
                    </table>                    
                    <span style="width:100%;" class"pull-right">{!! $package_data->render() !!}</span>
                </div>
            </div>
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>     
</div>   
</section>
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Package Image</h4>
            </div>
            <div class="modal-body">
                <img width="100%" height="auto" id="show_img_src" src=""/>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>      
        </div>
    </div>
</div>
<!-- global js -->
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<script src="{{ asset('public/assets/vendors/jasny-bootstrap/js/jasny-bootstrap.min.js') }}"
type="text/javascript"></script>
<script src="{{ asset('public/assets/vendors/validation/dist/js/bootstrapValidator.min.js') }}"
type="text/javascript"></script>
<script src="{{ asset('public/assets/vendors/intl-tel-input/build/js/intlTelInput.min.js') }}"
type="text/javascript"></script>
<script src="{{ asset('public/assets/js/pages/validation.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/js/jquery.tablednd.0.7.min.js') }}" type="text/javascript"></script>
<script>
//"uploads/packages";
$(document).ready(function() {
    // Initialise the first table (as before)
    $(".table").tableDnD({
        onDragClass: "myDragClass",        
        onDrop: function(table, row) {
           
        },
        onDragStart: function(table, row) {
           
        }
    });
});
</script>


@stop