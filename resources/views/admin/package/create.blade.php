@extends('admin.layouts.master')
{{-- Page title --}}
@section('title')
Package
@parent
@stop

{{-- page level styles --}}
@section('header_styles')

<link href="{{ asset('public/assets/css/pages/form2.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/css/pages/form3.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/vendors/intl-tel-input/build/css/intlTelInput.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/vendors/validation/dist/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
<script type="text/javascript" src="http://tinymce.cachefly.net/4.2/tinymce.min.js"></script>
            <script type="text/javascript">
            tinymce.init({
                selector: "textarea",
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table contextmenu paste"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
            });
            </script>
<style type='text/css'>
    .form-group.required .control-label:after { 
        content:"*";
        color:red;
    }
</style>
@stop

{{-- Page content --}}
@section('content')
<?php
$title = "Create";
$action = route('admin.package.store');
$method = '';
if (!empty($package->id)) {
    $title = "Edit";
    $action = route('admin.package.update', $package->id);
    $method = '<input type="hidden" name="_method" value="PUT" />';
}
?>
<section class="content-header">
    <!--section starts-->
    <h1>Package</h1>

</section>
<!--section ends-->
<section class="content">
    <!--main content-->
    <div class="row">            
        <div class="col-md-12">                
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="livicon" data-name="rocket" data-size="16" data-loop="true" data-c="#fff"
                           data-hc="white"></i>
                        {{$title}}
                    </h3>                        
                    <span class="pull-right clickable">
                        <i class="glyphicon glyphicon-chevron-up"></i>
                    </span>
                </div>                    
                <div class="panel-body">
                    <div class="row">
                        @if (session('flash_message'))
                        <div class="flash-message">
                            <div class="alert alert-{{session('flash_action')}}">
                                <p>{{session('flash_message')}}</p>
                            </div>
                        </div>
                        @endif
                        <form action="{{$action}}" method="POST" role="form" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <input type="hidden" name="id" value="{{@$package->id}}" />
                            <input type="hidden" name="profile_id" value="{{!empty(Request::input('pp'))?Request::input('pp'):0}}" />
                            <input type="hidden" name="action" value="{{$title}}" />
                            <?php echo $method; ?>                       
                            <div class="row">
                                <div class="col-xs-12 col-md-12">                                        
                                    <div class="form-group required {{ $errors->first('package_name', 'has-error') }}">                                            
                                        <label class="control-label">Package Name</label>
                                        <input type="text" name="package_name" id="package_name" class="form-control input-md" placeholder="Package Name" value="{{!empty($package->name)?$package->name:Input::old('package_name')}}">
                                        {!! $errors->first('package_name', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row"> 
                                <div class="col-xs-12 col-md-12">
                                    <div class="form-group {{ $errors->first('package_price', 'has-error') }}">
                                        <label class="control-label">Price</label>
                                        <input type="text" name="package_price" id="package_price" class="form-control input-md" placeholder="Package Price" value="{{!empty($package->price)?$package->price:Input::old('package_price')}}">
                                        {!! $errors->first('package_price', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                            </div>
                             <div class="row">                                    
                                    <div class="col-xs-12 col-md-12">
                                        <div class="form-group {{ $errors->first('package_desc', 'has-error') }}">
                                            <label class="control-label">Description</label>
                                            <textarea id="package_desc" name="package_desc" rows="5" col="5" class="form-control input-md" row="4" value="{{!empty($package->description)?$package->description:Input::old('package_desc')}}" placeholder="Package Description">{{!empty($package->description)?$package->description:Input::old('package_desc')}}</textarea>
                                            {!! $errors->first('package_desc', '<span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>
                            <div class="row">                                    
                                <div class="col-xs-12 col-md-12">
                                    <div class="form-group {{ $errors->first('package_image', 'has-error') }}">
                                        <label class="control-label">image</label>
                                        <input type="file" name="package_image" id="package_image" class="form-control input-md" >
                                        <?php if (!empty($package->image)) {
                                            ?><span><img src="<?php echo url('/public/uploads/demo') . '/' . $package->image; ?>" style="width:100px; height:100px;"/></span>
                                        <?php } ?>
                                        {!! $errors->first('package_image', '<span class="help-block">:message</span>') !!}
                                    </div>                                        
                                </div>
                            </div>
                            <div class="row">                                    
                                <div class="col-xs-12 col-md-12">
                                    <div class="form-group {{ $errors->first('ph', 'has-error') }}">
                                        <label class="control-label">Apply Postage and handling (Freight) P&H</label></br>
                                        <input name="ph" type="radio" checked="checked" value="1">Yes
                                        <input name="ph" type="radio" value="0">No
                                    </div>                                        
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12">
                                    <input type="submit" class="btn btn-primary btn-block btn-md btn-responsive create-btn" value="Create">
                                </div>
                            </div>
                        </form>
                    </div>                        
                </div>
            </div>                
        </div>
    </div>
    <!--row ends-->
</section>
<!-- content -->
<!-- global js -->
@stop

{{-- page level scripts --}}
@section('footer_scripts')    
<script src="{{ asset('public/assets/vendors/jasny-bootstrap/js/jasny-bootstrap.min.js') }}"
type="text/javascript"></script>
<script src="{{ asset('public/assets/vendors/validation/dist/js/bootstrapValidator.min.js') }}"
type="text/javascript"></script>
<script src="{{ asset('public/assets/vendors/intl-tel-input/build/js/intlTelInput.min.js') }}"
type="text/javascript"></script>
<script src="{{ asset('public/assets/js/pages/validation.js') }}" type="text/javascript"></script>
@stop