@extends('admin.layouts.master')
{{-- Page title --}}
@section('title')
Fotopay | Miscellaneous Job
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link href="{{ asset('public/assets/css/pages/form2.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/css/pages/form3.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/vendors/intl-tel-input/build/css/intlTelInput.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/vendors/validation/dist/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/css/datepicker.css') }}" />
<link href="{{ asset('public/assets/vendors/enyo/dropzone/dist/dropzone.css') }}" rel="stylesheet"/>
<style type='text/css'>
    .form-group.required .control-label:after { 
        content:"*";
        color:red;
    }
    .dropzone img{
        width: 100%;
        height:100%;
    }
</style>
@stop

{{-- Page content --}}
@section('content')
<?php
$title = "Create";
$action = route('admin.miscellaneousjob.store');
$method = '';
if (!empty($miscellaneousjob->id)) {
    $title = "Edit";
    $action = route('admin.miscellaneousjob.update', $miscellaneousjob->id);
    $method = '<input type="hidden" name="_method" value="PUT" />';
}
?>

<section class="content-header">
    <!--section starts-->
    <h1>Miscellaneous Job</h1>
</section>

<!--section ends-->
<section class="content">
    <!--main content-->
    <div class="row">            
        <div class="col-md-12">                
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="livicon" data-name="rocket" data-size="16" data-loop="true" data-c="#fff"
                           data-hc="white"></i>
                        {{$title}}
                    </h3>                        
                    <span class="pull-right clickable">
                        <i class="glyphicon glyphicon-chevron-up"></i>
                    </span>

                </div>                    
                <div class="panel-body">
                    <div class="row">
                        @if (session('flash_message'))
                        <div class="flash-message">
                            <div class="alert alert-{{session('flash_action')}}">
                                <p>{{session('flash_message')}}</p>
                            </div>
                        </div>
                        @endif
                        <form action="{{$action}}" method="POST" role="form">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <input type="hidden" name="action" value="{{$title}}" />
                            <?php echo $method; ?>                               
                            <div class="row">
                                <div class="col-xs-12 col-md-12">                                        
                                    <div class="form-group required {{ $errors->first('job_name', 'has-error') }}">
                                        <label class="control-label">Job Name</label>
                                        <input type="text" name="job_name" id="job_name" class="form-control" placeholder="Job Name" value="{{!empty($miscellaneousjob->job_name)?$miscellaneousjob->job_name:Input::old('job_name')}}">
                                        {!! $errors->first('job_name', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12">
                                    <div class="form-group required {{ $errors->first('job_code', 'has-error')}}">
                                        <label class="control-label">Unique Code</label>
                                        <input id="job_code" name="job_code" class="form-control input-md" value="{{!empty($miscellaneousjob->job_code)?$miscellaneousjob->job_code:Input::old('job_code')}}" placeholder="Job Code" <?php echo !empty($miscellaneousjob->job_code)?"disabled='disabled'":"" ?>/>
<!--                                        <div class="input-group">                                            
                                            <input id="job_code" name="job_code" class="form-control input-md" value="{{!empty($miscellaneousjob->job_code)?$miscellaneousjob->job_code:Input::old('job_code')}}" placeholder="Job Code"/>
                                            <span class="input-group-addon">
                                                <button type="button" id="gen_unique_code" name="gen_unique_code">Generate Code</button>
                                            </span>
                                        </div>                                                                                -->
                                        {!! $errors->first('job_code', '<span class="help-block">:message</span>') !!}
                                    </div>                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12">                                        
                                    <div class="form-group required {{ $errors->first('job_date', 'has-error') }}">
                                        <label class="control-label">Job Date</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i data-loop="true" data-hc="#000" data-c="#000" data-size="18" data-name="calendar" class="livicon" id="livicon-47"></i>
                                            </span>
                                            <input type="text" name="job_date" id="job_date" class="form-control" placeholder="Job Date" value="{{!empty($miscellaneousjob->job_date)?$miscellaneousjob->job_date:Input::old('job_date')}}">
                                        </div>                                                
                                        {!! $errors->first('job_date', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                            </div>                                                                                    
                            @if(isset($miscellaneousjob->id) && !empty($miscellaneousjob->id))                            
                            <div class="row">                                    
                                <div class="col-xs-12 col-md-12">
                                    <div class="form-group {{ $errors->first('status', 'has-error')}}">
                                        <label class="control-label">Status</label>                                        
                                        <?php
                                        $active = ($miscellaneousjob->status == '1') ? 'checked="checked"' : "";
                                        $inactive = ($miscellaneousjob->status == '0') ? 'checked="checked"' : "";
                                        ?>                                        
                                        <input name="status" value="1" type="radio" <?php echo $active; ?>/> Active <input type="radio" name="status" value="0" <?php echo $inactive; ?>/> Inactive
                                        {!! $errors->first('status', '<span class="help-block">:message</span>') !!}
                                    </div>                                    
                                </div>
                            </div>                            
                            @endif
                            <div class="row">
                                <div class="col-xs-12 col-md-12">                                        
                                    <div class="dropzone" id="myId">
                                        <div class="fallback">
                                            <input id="files" multiple="true" name="files" type="file">
                                        </div>
                                        <div id="uploadimages">

                                        </div>
                                    </div>                                    
                                </div>
                            </div>                            
                            <div class="row">                                    
                                <div class="col-xs-12 col-md-12">
                                    <div class="form-group required {{ $errors->first('packageprofile_id', 'has-error')}}">
                                        <label class="control-label">Package profile name</label>
                                        <select class="form-control input-md" id="packageprofile_id" name="packageprofile_id">
                                            <option value="">Please Select</option>
                                            @if(!empty($packageprofile))
                                            @foreach($packageprofile as $objPackageProfile)
                                            @if($objPackageProfile->id == Input::old('packageprofile_id') || (isset($miscellaneousjob->packageprofile_id) && $objPackageProfile->id == $miscellaneousjob->packageprofile_id))
                                            <option value="{{$objPackageProfile->id}}" selected="selected">{{$objPackageProfile->package_profile_name}}</option>
                                            @else
                                            <option value="{{$objPackageProfile->id}}">{{$objPackageProfile->package_profile_name}}</option>
                                            @endif
                                            @endforeach
                                            @endif
                                        </select>                                        
                                        {!! $errors->first('packageprofile_id', '<span class="help-block">:message</span>') !!}
                                    </div>                                    
                                </div>
                            </div>
                            <div class="row">                                
                                <div class="col-xs-12 col-md-12">
                                    <input type="submit" class="btn btn-primary btn-block btn-md btn-responsive create-btn" value="Create">
                                </div>                                                                   
                            </div>
                        </form>
                    </div>                        
                </div>
            </div>                
        </div>
    </div>
    <!--row ends-->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')    
<script src="{{ asset('public/assets/vendors/jasny-bootstrap/js/jasny-bootstrap.min.js') }}"
type="text/javascript"></script>
<script src="{{ asset('public/assets/vendors/validation/dist/js/bootstrapValidator.min.js') }}"
type="text/javascript"></script>
<script src="{{ asset('public/assets/vendors/intl-tel-input/build/js/intlTelInput.min.js') }}"
type="text/javascript"></script>    
<script src="{{ asset('public/assets/js/pages/validation.js') }}" type="text/javascript"></script>

<script type="text/javascript" src="{{ asset('public/assets/js/bootstrap-datepicker.js') }}" ></script>
<script src="{{ asset('public/assets/vendors/enyo/dropzone/dist/dropzone.js') }}" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
    Dropzone.autoDiscover = false;
    $("#job_date").datepicker({
        format: 'yyyy-mm-dd'
    });
    $("#finished_date").datepicker({
        format: 'yyyy-mm-dd'
    });
    $("#add_grade").click(function(){         
        $("#dynamic_grade").append("<input type='text' name='grade[]' class='form-control grade' placeholder='Grade'>");
        $(".grade").last().focus();
    });
    $("div#myId").dropzone({
        url: "{{route('admin.uploadImage.upload')}}",
        addRemoveLinks: true,
        dictDefaultMessage: '<span class="text-center"><span class="font-lg visible-xs-block visible-sm-block visible-lg-block"><span class="font-lg"><i class="fa fa-caret-right text-danger"></i> Drop files <span class="font-xs">to upload</span></span><span>&nbsp&nbsp<h4 class="display-inline"> (Or Click)</h4></span>',
        dictResponseError: 'Error uploading file!',        
        init: function() {            
            //Call the action method to load the images from the server
            var thisDropzone = this;
            $.getJSON("/fotopay/admin/uploadImage/getImages/<?php echo @$miscellaneousjob->id ?>").done(function(data) {
                if (data.data != '') {
                    $.each(data.data, function(index, item) {                        
                        var mockFile = {
                            name: item.FileName,
                            size: item.size,
                            status: Dropzone.ADDED,
                            accepted: true,                            
                        };                                     
                        thisDropzone.emit("addedfile", mockFile);
                        thisDropzone.emit("thumbnail", mockFile, item.Path);
                        thisDropzone.emit("complete", mockFile);
			thisDropzone.files.push(mockFile);                        
                        $("#uploadimages").append("<input type='hidden' name='uploadimages[]' value='" + item.FileName + "'>");
                    });
                }
            });
        },
        sending: function(file, xhr, formData) {
            formData.append('_token', "{{csrf_token()}}");
            formData.append('student_id', $("#student_id").val());
        },
        success: function(file, newfilename) {
            file.newfilename = newfilename;            
            $("#uploadimages").append("<input type='hidden' name='uploadimages[]' value='" + newfilename + "'>");

        },
        removedfile: function(file) {
            var thisfile = file;
            if(file.status == "success"){
                removeImage(file.newfilename, file.status);
                $("input[value='" + thisfile.newfilename + "']").remove();                
            }else{                
                removeImage(file.name, file.status);
                $("input[value='" + thisfile.name + "']").remove();
                $("#uploadimages").append("<input type='hidden' name='removeimages[]' value='" + thisfile.name + "'>");
            }            
            var _ref;
            return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
        },
    });
});
function removeImage(imgNm, action) { 
    $.ajax({
        url: "{{route('admin.uploadImage.remove')}}",
        type: 'POST',
        dataType: 'JSON',
        data: ({imgNm: imgNm, _token: "{{csrf_token()}}", status:action}),
        success: function(response) {
            console.log(response);
        },
        error: function(response) {
            
        }
    });
}
</script>    

@stop
