@extends('admin.layouts.master')
{{-- Page title --}}
@section('title')
    Contact
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')

    <link href="{{ asset('public/assets/css/pages/form2.css') }}" rel="stylesheet"/>
    <link href="{{ asset('public/assets/css/pages/form3.css') }}" rel="stylesheet"/>
    <link href="{{ asset('public/assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('public/assets/vendors/intl-tel-input/build/css/intlTelInput.css') }}" rel="stylesheet"/>
    <link href="{{ asset('public/assets/vendors/validation/dist/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
    <style type='text/css'>
        .form-group.required .control-label:after { 
            content:"*";
            color:red;
        }
    </style>
@stop

{{-- Page content --}}
@section('content')
 <?php
        $title = "Create";
        $action = route('admin.item.store');
        $method = '';
        if(!empty($item->id)){
            $title = "Edit";
            $action = route('admin.item.update', $item->id);
            $method = '<input type="hidden" name="_method" value="PUT" />';
        }?>
    <section class="content-header">
        <!--section starts-->
        <h1>Contact</h1>
    </section>
    <!--section ends-->
  <section class="content">
        <!--main content-->
        <div class="row">            
            <div class="col-md-12">                
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="livicon" data-name="rocket" data-size="16" data-loop="true" data-c="#fff"
                               data-hc="white"></i>
                            {{$title}}
                        </h3>                        
                        <span class="pull-right clickable">
                            <i class="glyphicon glyphicon-chevron-up"></i>
                        </span>

                    </div>                    
                   <div class="panel-body">                                        
            <div class="row">
                <div class="col-md-12">            
            <!-- BEGIN SAMPLE TABLE PORTLET-->
            <div class="panel panel-primary filterable">
                @if (session('flash_alert_notice'))
                <div class="flash-message">
                    <div class="alert alert-{{session('flash_action')}}">
                        <p>{{session('flash_alert_notice')}}</p>
                    </div>
                </div>
                @endif
                <form action="" method="POST" role="form">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <input name="_method" type="hidden" value="PUT">
                    <input name="entity_id" type="hidden" value="">
                    <div class="internal-panel-heading">
                        <h4>Physical Address Information</h4>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-4">                                        
                            <div class="form-group {{ $errors->first('entity_physical_address1', 'has-error') }}">                                            
                                <label class="control-label">Address Line 1</label>
                                <input type="text" name="entity_physical_address1" id="entity_physical_address1" class="form-control input-md" placeholder="Physical Address Line 1" value="{{@$entity_data['entity_physical_address1']}}">
                                {!! $errors->first('entity_physical_address1', '<span class="help-block">:message</span>') !!}                                                                                       
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4">
                            <div class="form-group {{ $errors->first('entity_physical_address2', 'has-error') }}">                                            
                                <label class="control-label">Address Line 2</label>
                                <input type="text" name="entity_physical_address2" id="entity_physical_address2" class="form-control input-md" placeholder="Physical Address Line 2" value="{{@$entity_data['entity_physical_address2']}}">
                                {!! $errors->first('entity_physical_address2', '<span class="help-block">:message</span>') !!}                                                                                       
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4">
                            <div class="form-group {{ $errors->first('entity_physical_address3', 'has-error') }}">                                            
                                <label class="control-label">Address Line 3</label>
                                <input type="text" name="entity_physical_address3" id="entity_physical_address3" class="form-control input-md" placeholder="Physical Address Line 3" value="{{@$entity_data['entity_physical_address3']}}">
                                {!! $errors->first('entity_physical_address3', '<span class="help-block">:message</span>') !!}                                                                                       
                            </div>
                        </div>
                    </div>
                    <div class="row">                                    
                        <div class="col-xs-12 col-md-2">
                            <div class="form-group {{ $errors->first('entity_physical_suburb', 'has-error') }}">
                                <label class="control-label">suburb</label>
                                <input type="text" name="entity_physical_suburb" id="entity_physical_suburb" class="form-control input-md auto" placeholder="Suburb" value="{{@$entity_data['entity_physical_suburb']}}">
                                {!! $errors->first('entity_physical_suburb', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-2">
                            <div class="form-group {{ $errors->first('entity_physical_city', 'has-error') }}">
                                <label class="control-label">City</label>
                                <input type="text" name="entity_physical_city" id="entity_physical_city" class="form-control input-md" placeholder="City" value="{{@$entity_data['entity_physical_city']}}">
                                {!! $errors->first('entity_physical_city', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-2">
                            <div class="form-group {{ $errors->first('entity_physical_streetcode', 'has-error') }}">
                                <label class="control-label">Street Code</label>
                                <input type="text" name="entity_physical_streetcode" id="entity_physical_streetcode" class="form-control input-md" placeholder="Street Code" value="{{@$entity_data['entity_physical_streetcode']}}">
                                {!! $errors->first('entity_physical_streetcode', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3">
                            <div class="form-group {{ $errors->first('entity_physical_province', 'has-error') }}">
                                <label class="control-label">Province</label>
                                <input type="text" name="entity_physical_province" id="entity_physical_province" class="form-control input-md" placeholder="Province" value="{{@$entity_data['entity_physical_province']}}">
                                {!! $errors->first('entity_physical_province', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3">
                            <div class="form-group {{ $errors->first('entity_physical_country', 'has-error') }}">
                                <label class="control-label">Country</label>
                                <input type="text" name="entity_physical_country" id="entity_physical_country" class="form-control input-md" placeholder="Country" value="{{@$entity_data['entity_physical_country']}}">
                                {!! $errors->first('entity_physical_country', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>                                                                                                        
                    </div>

                    <div class="internal-panel-heading">
                        <h4>Postal Address Information</h4>                                                    
                    </div>
                    <div class="checkbox-portal-address">
                        <?php
                        $checked = "";
                        if (!empty($entity_data['entity_postal_address1']) || !empty($entity_data['entity_postal_address2']) || !empty($entity_data['entity_postal_address3']) || !empty($entity_data['entity_postal_suburb']) || !empty($entity_data['entity_postal_city']) || !empty($entity_data['entity_postal_postalcode']) || !empty($entity_data['entity_postal_province']) || !empty($entity_data['entity_postal_country'])) {
                            $checked = "checked='checked'";
                        }
                        ?>
                        <input type="checkbox" name="portal_address_checkbox" id="portal_address_checkbox" {{$checked}}>
                        <label class="control-label">Statement address and the business address is the same</label>                                                        
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-md-4">                                        
                            <div class="form-group {{ $errors->first('entity_postal_address1', 'has-error') }}">                                            
                                <label class="control-label">Address Line 1</label>
                                <input type="text" name="entity_postal_address1" id="entity_postal_address1" class="form-control input-md" placeholder="Physical Address Line 1" value="{{@$entity_data['entity_postal_address1']}}">
                                {!! $errors->first('entity_postal_address1', '<span class="help-block">:message</span>') !!}                                                                                       
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4">
                            <div class="form-group {{ $errors->first('entity_postal_address2', 'has-error') }}">                                            
                                <label class="control-label">Address Line 2</label>
                                <input type="text" name="entity_postal_address2" id="entity_postal_address2" class="form-control input-md" placeholder="Physical Address Line 2" value="{{@$entity_data['entity_postal_address2']}}">
                                {!! $errors->first('entity_postal_address2', '<span class="help-block">:message</span>') !!}                                                                                       
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4">
                            <div class="form-group {{ $errors->first('entity_postal_address3', 'has-error') }}">                                            
                                <label class="control-label">Address Line 3</label>
                                <input type="text" name="entity_postal_address3" id="entity_postal_address3" class="form-control input-md" placeholder="Physical Address Line 3" value="{{@$entity_data['entity_postal_address3']}}">
                                {!! $errors->first('entity_postal_address3', '<span class="help-block">:message</span>') !!}                                                                                       
                            </div>
                        </div>
                    </div>                                                
                    <div class="row">                                    
                        <div class="row">                                    
                            <div class="col-xs-12 col-md-2">
                                <div class="form-group {{ $errors->first('entity_postal_suburb', 'has-error') }}">
                                    <label class="control-label">suburb</label>
                                    <input type="text" name="entity_postal_suburb" id="entity_postal_suburb" class="form-control input-md auto1" placeholder="Suburb" value="{{@$entity_data['entity_postal_suburb']}}">
                                    {!! $errors->first('entity_postal_suburb', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-2">
                                <div class="form-group {{ $errors->first('entity_postal_city', 'has-error') }}">
                                    <label class="control-label">City</label>
                                    <input type="text" name="entity_postal_city" id="entity_postal_city" class="form-control input-md" placeholder="City" value="{{@$entity_data['entity_postal_city']}}">
                                    {!! $errors->first('entity_postal_city', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-2">
                                <div class="form-group {{ $errors->first('entity_postal_streetcode', 'has-error') }}">
                                    <label class="control-label">Street Code</label>
                                    <input type="text" name="entity_postal_streetcode" id="entity_postal_streetcode" class="form-control input-md" placeholder="Street Code" value="{{@$entity_data['entity_postal_postalcode']}}">
                                    {!! $errors->first('entity_postal_streetcode', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-3">
                                <div class="form-group {{ $errors->first('entity_postal_province', 'has-error') }}">
                                    <label class="control-label">Province</label>
                                    <input type="text" name="entity_postal_province" id="entity_postal_province" class="form-control input-md" placeholder="Province" value="{{@$entity_data['entity_postal_province']}}">
                                    {!! $errors->first('entity_postal_province', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-3">
                                <div class="form-group {{ $errors->first('entity_postal_country', 'has-error') }}">
                                    <label class="control-label">Country</label>
                                    <input type="text" name="entity_postal_country" id="entity_postal_country" class="form-control input-md" placeholder="Country" value="{{@$entity_data['entity_postal_country']}}">
                                    {!! $errors->first('entity_postal_country', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>                                                                                                        
                        </div>                                                    
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-4"></div>
                        <div class="col-xs-12 col-md-4">
                            <input type="submit" class="btn btn-primary btn-block btn-md btn-responsive update-btn" value="Updated">
                        </div>
                        <div class="col-xs-12 col-md-4"></div>
                    </div>
                </form>
               
            </div>                        
        </div>
                </div>                
            </div>
        </div>
        
        <!--row ends-->
    </section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')    
    <script src="{{ asset('public/assets/vendors/jasny-bootstrap/js/jasny-bootstrap.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('public/assets/vendors/validation/dist/js/bootstrapValidator.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('public/assets/vendors/intl-tel-input/build/js/intlTelInput.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('public/assets/js/pages/validation.js') }}" type="text/javascript"></script>
    <style>
.internal-panel-heading {
  background: #3a4252 none repeat scroll 0 0;
  height: 34px;
  margin: 9px;
  padding: 1px 4px 3px 5px;
  width: 97%;
}
.internal-panel-heading h4 {
    color: #fff;
    font-size: 10pt;
    margin-left: 10px;
}
    </style>
@stop
