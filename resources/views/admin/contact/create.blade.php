@extends('admin.layouts.master')
{{-- Page title --}}
@section('title')
    Contact
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')

    <link href="{{ asset('public/assets/css/pages/form2.css') }}" rel="stylesheet"/>
    <link href="{{ asset('public/assets/css/pages/form3.css') }}" rel="stylesheet"/>
    <link href="{{ asset('public/assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('public/assets/vendors/intl-tel-input/build/css/intlTelInput.css') }}" rel="stylesheet"/>
    <link href="{{ asset('public/assets/vendors/validation/dist/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
    <style type='text/css'>
        .form-group.required .control-label:after { 
            content:"*";
            color:red;
        }
    </style>
@stop

{{-- Page content --}}
@section('content')
 <?php
        $title = "Edit";
        $action = route('admin.contact.store');
        //$method = '';
        ?>
    <section class="content-header">
        <!--section starts-->
        <h1>Contact</h1>
    </section>
    <!--section ends-->
  <section class="content">
        <!--main content-->
        <div class="row">            
            <div class="col-md-12">                
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="livicon" data-name="rocket" data-size="16" data-loop="true" data-c="#fff"
                               data-hc="white"></i>
                            {{$title}}
                        </h3>                        
                        <span class="pull-right clickable">
                            <i class="glyphicon glyphicon-chevron-up"></i>
                        </span>

                    </div>                    
                   <div class="panel-body">                                        
            <div class="row">
                <div class="col-md-12">            
            <!-- BEGIN SAMPLE TABLE PORTLET-->
            <div class="panel panel-primary filterable">
                @if (session('flash_message'))
                <div class="flash-message">
                    <div class="alert alert-{{session('flash_action')}}">
                        <p>{{session('flash_message')}}</p>
                    </div>
                </div>
                @endif
                
                <form action="{{$action}}" method="POST" role="form">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    
                    <div class="internal-panel-heading">
                        <h4>Physical Address Information</h4>
                    </div>
                    @if(!empty($contact_data))                                                               
                    @foreach($contact_data as $objContact)
                    <div class="row">
                        <div class="col-xs-12 col-md-4">                                        
                            <div class="form-group {{ $errors->first('contact_physical_address1', 'has-error') }}">                                            
                                <label class="control-label">Address Line 1 *</label>
                                <input type="text" name="contact_physical_address1" id="contact_physical_address1" class="form-control input-md" placeholder="Physical Address Line 1" value="{{@$objContact['address']}}">
                                {!! $errors->first('contact_physical_address1', '<span class="help-block">:message</span>') !!}                                                                                       
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4">
                            <div class="form-group {{ $errors->first('contact_physical_address2', 'has-error') }}">                                            
                                <label class="control-label">Address Line 2</label>
                                <input type="text" name="contact_physical_address2" id="contact_physical_address2" class="form-control input-md" placeholder="Physical Address Line 2" value="{{@$objContact['address_two']}}">
                                {!! $errors->first('contact_physical_address2', '<span class="help-block">:message</span>') !!}                                                                                       
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4">
                            <div class="form-group {{ $errors->first('contact_physical_address3', 'has-error') }}">                                            
                                <label class="control-label">Address Line 3</label>
                                <input type="text" name="contact_physical_address3" id="contact_physical_address3" class="form-control input-md" placeholder="Physical Address Line 3" value="{{@$objContact['address_three']}}">
                                {!! $errors->first('contact_physical_address3', '<span class="help-block">:message</span>') !!}                                                                                       
                            </div>
                        </div>
                    </div>
                    <div class="row">                                    
                        <div class="col-xs-12 col-md-2">
                            <div class="form-group {{ $errors->first('contact_physical_suburb', 'has-error') }}">
                                <label class="control-label">suburb *</label>
                                <input type="text" name="contact_physical_suburb" id="contact_physical_suburb" class="form-control input-md auto" placeholder="Suburb" value="{{@$objContact['suburb']}}">
                                {!! $errors->first('contact_physical_suburb', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-2">
                            <div class="form-group {{ $errors->first('contact_physical_postcode', 'has-error') }}">
                                <label class="control-label">Post Code *</label>
                                <input type="text" name="contact_physical_postcode" id="contact_physical_postcode" class="form-control input-md" placeholder="Post Code" value="{{@$objContact['post_code']}}">
                                {!! $errors->first('contact_physical_postcode', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-2">
                            <div class="form-group {{ $errors->first('contact_physical_phone', 'has-error') }}">
                                <label class="control-label">Phone Number *</label>
                                <input type="text" name="contact_physical_phone" id="contact_physical_phone" class="form-control input-md" placeholder="Phone Number" value="{{@$objContact['phone_no']}}">
                                {!! $errors->first('contact_physical_phone', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                          <div class="col-xs-12 col-md-2">
                            <div class="form-group {{ $errors->first('contact_physical_email', 'has-error') }}">
                                <label class="control-label">Email *</label>
                                <input type="text" name="contact_physical_email" id="contact_physical_email" class="form-control input-md" placeholder="Email" value="{{@$objContact['email']}}">
                                {!! $errors->first('contact_physical_email', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        
                    </div>

                    <div class="internal-panel-heading">
                        <h4>Postal Address Information</h4>                                                    
                    </div>
                    <div class="checkbox-portal-address">
                        <?php
                        $checked = "";
                        if (!empty($entity_data['entity_postal_address1']) || !empty($entity_data['entity_postal_address2']) || !empty($entity_data['entity_postal_address3']) || !empty($entity_data['entity_postal_suburb']) || !empty($entity_data['entity_postal_city']) || !empty($entity_data['entity_postal_postalcode']) || !empty($entity_data['entity_postal_province']) || !empty($entity_data['entity_postal_country'])) {
                            $checked = "checked='checked'";
                        }
                        ?>
                        <input type="checkbox" name="postal_address_checkbox" id="postal_address_checkbox" {{$checked}}>
                        <label class="control-label">Click here if Physical address and postal address is the same</label>                                                        
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-md-4">                                        
                            <div class="form-group {{ $errors->first('contact_postal_address1', 'has-error') }}">                                            
                                <label class="control-label">Address Line 1 *</label>
                                <input type="text" name="contact_postal_address1" id="contact_postal_address1" class="form-control input-md" placeholder="Postal Address Line 1" value="{{@$objContact['postal_address']}}">
                                {!! $errors->first('contact_postal_address1', '<span class="help-block">:message</span>') !!}                                                                                       
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4">
                            <div class="form-group {{ $errors->first('contact_postal_address2', 'has-error') }}">                                            
                                <label class="control-label">Address Line 2</label>
                                <input type="text" name="contact_postal_address2" id="contact_postal_address2" class="form-control input-md" placeholder="Postal Address Line 2" value="{{@$objContact['postal_address_two']}}">
                                {!! $errors->first('contact_postal_address2', '<span class="help-block">:message</span>') !!}                                                                                       
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4">
                            <div class="form-group {{ $errors->first('contact_postal_address3', 'has-error') }}">                                            
                                <label class="control-label">Address Line 3</label>
                                <input type="text" name="contact_postal_address3" id="contact_postal_address3" class="form-control input-md" placeholder="Postal Address Line 3" value="{{@$objContact['postal_address_three']}}">
                                {!! $errors->first('contact_postal_address3', '<span class="help-block">:message</span>') !!}                                                                                       
                            </div>
                        </div>
                    </div>                                                
                    <div class="row">                                    
                        <div class="row">                                    
                            <div class="col-xs-12 col-md-2">
                                <div class="form-group {{ $errors->first('contact_postal_suburb', 'has-error') }}">
                                    <label class="control-label">suburb *</label>
                                    <input type="text" name="contact_postal_suburb" id="contact_postal_suburb" class="form-control input-md auto1" placeholder="Suburb" value="{{@$objContact['postal_suburb']}}">
                                    {!! $errors->first('contact_postal_suburb', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-2">
                                <div class="form-group {{ $errors->first('contact_postal_postcode', 'has-error') }}">
                                    <label class="control-label">Post Code *</label>
                                    <input type="text" name="contact_postal_postcode" id="contact_postal_postcode" class="form-control input-md" placeholder="Post Code" value="{{@$objContact['postal_postcode']}}">
                                    {!! $errors->first('contact_postal_postcode', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-2">
                                <div class="form-group {{ $errors->first('contact_postal_phonenumber', 'has-error') }}">
                                    <label class="control-label">Phone Number *</label>
                                    <input type="text" name="contact_postal_phonenumber" id="contact_postal_phonenumber" class="form-control input-md" placeholder="Phone Number" value="{{@$objContact['postal_phone_number']}}">
                                    {!! $errors->first('contact_postal_phonenumber', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-3">
                                <div class="form-group {{ $errors->first('contact_postal_email', 'has-error') }}">
                                    <label class="control-label">Email *</label>
                                    <input type="text" name="contact_postal_email" id="contact_postal_email" class="form-control input-md" placeholder="Email" value="{{@$objContact['postal_email']}}">
                                    {!! $errors->first('contact_postal_email', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                                                                                                                                 
                        </div>                                                    
                    </div>
                        <div class="row">                                    
                            <div class="col-xs-12 col-md-12">
                                <input type="submit" class="btn btn-primary btn-block btn-md btn-responsive create-btn" value="Create">
                            </div>
                        </div> 
                </form>
                @endforeach                            
                @endif
            </div>                        
        </div>
                </div>                
            </div>
        </div>
        
        <!--row ends-->
    </section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')    
    <script src="{{ asset('public/assets/vendors/jasny-bootstrap/js/jasny-bootstrap.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('public/assets/vendors/validation/dist/js/bootstrapValidator.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('public/assets/vendors/intl-tel-input/build/js/intlTelInput.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('public/assets/js/pages/validation.js') }}" type="text/javascript"></script>
    <style>
    .internal-panel-heading {
      background: #3a4252 none repeat scroll 0 0;
      height: 34px;
      margin: 9px;
      padding: 1px 4px 3px 5px;
      width: 97%;
    }
    .internal-panel-heading h4 {
        color: #fff;
        font-size: 10pt;
        margin-left: 10px;
    }
    .checkbox-portal-address {
        margin: 9px;
    }
    </style>
    <script>
    $("#postal_address_checkbox").click(function() {
            if ($(this).is(':checked')) {
                $("#contact_postal_address1").val($("#contact_physical_address1").val());
                $("#contact_postal_address2").val($("#contact_physical_address2").val());
                $("#contact_postal_address3").val($("#contact_physical_address3").val());
                $("#contact_postal_suburb").val($("#contact_physical_suburb").val());
                $("#contact_postal_postcode").val($("#contact_physical_postcode").val());
                $("#contact_postal_phonenumber").val($("#contact_physical_phone").val());
                $("#contact_postal_email").val($("#contact_physical_email").val());
                
            } else {
                $("#contact_postal_address1").val("");
                $("#contact_postal_address2").val("");
                $("#contact_postal_address3").val("");
                $("#contact_postal_suburb").val("");
                $("#contact_postal_postcode").val("");
                $("#contact_postal_phonenumber").val("");
                $("#contact_postal_email").val("");
                
            }
        });
    </script>
@stop
