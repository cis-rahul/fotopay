@extends('admin.layouts.master')
{{-- Page title --}}
@section('title')
Grade 
@parent
@stop

{{-- page level styles --}}
@section('header_styles')

<link href="{{ asset('public/assets/css/pages/form2.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/css/pages/form3.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/vendors/intl-tel-input/build/css/intlTelInput.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/vendors/validation/dist/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
<style type='text/css'>
    .form-group.required .control-label:after { 
        content:"*";
        color:red;
    }
</style>
@stop

{{-- Page content --}}
@section('content')
<?php
    $title = "Create";
    $action = route('admin.grade.store');
    $method = '';
    if(!empty($grade->id)){
        $title = "Edit";
        $action = route('admin.grade.update', $grade->id);
        $method = '<input type="hidden" name="_method" value="PUT" />';
    }
?>


<section class="content-header">
    <!--section starts-->
    <h1>Grade</h1>
</section>
<!--section ends-->
<section class="content">
    <!--main content-->
    <div class="row">            
        <div class="col-md-12">                
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="livicon" data-name="rocket" data-size="16" data-loop="true" data-c="#fff"
                           data-hc="white"></i>
                        {{$title}}
                    </h3>                        
                    <span class="pull-right clickable">
                        <i class="glyphicon glyphicon-chevron-up"></i>
                    </span>

                </div>                    
                <div class="panel-body">
                    <div class="row">
                        @if (session('flash_message'))
                        <div class="flash-message">
                            <div class="alert alert-{{session('flash_action')}}">
                                <p>{{session('flash_message')}}</p>
                            </div>
                        </div>
                        @endif
                        <form action="{{$action}}" method="POST" role="form">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <input type="hidden" name="action" value="{{$title}}" />
                            <?php echo $method; ?>
                            <div class="row">                                    
                                <div class="col-xs-12 col-md-12">
                                    <div class="form-group required {{ $errors->first('school_name', 'has-error')}}">
                                        <label class="control-label">School Name</label>
                                        <select class="form-control input-md" id="school_name" name="school_name">
                                            <option value="">Please Select</option>
                                            @if(!empty($schools))
                                                @foreach($schools as $objSchool)
                                                @if($objSchool->id == Input::old('school_name') || (isset($grade->school_id) && $objSchool->id == $grade->school_id))
                                                <option value="{{$objSchool->id}}" selected="selected">{{$objSchool->name}}</option>
                                                @else
                                                <option value="{{$objSchool->id}}">{{$objSchool->name}}</option>
                                                @endif
                                                @endforeach
                                            @endif
                                        </select>                                        
                                        {!! $errors->first('school_name', '<span class="help-block">:message</span>') !!}
                                    </div>                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12">                                        
                                    <div class="form-group required {{ $errors->first('title', 'has-error') }}">                                            
                                        <label class="control-label">Grade Title</label>
                                        <input type="text" name="title" id="title" class="form-control input-md" placeholder="Grade Title" value="{{!empty($grade->title)?$grade->title:Input::old('title')}}">
                                        {!! $errors->first('title', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                            </div>                            
                            @if(isset($grade->id) && !empty($grade->id))
                            <div class="row">                                    
                                <div class="col-xs-12 col-md-12">
                                    <div class="form-group {{ $errors->first('status', 'has-error')}}">
                                        <label class="control-label">Status</label>                                        
                                        <?php                                        
                                        $active = ($grade->status == '1')?'checked="checked"':"";
                                        $inactive = ($grade->status == '0')?'checked="checked"':"";
                                        ?>                                        
                                        <input name="status" value="1" type="radio" <?php echo $active; ?>/> Active <input type="radio" name="status" value="0" <?php echo $inactive; ?>/> Inactive
                                        {!! $errors->first('status', '<span class="help-block">:message</span>') !!}
                                    </div>                                    
                                </div>
                            </div>
                            @endif
                            <div class="row">                                    
                                
                                    <div class="col-xs-12 col-md-12">
                                        <input type="submit" class="btn btn-primary btn-block btn-md btn-responsive create-btn" value="Create">
                                    </div>                                                                   
                            </div>
                            
                        </form>
                    </div>                        
                </div>
            </div>                
        </div>
    </div>

    <!--row ends-->
</section>
<!-- content -->
</aside>
<!-- right-side -->
</div>
<a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Return to top"
   data-toggle="tooltip" data-placement="left">
    <i class="livicon" data-name="plane-up" data-size="18" data-loop="true" data-c="#fff" data-hc="white"></i>
</a>
<!-- global js -->
@stop

{{-- page level scripts --}}
@section('footer_scripts')    
<script src="{{ asset('public/assets/vendors/jasny-bootstrap/js/jasny-bootstrap.min.js') }}"
type="text/javascript"></script>
<script src="{{ asset('public/assets/vendors/validation/dist/js/bootstrapValidator.min.js') }}"
type="text/javascript"></script>
<script src="{{ asset('public/assets/vendors/intl-tel-input/build/js/intlTelInput.min.js') }}"
type="text/javascript"></script>    
<script src="{{ asset('public/assets/js/pages/validation.js') }}" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){    
    $("#gen_unique_code").click(function(){ 
        if($("#name").val() != ""){
            var code = "SC";
            code = code + $("#name").val().substring(0, 2).toUpperCase();
            code = code + (Math.random()+' ').substring(4,10);
            $("#code").val(code);
        }else{
            alert("Please fill the grade name first!");
        }
    });        
});
</script>
@stop
