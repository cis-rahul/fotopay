@extends('admin.layouts.master')
{{-- Page title --}}
@section('title')
    School
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')

    <link href="{{ asset('public/assets/css/pages/form2.css') }}" rel="stylesheet"/>
    <link href="{{ asset('public/assets/css/pages/user_profile.css') }}" rel="stylesheet"/>
    <link href="{{ asset('public/assets/css/pages/form3.css') }}" rel="stylesheet"/>
    <link href="{{ asset('public/assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('public/assets/vendors/intl-tel-input/build/css/intlTelInput.css') }}" rel="stylesheet"/>
    <link href="{{ asset('public/assets/vendors/validation/dist/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
    <style type='text/css'>
        .form-group.required .control-label:after { 
            content:"*";
            color:red;
        }
    </style>
@stop

{{-- Page content --}}
@section('content')

    <section class="content-header">
        <!--section starts-->
        <h1>Profile Management</h1>

    </section>
    <!--section ends-->
     <section class="content">
                <div class="row">
                    <div class="col-lg-12">
                        <ul class="nav  nav-tabs ">
                            <li class="active">
                                <a href="#tab1" data-toggle="tab">
                                   <i class="livicon" data-name="user" data-size="16" data-c="#000" data-hc="#000" data-loop="true"></i>
                                User Profile</a>
                            </li>
                            <li>
                                <a href="#tab2" data-toggle="tab">
                             <i class="livicon" data-name="key" data-size="16" data-loop="true" data-c="#000" data-hc="#000"></i>
                                Change Password</a>
                            </li>
                            
                        </ul>
                        <div  class="tab-content mar-top">
                            <div id="tab1" class="tab-pane fade active in">
                                <div class="row">
                                    <div class="col-lg-12">
                                    <form action="{{ route('admin.profile.store') }}" class="form-horizontal" method="POST" role="form" enctype="multipart/form-data">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">
                                                   
                                                    User Profile
                                                </h3>

                                            </div>
                                            <div class="panel-body">
                                                <div class="col-md-4">
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <div class="fileinput-new thumbnail img-file">
                                                        <span><img id="target" src="<?php echo (!empty($profile->image)) ? url('/public/uploads/demo') . '/' . $profile['image'] : ""; ?>" style="width:300px; height:180px;"/></span>
                                        				<img data-src="holder.js/100%x100%" alt="..."></div>
                                                        <div class="fileinput-preview fileinput-exists thumbnail img-max"></div>
                                                        <div>
                                                            <span class="btn btn-default btn-file">
                                                                <span class="fileinput-new">Select image</span>
                                                                <span class="fileinput-exists">Change</span>
                                                                <input type="file" name="admin_image" id="admin_image" class="form-control input-md" ></span>
                                                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="panel-body">
                                                        <div class="table-responsive">
                                                            <table class="table table-bordered table-striped" id="users">

                                                                <tr>
                                                                    <td>User Name</td>
                                                                    <td>
                                                                        <a href="#" data-pk="1" class="editable" data-title="Edit User Name">{{$profile['username']}}</a>
                                                                    </td>

                                                                </tr>
                                                                <tr>
                                                                    <td>First Name</td>
                                                                    <td>
                                                                        <a href="#" data-pk="1" class="editable" data-title="Edit First Name">{{$profile['firstname']}}</a>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Last Name</td>
                                                                    <td>
                                                                        <a href="#" data-pk="1" class="editable" data-title="Edit Last Name">{{$profile['lastname']}}</a>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>E-mail</td>
                                                                    <td>
                                                                        <a href="#" data-pk="1" class="editable" data-title="Edit E-mail">{{$profile['email']}}</a>
                                                                    </td>
                                                                </tr>
                                                                
                                                                <tr>
                                                                    <td>Status</td>
                                                                    <td>
                                                                        <a href="#" id="status" data-type="select" data-pk="1" data-value="1" data-title="Status">{{($profile['status']=='1')?'Active':'Inactive'}}</a>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Created At</td>
                                                                    <td>
                                                                        {{$profile['created_at']}}
                                                                    </td>
                                                                </tr>
                                                                
                                                            </table>
                                                            <button type="submit" class="btn btn-primary">Submit</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab2" class="tab-pane fade">
                                <div class="row">
                                    <div class="col-md-12 pd-top">
				                        @if (session('flash_message'))
				                        <div class="flash-message">
				                            <div class="alert alert-{{session('flash_action')}}">
				                                <p>{{session('flash_message')}}</p>
				                            </div>
				                        </div>
				                        @endif
                                        <form action="{{ route('admin.profile.password') }}" class="form-horizontal" method="POST" role="form">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <label for="inputpassword" class="col-md-3 control-label">
                                                        Password
                                                        <span class='require'>*</span>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <div class="input-group {{ $errors->first('password', 'has-error') }}">
                                                            <span class="input-group-addon">
                                                                <i class="livicon" data-name="key" data-size="16" data-loop="true" data-c="#000" data-hc="#000"></i>
                                                            </span>
                                                            <input type="password" name="password" placeholder="Password" class="form-control"/>
                                                        </div>
                                                         {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div></br>
                                             
                                            </div>
                                            <div class="form-actions">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <button type="submit" class="btn btn-primary">Submit</button>
                                                    &nbsp;
                                                    <button type="button" class="btn btn-danger">Cancel</button>
                                                    &nbsp;
                                                    <input type="reset" class="btn btn-default hidden-xs" value="Reset"></div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
    <!-- content -->
    </aside>
    <!-- right-side -->
    </div>
    <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Return to top"
       data-toggle="tooltip" data-placement="left">
        <i class="livicon" data-name="plane-up" data-size="18" data-loop="true" data-c="#fff" data-hc="white"></i>
    </a>
    <!-- global js -->
    <script src="{{ asset('public/assets/js/jquery-1.11.1.min.js') }}" type="text/javascript"></script>
<!--    <script src="{{ asset('public/assets/js/bootstrap.min.js') }}" type="text/javascript"></script>
    livicons
    <script src="{{ asset('public/assets/vendors/livicons/minified/raphael-min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/livicons/minified/livicons-1.4.min.js') }}"></script>
    <script src="{{ asset('public/assets/js/josh.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/assets/js/metisMenu.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/assets/vendors/holder-master/holder.js') }}"></script>-->
@stop

{{-- page level scripts --}}
@section('footer_scripts')    
    <script src="{{ asset('public/assets/vendors/jasny-bootstrap/js/jasny-bootstrap.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('public/assets/vendors/validation/dist/js/bootstrapValidator.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('public/assets/vendors/intl-tel-input/build/js/intlTelInput.min.js') }}"
            type="text/javascript"></script>
    
    <script src="{{ asset('public/assets/js/pages/validation.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/assets/js/pages/jquery.mockjax.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/assets/js/pages/bootstrap-editable.js') }}" type="text/javascript"></script>
    
@stop
