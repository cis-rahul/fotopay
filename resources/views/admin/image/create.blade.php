@extends('admin.layouts.master')
{{-- Page title --}}
@section('title')
    Image
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')

    <link href="{{ asset('public/assets/css/pages/form2.css') }}" rel="stylesheet"/>
    <link href="{{ asset('public/assets/css/pages/form3.css') }}" rel="stylesheet"/>
    <link href="{{ asset('public/assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('public/assets/vendors/intl-tel-input/build/css/intlTelInput.css') }}" rel="stylesheet"/>
    <link href="{{ asset('public/assets/vendors/validation/dist/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
    <style type='text/css'>
        .form-group.required .control-label:after { 
            content:"*";
            color:red;
        }
    </style>
@stop

{{-- Page content --}}
@section('content')
    <?php
        $title = "Create";
        $action = route('admin.image.store');
        $method = '';
        if(!empty($image->id)){
            $title = "Edit";
            $action = route('admin.image.update', $image->id);
            $method = '<input type="hidden" name="_method" value="PUT" />';
        }

    ?>
    <section class="content-header">
        <!--section starts-->
        <h1>Image</h1>

    </section>
    <!--section ends-->
    <section class="content">
        <!--main content-->
        <div class="row">            
            <div class="col-md-12">                
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="livicon" data-name="rocket" data-size="16" data-loop="true" data-c="#fff"
                               data-hc="white"></i>
                            {{$title}}
                        </h3>                        
                        <span class="pull-right clickable">
                            <i class="glyphicon glyphicon-chevron-up"></i>
                        </span>

                    </div>                    
                    <div class="panel-body">
                        <div class="row">
                        @if (session('flash_message'))
                        <div class="flash-message">
                            <div class="alert alert-{{session('flash_action')}}">
                                <p>{{session('flash_message')}}</p>
                            </div>
                        </div>
                        @endif
                            <form action="{{$action}}" method="POST" role="form">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                <input type="hidden" name="id" value="{{@$image->id}}" />
                                <?php echo $method; ?>                       
                                <div class="row">
                                    
                                    <div class="col-xs-12 col-md-12">                                        
                                        <div class="form-group required {{ $errors->first('image_type', 'has-error') }}">                                            
                                            <label class="control-label">Image Type</label>
                                           <input type="text" name="image_type" id="image_type" class="form-control input-md" placeholder="Image Type" value="{{!empty($image->image_title)?$image->image_title:Input::old('image_type')}}">
                                           {!! $errors->first('image_type', '<span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>

                                  @if(isset($image->id) && !empty($image->id))
                                    <div class="row">                                    
                                        <div class="col-xs-12 col-md-12">
                                            <div class="form-group {{ $errors->first('status', 'has-error')}}">
                                                <label class="control-label">Status</label>                                        
                                                <?php                                        
                                                $active = ($image->status == '1')?'checked="checked"':"";
                                                $inactive = ($image->status == '0')?'checked="checked"':"";
                                                ?>                                        
                                                <input name="status" value="1" type="radio" <?php echo $active; ?>/> Active <input type="radio" name="status" value="0" <?php echo $inactive; ?>/> Inactive
                                                {!! $errors->first('status', '<span class="help-block">:message</span>') !!}
                                            </div>                                    
                                        </div>
                                    </div>
                                    @endif
                                
                                <div class="row">                                    
                                    <div class="col-xs-12 col-md-12">
                                        <input type="submit" class="btn btn-primary btn-block btn-md btn-responsive create-btn" value="Create">
                                    </div>

                                </div>    
                            </form>
                        </div>                        
                    </div>
                </div>                
            </div>
        </div>
        
        <!--row ends-->
    </section>
    <!-- content -->
@stop

{{-- page level scripts --}}
@section('footer_scripts')    
    <script src="{{ asset('public/assets/vendors/jasny-bootstrap/js/jasny-bootstrap.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('public/assets/vendors/validation/dist/js/bootstrapValidator.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('public/assets/vendors/intl-tel-input/build/js/intlTelInput.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('public/assets/js/pages/validation.js') }}" type="text/javascript"></script>
@stop
