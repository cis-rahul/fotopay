@extends('admin.layouts.master')
{{-- Page title --}}
@section('title')
Entity
@parent
@stop

{{-- page level styles --}}
@section('header_styles')

<link href="{{ asset('public/assets/css/pages/form2.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/css/pages/form3.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/vendors/intl-tel-input/build/css/intlTelInput.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/vendors/validation/dist/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
<style type='text/css'>
    .form-group.required .control-label:after { 
        content:"*";
        color:red;
    }
</style>
@stop

{{-- Page content --}}
@section('content')

<section class="content-header">
    <!--section starts-->
    <h1>Dashboard</h1>
</section>
<!--section ends-->
<section class="content">
   <span style="margin-left:350px;font-size:30px;"><?php echo "Welcome Nick to Super Admin Dashboard!!";?></span>
</div>   
</section>
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Package Image</h4>
            </div>
            <div class="modal-body">
                <img width="100%" height="auto" id="show_img_src" src=""/>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>      
        </div>
    </div>
</div>
<!-- global js -->
@stop

{{-- page level scripts --}}
@section('footer_scripts')    
<script src="{{ asset('public/assets/vendors/jasny-bootstrap/js/jasny-bootstrap.min.js') }}"
type="text/javascript"></script>
<script src="{{ asset('public/assets/vendors/validation/dist/js/bootstrapValidator.min.js') }}"
type="text/javascript"></script>
<script src="{{ asset('public/assets/vendors/intl-tel-input/build/js/intlTelInput.min.js') }}"
type="text/javascript"></script>
<script src="{{ asset('public/assets/js/pages/validation.js') }}" type="text/javascript"></script>
<script>        
    //"uploads/packages";
    $(".show_img").click(function() {
        var src = "{{url('/public/uploads/packages')}}";
        var imagename = $(this).attr('image-name');
        src = src + "/" + imagename;
        alert(src);
        $("#show_img_src").attr("src", src);
        $('#myModal').modal();
    });
</script>


@stop
