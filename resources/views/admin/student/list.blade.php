@extends('admin.layouts.master')
{{-- Page title --}}
@section('title')
    Student
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')

    <link href="{{ asset('public/assets/css/pages/form2.css') }}" rel="stylesheet"/>
    <link href="{{ asset('public/assets/css/pages/form3.css') }}" rel="stylesheet"/>
    <link href="{{ asset('public/assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('public/assets/vendors/intl-tel-input/build/css/intlTelInput.css') }}" rel="stylesheet"/>
    <link href="{{ asset('public/assets/vendors/validation/dist/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
    <style type='text/css'>
        .form-group.required .control-label:after { 
            content:"*";
            color:red;
        }
    </style>
@stop

{{-- Page content --}}
@section('content')

    <section class="content-header">
        <!--section starts-->
        <h1>Student Management</h1>

    </section>
    <!--section ends-->
    <section class="content">
    <div class="row">
        <div class="col-md-12">            
            <!-- BEGIN SAMPLE TABLE PORTLET-->
            <div class="panel panel-primary filterable">
                <div class="panel-heading clearfix">
                    <div class="panel-heading clearfix  ">
                        <div class="caption pull-left">
                            <i class="livicon" data-name="list" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                            Student List                        
                        </div>
                        <div class="btn-group pull-right">
                            <a href="{{route('admin.student.create')}}" class="btn btn-warning add-anc">Add New Student</a>
                        </div>
                    </div>    
                </div>
                <div class="panel-body table-responsive">
                    @if (session('flash_message'))
                    <div class="flash-message">
                        <div class="alert alert-{{session('flash_action')}}">
                            <p>{{session('flash_message')}}</p>
                        </div>
                    </div>
                    @endif                    
                    <table class="table table-striped table-bordered" id="table-entity">
                        <thead>
                            <tr>
                                <th style="text-align: center;">#</th>
                                <th style="width: 20%;text-align: center;">Student Name</th>
                                <th style="width: 20%;text-align: center;">Phone Number</th>
                                <th style="width: 10%;text-align: center;">Email ID</th>
                                <th style="width: 10%;text-align: center;">Status</th>
                                <th style="text-align: center;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($students))                                                               
                            @foreach($students as $objStudent)
                            <tr>
                                <td style="text-align: center;">{{$objStudent['id']}}</td>
                                <td style="text-align: left;">{{$objStudent['firstname'] . " ". $objStudent['lastname']}}</td>
                                <td style="text-align: center;">{{$objStudent['phone_no']}}</td>
                                <td style="text-align: center;">{{$objStudent['email_id']}}</td>
                                <td style="text-align: center;">{{($objStudent['status']=='1')?'Active':'Inactive'}}</td>
                                <td style="text-align: center;">
                                    <a href="{{route('admin.student.edit',$objStudent['id'])}}" id="edit" name="edit" class=" btn btn-warning add-anc">Edit</a>
                                    <a data-method="delete" href="{{route('admin.student.destroy', $objStudent['id'])}}" id="delete" name="delete" class=" btn btn-danger add-anc" onclick="return confirm('Do you realy want to delete!')">Delete</a>
                                </td>
                            </tr>                                
                            @endforeach                            
                            @endif 
                        </tbody>
                    </table>
                    <span class="pull-right">{!! $students->render() !!}</span>
                </div>
            </div>
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>     
</div>   
</section>
    <!-- content -->
    </aside>
    <!-- right-side -->
    </div>
    <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Return to top"
       data-toggle="tooltip" data-placement="left">
        <i class="livicon" data-name="plane-up" data-size="18" data-loop="true" data-c="#fff" data-hc="white"></i>
    </a>
    <!-- global js -->
    <script src="{{ asset('public/assets/js/jquery-1.11.1.min.js') }}" type="text/javascript"></script>
<!--    <script src="{{ asset('public/assets/js/bootstrap.min.js') }}" type="text/javascript"></script>
    livicons
    <script src="{{ asset('public/assets/vendors/livicons/minified/raphael-min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/livicons/minified/livicons-1.4.min.js') }}"></script>
    <script src="{{ asset('public/assets/js/josh.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/assets/js/metisMenu.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/assets/vendors/holder-master/holder.js') }}"></script>-->
@stop

{{-- page level scripts --}}
@section('footer_scripts')    
    <script src="{{ asset('public/assets/vendors/jasny-bootstrap/js/jasny-bootstrap.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('public/assets/vendors/validation/dist/js/bootstrapValidator.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('public/assets/vendors/intl-tel-input/build/js/intlTelInput.min.js') }}"
            type="text/javascript"></script>
    
    <script src="{{ asset('public/assets/js/pages/validation.js') }}" type="text/javascript"></script>
    
@stop
