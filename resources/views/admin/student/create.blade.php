@extends('admin.layouts.master')
{{-- Page title --}}
@section('title')
Fotopay | Student
@parent
@stop

{{-- page level styles --}}
@section('header_styles')

<link href="{{ asset('public/assets/css/pages/form2.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/css/pages/form3.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/vendors/intl-tel-input/build/css/intlTelInput.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/vendors/validation/dist/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
<style type='text/css'>
    .form-group.required .control-label:after { 
        content:"*";
        color:red;
    }
</style>
@stop

{{-- Page content --}}
@section('content')
<?php
    $title = "Create";
    $action = route('admin.student.store');
    $method = '';
    if(!empty($student->id)){
        $title = "Edit";
        $action = route('admin.student.update', $student->id);
        $method = '<input type="hidden" name="_method" value="PUT" />';
    }
?>


<section class="content-header">
    <!--section starts-->
    <h1>Student</h1>
</section>
<!--section ends-->
<section class="content">
    <!--main content-->
    <div class="row">            
        <div class="col-md-12">                
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="livicon" data-name="rocket" data-size="16" data-loop="true" data-c="#fff"
                           data-hc="white"></i>
                        {{$title}}
                    </h3>                        
                    <span class="pull-right clickable">
                        <i class="glyphicon glyphicon-chevron-up"></i>
                    </span>

                </div>                    
                <div class="panel-body">
                    <div class="row">
                        @if (session('flash_message'))
                        <div class="flash-message">
                            <div class="alert alert-{{session('flash_action')}}">
                                <p>{{session('flash_message')}}</p>
                            </div>
                        </div>
                        @endif
                        <form action="{{$action}}" method="POST" role="form">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <input type="hidden" name="action" value="{{$title}}" />                            
                            <?php echo $method; ?>
                            <div class="row">
                                <div class="col-xs-12 col-md-12">                                        
                                    <div class="form-group required {{ $errors->first('school_id', 'has-error') }}">                                            
                                        <label class="control-label">School</label>
                                        <select class="form-control input-md" id="school_id" name="school_id">
                                            <option value="">Please Select</option>                                            
                                            @if(!empty($schools))
                                                @foreach($schools as $objSchool)
                                                @if($objSchool->id == Input::old('school_id') || (isset($student->school_id) && $objSchool->id == $student->school_id))
                                                <option value="{{$objSchool->id}}" selected="selected">{{$objSchool->name}}</option>
                                                @else
                                                <option value="{{$objSchool->id}}">{{$objSchool->name}}</option>
                                                @endif
                                                @endforeach
                                            @endif
                                        </select>                                        
                                        {!! $errors->first('school_id', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12">                                        
                                    <div class="form-group required {{ $errors->first('grade_id', 'has-error') }}">                                            
                                        <label class="control-label">Grade</label>
                                        <select class="form-control input-md" id="grade_id" name="grade_id">
                                            <option value="">Please Select</option>
                                        </select>
                                        {!! $errors->first('grade_id', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12">                                        
                                    <div class="form-group required {{ $errors->first('firstname', 'has-error') }}">                                            
                                        <label class="control-label">Student First Name</label>
                                        <input type="text" name="firstname" id="firstname" class="form-control input-md" placeholder="Student First Name" value="{{!empty($student->firstname)?$student->firstname:Input::old('firstname')}}">
                                        {!! $errors->first('firstname', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                            </div>                                                   
                            <div class="row">                                    
                                <div class="col-xs-12 col-md-12">
                                    <div class="form-group {{ $errors->first('lastname', 'has-error')}}">
                                        <label class="control-label">Student Last Name</label>
                                        <input id="lastname" name="lastname" class="form-control input-md" value="{{!empty($student->lastname)?$student->lastname:Input::old('lastname')}}" placeholder="Student Last Name"/>                                        
                                        {!! $errors->first('lastname', '<span class="help-block">:message</span>') !!}
                                    </div>                                    
                                </div>
                            </div>
                            <div class="row">                                    
                                <div class="col-xs-12 col-md-12">
                                    <div class="form-group {{ $errors->first('phoneno', 'has-error')}}">
                                        <label class="control-label">Phone Number</label>
                                        <input id="phoneno" name="phoneno" class="form-control input-md" value="{{!empty($student->phone_no)?$student->phone_no:Input::old('phoneno')}}" placeholder="Phone Number"/>                                        
                                        {!! $errors->first('phoneno', '<span class="help-block">:message</span>') !!}
                                    </div>                                    
                                </div>
                            </div>
                            <div class="row">                                    
                                <div class="col-xs-12 col-md-12">
                                    <div class="form-group {{ $errors->first('email', 'has-error')}}">
                                        <label class="control-label">Email</label>
                                        <input id="email" name="email" class="form-control input-md" value="{{!empty($student->email_id)?$student->email_id:Input::old('email')}}" placeholder="Student Email"/>                                        
                                        {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                                    </div>                                    
                                </div>
                            </div>                            
                            @if(isset($student->id) && !empty($student->id))
                            <div class="row">                                    
                                <div class="col-xs-12 col-md-12">
                                    <div class="form-group {{ $errors->first('status', 'has-error')}}">
                                        <label class="control-label">Status</label>                                        
                                        <?php                                        
                                        $active = ($student->status == '1')?'checked="checked"':"";
                                        $inactive = ($student->status == '0')?'checked="checked"':"";
                                        ?>                                        
                                        <input name="status" value="1" type="radio" <?php echo $active; ?>/> Active <input type="radio" name="status" value="0" <?php echo $inactive; ?>/> Inactive
                                        {!! $errors->first('status', '<span class="help-block">:message</span>') !!}
                                    </div>                                    
                                </div>
                            </div>
                            @endif
                            <div class="row">                                    
                                
                                    <div class="col-xs-12 col-md-12">
                                        <input type="submit" class="btn btn-primary btn-block btn-md btn-responsive create-btn" value="Create">
                                    </div>                                                                   
                            </div>
                            
                        </form>
                    </div>                        
                </div>
            </div>                
        </div>
    </div>

    <!--row ends-->
</section>
<!-- content -->
</aside>
<!-- right-side -->
</div>
<a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Return to top"
   data-toggle="tooltip" data-placement="left">
    <i class="livicon" data-name="plane-up" data-size="18" data-loop="true" data-c="#fff" data-hc="white"></i>
</a>
<!-- global js -->
@stop

{{-- page level scripts --}}
@section('footer_scripts')    
<script src="{{ asset('public/assets/vendors/jasny-bootstrap/js/jasny-bootstrap.min.js') }}"
type="text/javascript"></script>
<script src="{{ asset('public/assets/vendors/validation/dist/js/bootstrapValidator.min.js') }}"
type="text/javascript"></script>
<script src="{{ asset('public/assets/vendors/intl-tel-input/build/js/intlTelInput.min.js') }}"
type="text/javascript"></script>    
<script src="{{ asset('public/assets/js/pages/validation.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
        var school_id = "{{!empty($student->school_id)?$student->school_id:Input::old('school_id')}}";
        var grade_id = "{{!empty($student->grade_id)?$student->grade_id:Input::old('grade_id')}}";
        $("#school_id").change(function(){
            var url = "{{url('admin/grade/gradelist/')}}";
            url = url + "/" + $(this).val();
            var htmloption = "<option value=''>Please Select</option>";
            $.ajax({
                url:url,                
                type:'GET',
                dataType: 'JSON',
                success:function(res){                    
                    $.each(res['data'], function(key, val){
                        if(val['id'] == grade_id){
                            htmloption += "<option value='"+ val['id'] +"' selected='selected'>"+ val['title'] +"</option>";
                        }else{
                            htmloption += "<option value='"+ val['id'] +"'>"+ val['title'] +"</option>";
                        }
                    });
                    $("#grade_id").html(htmloption);
                },
                error:function(e){
                    alert("System Error!");
                }
            });
        });
        if(school_id != ""){            
            $("#school_id").change();
        }
    });
</script>
@stop
