@extends('admin.layouts.master')
{{-- Page title --}}
@section('title')
    School
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')

    <link href="{{ asset('public/assets/css/pages/form2.css') }}" rel="stylesheet"/>
    <link href="{{ asset('public/assets/css/pages/form3.css') }}" rel="stylesheet"/>
    <link href="{{ asset('public/assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('public/assets/vendors/intl-tel-input/build/css/intlTelInput.css') }}" rel="stylesheet"/>
    <link href="{{ asset('public/assets/vendors/validation/dist/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
    <style type='text/css'>
        .form-group.required .control-label:after { 
            content:"*";
            color:red;
        }
    </style>
@stop

{{-- Page content --}}
@section('content')

    <section class="content-header">
        <!--section starts-->
        <h1>School Management</h1>

    </section>
    <!--section ends-->
    <section class="content">
    <div class="row">
        <div class="col-md-12">            
            <!-- BEGIN SAMPLE TABLE PORTLET-->
            <div class="panel panel-primary filterable">
                <div class="panel-heading clearfix">
                    <div class="panel-heading clearfix  ">
                        <div class="caption pull-left">
                            <i class="livicon" data-name="list" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                            School List                        
                        </div>
                        <div class="btn-group pull-right">
                            <a href="{{route('admin.school.create')}}" class="btn btn-warning add-anc">Add New School</a>
                        </div>
                    </div>    
                </div>
                <div class="panel-body table-responsive">
                    @if (session('flash_message'))
                    <div class="flash-message">
                        <div class="alert alert-{{session('flash_action')}}">
                            <p>{{session('flash_message')}}</p>
                        </div>
                    </div>
                    @endif   
                    <form action="{{ route('admin.school.index') }}" method="GET" role="form">                    
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <input type="hidden" name="action" value="" />
                    <div class="row">
                        <div class="col-xs-12 col-md-12">                                        
                            <div class="form-group {{ $errors->first('job_title', 'has-error') }}">
                                    <label class="control-label">Search</label>
                                        <input type="text" name="search_keyword" id="search_keyword" class="form-control" style="width:320px;" placeholder="Search by school name and Job code" value="{{Input::old('search_keyword')}}">    
                                    <input type="submit" class="btn btn-primary" style="width:120px;margin-top:-58px;margin-left:404px;" value="Search">
                            </div>
                        </div>
                    </div> 
                    </form>                   
                    <table class="table table-striped table-bordered" id="table-entity">
                        <thead>
                            <tr>
                                <th style="text-align: center;">#</th>
                                <th style="width: 20%;text-align: center;">School Name</th>
                                <th style="width: 20%;text-align: center;">School Code</th>
                                <th style="width: 10%;text-align: center;">Created Date</th>
                                <th style="width: 10%;text-align: center;">Status</th>
                                <th style="text-align: center;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($schools))                                                               
                            @foreach($schools as $objSchool)
                            <tr>
                                <td style="text-align: center;">{{$objSchool['id']}}</td>
                                <td style="text-align: left;">{{$objSchool['name']}}</td>
                                <td style="text-align: center;">{{$objSchool['code']}}</td>
                                <td style="text-align: center;">{{$objSchool['created_at']}}</td>
                                <td style="text-align: center;">{{($objSchool['status']=='1')?'Active':'Inactive'}}</td>
                                <td style="text-align: center;">
                                    <a href="{{route('admin.school.edit',$objSchool['id'])}}" id="edit" name="edit" class=" btn btn-warning add-anc">Edit</a>
                                    <a data-method="delete" href="{{route('admin.school.destroy', $objSchool['id'])}}" id="delete" name="delete" class=" btn btn-danger add-anc" onclick="return confirm('Do you realy want to delete!')">Delete</a>
                                </td>
                            </tr>                                
                            @endforeach                            
                            @endif 
                        </tbody>
                    </table>
                    <span class="pull-right">{!! $schools->render() !!}</span>
                </div>
            </div>
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>     
</div>   
</section>
    <!-- content -->
    </aside>
    <!-- right-side -->
    </div>
    <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Return to top"
       data-toggle="tooltip" data-placement="left">
        <i class="livicon" data-name="plane-up" data-size="18" data-loop="true" data-c="#fff" data-hc="white"></i>
    </a>
    <!-- global js -->
    <script src="{{ asset('public/assets/js/jquery-1.11.1.min.js') }}" type="text/javascript"></script>
<!--    <script src="{{ asset('public/assets/js/bootstrap.min.js') }}" type="text/javascript"></script>
    livicons
    <script src="{{ asset('public/assets/vendors/livicons/minified/raphael-min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/livicons/minified/livicons-1.4.min.js') }}"></script>
    <script src="{{ asset('public/assets/js/josh.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/assets/js/metisMenu.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/assets/vendors/holder-master/holder.js') }}"></script>-->
@stop

{{-- page level scripts --}}
@section('footer_scripts')    
    <script src="{{ asset('public/assets/vendors/jasny-bootstrap/js/jasny-bootstrap.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('public/assets/vendors/validation/dist/js/bootstrapValidator.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('public/assets/vendors/intl-tel-input/build/js/intlTelInput.min.js') }}"
            type="text/javascript"></script>
    
    <script src="{{ asset('public/assets/js/pages/validation.js') }}" type="text/javascript"></script>
    
@stop
