@extends('admin.layouts.master')
{{-- Page title --}}
@section('title')
    Fotopay | Job
@parent
@stop

{{-- page level styles --}}
@section('header_styles')

<link href="{{ asset('public/assets/css/pages/form2.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/css/pages/form3.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/vendors/intl-tel-input/build/css/intlTelInput.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/vendors/validation/dist/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/css/datepicker.css') }}" />
<style type='text/css'>
    .form-group.required .control-label:after { 
        content:"*";
        color:red;
    }
</style>
@stop

{{-- Page content --}}
@section('content')
<?php
    $title = "Create";
    $action = route('admin.job.store');
    $method = '';
    if(!empty($job->id)){
        $title = "Edit";
        $action = route('admin.job.update', $job->id);
        $method = '<input type="hidden" name="_method" value="PUT" />';
    }
?>

<section class="content-header">
    <!--section starts-->
    <h1>Job</h1>
</section>
<!--section ends-->
<section class="content">
    <!--main content-->
    <div class="row">            
        <div class="col-md-12">                
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="livicon" data-name="rocket" data-size="16" data-loop="true" data-c="#fff"
                           data-hc="white"></i>
                        {{$title}}
                    </h3>                        
                    <span class="pull-right clickable">
                        <i class="glyphicon glyphicon-chevron-up"></i>
                    </span>

                </div>                    
                <div class="panel-body">
                    <div class="row">
                        @if (session('flash_message'))
                        <div class="flash-message">
                            <div class="alert alert-{{session('flash_action')}}">
                                <p>{{session('flash_message')}}</p>
                            </div>
                        </div>
                        @endif
                        <form action="{{$action}}" method="POST" role="form">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <input type="hidden" name="action" value="{{$title}}" />
                            <?php echo $method; ?>
                            <div class="row">                                    
                                <div class="col-xs-12 col-md-12">
                                    <div class="form-group required {{ $errors->first('school_id', 'has-error')}}">
                                        <label class="control-label">Package profile name</label>
                                        <select class="form-control input-md" id="packageprofile_id" name="school_id">
                                            <option value="">Please Select</option>
                                            @if(!empty($packageprofile))
                                                @foreach($packageprofile as $objPackageProfile)
                                                @if($objPackageProfile->id == Input::old('packageprofile_id') || (isset($job->packageprofile_id) && $objPackageProfile->id == $job->packageprofile_id))
                                                <option value="{{$objPackageProfile->id}}" selected="selected">{{$objPackageProfile->package_profile_name}}</option>
                                                @else
                                                <option value="{{$objPackageProfile->id}}">{{$objPackageProfile->package_profile_name}}</option>
                                                @endif
                                                @endforeach
                                            @endif
                                        </select>                                        
                                        {!! $errors->first('school_id', '<span class="help-block">:message</span>') !!}
                                    </div>                                    
                                </div>
                            </div>   
                            <div class="row">
                                <div class="col-xs-12 col-md-12">                                        
                                    <div class="form-group {{ $errors->first('job_title', 'has-error') }}">
                                        <label class="control-label">Job Name</label>
                                        <input type="text" name="job_title" id="job_title" class="form-control" placeholder="Job Title" value="{{!empty($job->job_title)?$job->job_title:Input::old('job_title')}}">
                                        {!! $errors->first('job_title', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12">                                        
                                    <div class="form-group {{ $errors->first('job_date', 'has-error') }}">
                                            <label class="control-label">Job Date</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i data-loop="true" data-hc="#000" data-c="#000" data-size="18" data-name="calendar" class="livicon" id="livicon-47"></i>
                                                </span>
                                                <input type="text" name="job_date" id="job_date" class="form-control" placeholder="Job Date" value="{{!empty($job->job_date)?$job->job_date:Input::old('job_date')}}">    
                                            </div>                                                
                                            {!! $errors->first('job_date', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                            </div> 
                              <div class="row">
                                <div class="col-xs-12 col-md-12">                                        
                                    <div class="form-group {{ $errors->first('finished_date', 'has-error') }}">
                                            <label class="control-label">Finish Date</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i data-loop="true" data-hc="#000" data-c="#000" data-size="18" data-name="calendar" class="livicon" id="livicon-48"></i>
                                                </span>
                                                <input type="text" name="finished_date" id="finished_date" class="form-control" placeholder="Finish Date" value="{{!empty($job->finished_date)?$job->finished_date:Input::old('finished_date')}}">    
                                            </div>                                                
                                            {!! $errors->first('finished_date', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                            </div>                                                
                            
                            @if(isset($job->id) && !empty($job->id))                            
                            <div class="row">                                    
                                <div class="col-xs-12 col-md-12">
                                    <div class="form-group {{ $errors->first('status', 'has-error')}}">
                                        <label class="control-label">Status</label>                                        
                                        <?php                                        
                                        $active = ($job->status == '1')?'checked="checked"':"";
                                        $inactive = ($job->status == '0')?'checked="checked"':"";
                                        ?>                                        
                                        <input name="status" value="1" type="radio" <?php echo $active; ?>/> Active <input type="radio" name="status" value="0" <?php echo $inactive; ?>/> Inactive
                                        {!! $errors->first('status', '<span class="help-block">:message</span>') !!}
                                    </div>                                    
                                </div>
                            </div>
                            @else
                            <div class="row">                                    
                                <div class="col-xs-12 col-md-12">
                                    <div class="form-group {{ $errors->first('job_code', 'has-error')}}">
                                        <label class="control-label">Unique Code</label>
                                        <input id="job_code" name="job_code" class="form-control input-md" value="{{!empty($job->job_code)?$job->job_code:Input::old('job_code')}}" placeholder="Job Code"/>
                                        <button type="button" id="gen_unique_code" name="gen_unique_code">Generate Code</button>
                                        {!! $errors->first('job_code', '<span class="help-block">:message</span>') !!}
                                    </div>                                    
                                </div>
                            </div>
                            @endif
                            <div class="row">                                    
                                
                                    <div class="col-xs-12 col-md-12">
                                        <input type="submit" class="btn btn-primary btn-block btn-md btn-responsive create-btn" value="Create">
                                    </div>                                                                   
                            </div>
                            
                        </form>
                    </div>                        
                </div>
            </div>                
        </div>
    </div>

    <!--row ends-->
</section>
<!-- content -->
</aside>
<!-- right-side -->
</div>
<a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Return to top"
   data-toggle="tooltip" data-placement="left">
    <i class="livicon" data-name="plane-up" data-size="18" data-loop="true" data-c="#fff" data-hc="white"></i>
</a>
<!-- global js -->
@stop

{{-- page level scripts --}}
@section('footer_scripts')    
<script src="{{ asset('public/assets/vendors/jasny-bootstrap/js/jasny-bootstrap.min.js') }}"
type="text/javascript"></script>
<script src="{{ asset('public/assets/vendors/validation/dist/js/bootstrapValidator.min.js') }}"
type="text/javascript"></script>
<script src="{{ asset('public/assets/vendors/intl-tel-input/build/js/intlTelInput.min.js') }}"
type="text/javascript"></script>    
<script src="{{ asset('public/assets/js/pages/validation.js') }}" type="text/javascript"></script>

<script type="text/javascript" src="{{ asset('public/assets/js/bootstrap-datepicker.js') }}" ></script>
<script type="text/javascript">
$(document).ready(function(){    
    $("#gen_unique_code").click(function(){         
       var code = "NE";        
       code = code + (Math.random()+' ').substring(4,10);
       $("#job_code").val(code);
        
    });
    $("#job_date").datepicker({
       format: 'yyyy-mm-dd'                
    });
    $("#finished_date").datepicker({
       format: 'yyyy-mm-dd'                
    });
});
</script>    
   
@stop
