@extends('admin.layouts.master')
{{-- Page title --}}
@section('title')
Package Profile
@parent
@stop

{{-- page level styles --}}
@section('header_styles')

<link href="{{ asset('public/assets/css/pages/form2.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/css/pages/form3.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/vendors/intl-tel-input/build/css/intlTelInput.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/vendors/validation/dist/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
<script type="text/javascript" src="http://tinymce.cachefly.net/4.2/tinymce.min.js"></script>

<style type='text/css'>
    .form-group.required .control-label:after { 
        content:"*";
        color:red;
    }
    .internal-panel-heading {
        background: #3a4252 none repeat scroll 0 0;
        height: 34px;
        margin: 9px;
        padding: 1px 4px 3px 5px;
        width: 99%;
    }
    .internal-panel-heading h4{
        color: #fff;
        font-size: 10pt;
        margin-left: 10px;
    }        
</style>
@stop

{{-- Page content --}}
@section('content')
<?php
$title = "Create";
$action = route('admin.packageprofile.store');
$method = '';
if (!empty($packageprofile->id)) {
    $title = "Edit";
    $action = route('admin.packageprofile.update', $packageprofile->id);
    $method = '<input type="hidden" name="_method" value="PUT" />';
}
?>
<section class="content-header">
    <!--section starts-->
    <h1>Package Profile</h1>
    <input type='button' id='btn_add_package' name='btn_add_package' class='btn btn-info pull-right' value='Add Package' style="margin-top: -30px;"/>        

</section>
<!--section ends-->
<section class="content">
    <!--main content-->
    <div class="row">            
        <div class="col-md-12">                
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="livicon" data-name="rocket" data-size="16" data-loop="true" data-c="#fff"
                           data-hc="white"></i>
                        {{$title}}
                    </h3>                        
                    <span class="pull-right clickable">
                        <i class="glyphicon glyphicon-chevron-up"></i>
                    </span>
                </div>                    
                <div class="panel-body">
                    <div class="row">
                        @if (session('flash_message'))
                        <div class="flash-message">
                            <div class="alert alert-{{session('flash_action')}}">
                                <p>{{session('flash_message')}}</p>
                            </div>
                        </div>
                        @endif
                        <form action="{{$action}}" method="POST" role="form" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <input type="hidden" name="id" value="{{@$packageprofile->id}}" />
                            <input type="hidden" name="action" value="{{$title}}" />
                            <?php echo $method; ?>
                            <div class="row" style="padding-left: 0px;padding-right: 0px;">
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12">                                        
                                            <div class="form-group required {{ $errors->first('packageprofile_name', 'has-error') }}">                                            
                                                <label class="control-label">Package Profile Name</label>
                                                <input type="text" name="packageprofile_name" id="packageprofile_name" class="form-control input-md" placeholder="Package Profile Name" value="{{!empty($packageprofile->package_profile_name)?$packageprofile->package_profile_name:Input::old('packageprofile_name')}}">
                                                {!! $errors->first('packageprofile_name', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                    </div>                            
                                    <div class="row">                                    
                                        <div class="col-xs-12 col-md-12">
                                            <div class="form-group {{ $errors->first('packageprofile_image', 'has-error') }}">
                                                <label class="control-label">image</label>
                                                <input type="file" name="packageprofile_image" id="packageprofile_image" class="form-control input-md" >                                                                                        
                                                {!! $errors->first('packageprofile_image', '<span class="help-block">:message</span>') !!}
                                            </div>                                        
                                        </div>
                                    </div>
                                    @if(isset($packageprofile->id) && !empty($packageprofile->id))
                                    <div class="row">                                    
                                        <div class="col-xs-12 col-md-12">
                                            <div class="form-group {{ $errors->first('status', 'has-error')}}">
                                                <label class="control-label">Status</label>                                        
                                                <?php
                                                $active = ($packageprofile->status == '1') ? 'checked="checked"' : "";
                                                $inactive = ($packageprofile->status == '0') ? 'checked="checked"' : "";
                                                ?>                                        
                                                <input name="status" value="1" type="radio" <?php echo $active; ?>/> Active <input type="radio" name="status" value="0" <?php echo $inactive; ?>/> Inactive
                                                {!! $errors->first('status', '<span class="help-block">:message</span>') !!}
                                            </div>                                    
                                        </div>
                                    </div>
                                    @endif
                                </div>
                                <div class="col-md-4">
                                    <span><img id="target" src="<?php echo (!empty($packageprofile->image)) ? url('/public/uploads/demo') . '/' . $packageprofile->image : ""; ?>" style="width:300px; height:180px;"/></span>
                                </div>
                            </div>
                            <div class='packages'>
                                @if(!empty($packages))
                                <?php $i = 1 ?>
                                @foreach($packages as $objPackage)                                
                                <div class="package-internal">
                                    <input type="hidden" name="package[{{$i}}][id]" class="form-control input-md"  value="{{@$objPackage->id}}">
                                    <div class="internal-panel-heading">        
                                        <h4>Packages <a href="javascript:void(0)" class="btb btn-warning remove-package pull-right">Remove</a></h4>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12">
                                            <div class="form-group required">
                                                <label class="control-label">Package Name</label>
                                                <input type="text" name="package[{{$i}}][package_name]" id="package_name" class="form-control input-md" placeholder="Package Name" value="{{@$objPackage->name}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12">
                                            <div class="form-group">
                                                <label class="control-label" > Price </label>
                                                <input type="text" name="package[{{$i}}][package_price]" id="package_price" class="form-control input-md" placeholder="Package Price" value="{{@$objPackage->price}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12">
                                            <div class="form-group">
                                                <label class="control-label" > Description </label>
                                                <textarea id="package_desc_{{$i}}" name="package[{{$i}}][package_desc]" rows="5" col="5" class="form-control input-md mceEditor" row="4" placeholder="Package Description">{{@$objPackage->description}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="padding-left: 0px;padding-right: 0px;">
                                        <div class="col-md-8">                                            
                                            <div class="row">
                                                <div class="col-xs-12 col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label" > image </label>
                                                        <input type="file" name="package_image_{{$i}}" id="package_image" class="form-control input-md">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label" > Apply Postage and handling (Freight) P & H </label></br >
                                                        <input name="package[{{$i}}][ph]" type="radio" checked="checked" value="1" > Yes
                                                        <input name="package[{{$i}}][ph]" type="radio" value="0" > No
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <span><img id="target" src="<?php echo (!empty($packageprofile->image)) ? url('/public/uploads/demo') . '/' . $objPackage['image'] : ""; ?>" style="width:300px; height:180px;"/></span>
                                        </div>
                                    </div>                                        
                                </div>
                                <?php $i++ ?>
                                @endforeach
                                @endif
                            </div>
                            
                            <div class="row">
                                <div class="col-xs-12 col-md-12">
                                    <input type="submit" class="btn btn-primary btn-block btn-md btn-responsive create-btn" value="{{$title}}">
                                </div>
                            </div>
                        </form>
                    </div>                        
                </div>
            </div>                
        </div>
    </div>
    <!--row ends-->
</section>
<!-- content -->
<!-- global js -->
@stop

{{-- page level scripts --}}
@section('footer_scripts')    
<script src="{{ asset('public/assets/vendors/jasny-bootstrap/js/jasny-bootstrap.min.js') }}"
type="text/javascript"></script>
<script src="{{ asset('public/assets/vendors/validation/dist/js/bootstrapValidator.min.js') }}"
type="text/javascript"></script>
<script src="{{ asset('public/assets/vendors/intl-tel-input/build/js/intlTelInput.min.js') }}"
type="text/javascript"></script>
<script src="{{ asset('public/assets/js/pages/validation.js') }}" type="text/javascript"></script>
<script type='text/javascript'>
$(document).ready(function() {
    var len = $(".package-internal").length;
    if (len != "") {
        var i = 1;
        while (len >= i) {
            tinymce.EditorManager.execCommand('mceAddEditor', true, "package_desc_" + i);
            i++;
        }
    }
    $("#btn_add_package").click(function() {
        var len = $(".package-internal").length + 1;
        var html = '<div class="package-internal">';
        html += '<input type="hidden" name="package[' + len + '][id]" class="form-control input-md"  value="">'
        html += '<div class="internal-panel-heading">';
        html += '<h4>Packages <a href="javascript:void(0)" class="btb btn-warning remove-package pull-right">Remove</a></h4>';
        html += '</div>';
        html += '<div class="row">';
        html += '<div class="col-xs-12 col-md-12">';
        html += '<div class="form-group required">';
        html += '<label class="control-label">Package Name</label>';
        html += '<input type="text" name="package[' + len + '][package_name]" id="package_name" class="form-control input-md" placeholder="Package Name" value="">';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        html += '<div class="row">';
        html += '<div class="col-xs-12 col-md-12">';
        html += '<div class="form-group">';
        html += '<label class="control-label" > Price </label>';
        html += '<input type="text" name="package[' + len + '][package_price]" id="package_price" class="form-control input-md" placeholder="Package Price" value="">';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        html += '<div class="row">';
        html += '<div class="col-xs-12 col-md-12">';
        html += '<div class="form-group">';
        html += '<label class="control-label" > Description </label>';
        html += '<textarea id="package_desc_' + len + '" name="package[' + len + '][package_desc]" rows="5" col="5" class="form-control input-md textareaclass" row="4" value="" placeholder="Package Description">';
        html += '</textarea>';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        html += '<div class="row" style="padding-left: 0px;padding-right: 0px;">';
        html += '<div class="col-md-8">';
        html += '<div class="row">';
        html += '<div class="col-xs-12 col-md-12">';
        html += '<div class="form-group">';
        html += '<label class="control-label" > image </label>';
        html += '<input type="file" name="package_image_' + len + '"  id="package_image_' + len + '" class="form-control input-md">';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        html += '<div class="row">';
        html += '<div class="col-xs-12 col-md-12">';
        html += '<div class="form-group">';
        html += '<label class="control-label" > Apply Postage and handling (Freight) P & H </label></br >';
        html += '<input name="package[' + len + '][ph]" type="radio" checked="checked" value="1" > Yes';
        html += '<input name="package[' + len + '][ph]" type="radio" value="0" > No';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        html += '<div class="col-md-4">';
        html += '<span><img id="target" src=" " style="width:300px; height:180px;"/></span>';
        html += '</div>';
        html += '</div>';
        $(".packages").append(html);
        tinymce.EditorManager.execCommand('mceAddEditor', true, "package_desc_" + len);
        $("html, body").animate({ scrollTop: $(document).height()-$(window).height() });
    });
    $("body").on("click", ".remove-package", function() {
        $(this).parents(".package-internal").remove();
    });        
    $('body').on('change','.col-md-8 input[type="file"]', function(event){        
        var dd= $(this).parents('.col-md-8').parent();
        var src = URL.createObjectURL(event.target.files[0]);
        dd.find('img').attr('src',src);                
    });
});
</script>
@stop