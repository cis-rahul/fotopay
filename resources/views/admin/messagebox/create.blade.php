@extends('admin.layouts.master')
{{-- Page title --}}
@section('title')
    Message
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')

    <link href="{{ asset('public/assets/css/pages/form2.css') }}" rel="stylesheet"/>
    <link href="{{ asset('public/assets/css/pages/form3.css') }}" rel="stylesheet"/>
    <link href="{{ asset('public/assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('public/assets/vendors/intl-tel-input/build/css/intlTelInput.css') }}" rel="stylesheet"/>
    <link href="{{ asset('public/assets/vendors/validation/dist/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
    <style type='text/css'>
        .form-group.required .control-label:after { 
            content:"*";
            color:red;
        }
    </style>
    <script type="text/javascript" src="http://tinymce.cachefly.net/4.2/tinymce.min.js"></script>
            <script type="text/javascript">
            tinymce.init({
                selector: "textarea",
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table contextmenu paste"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
            });
            </script>
@stop

{{-- Page content --}}
@section('content')
    <?php
        $title = "Create";
        $action = route('admin.message.store');
        $method = '';
        if(!empty($message->id)){
            $title = "Edit";
            $action = route('admin.message.update', $message->id);
            $method = '<input type="hidden" name="_method" value="PUT" />';
        }

    ?>
    <section class="content-header">
        <!--section starts-->
        <h1>Message</h1>

    </section>
    <!--section ends-->
    <section class="content">
        <!--main content-->
        <div class="row">            
            <div class="col-md-12">                
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="livicon" data-name="rocket" data-size="16" data-loop="true" data-c="#fff"
                               data-hc="white"></i>
                            {{$title}}
                        </h3>                        
                        <span class="pull-right clickable">
                            <i class="glyphicon glyphicon-chevron-up"></i>
                        </span>

                    </div>                    
                    <div class="panel-body">
                        <div class="row">
                        @if (session('flash_message'))
                        <div class="flash-message">
                            <div class="alert alert-{{session('flash_action')}}">
                                <p>{{session('flash_message')}}</p>
                            </div>
                        </div>
                        @endif
                            <form action="{{$action}}" method="POST" role="form">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                <input type="hidden" name="id" value="{{@$message->id}}" />
                                <?php echo $method; ?>                       
                                <div class="row">
                                    
                                    <div class="col-xs-12 col-md-12">                                        
                                        <div class="form-group required {{ $errors->first('message_title', 'has-error') }}">                                            
                                            <label class="control-label">Message Title</label>
                                           <input type="text" name="message_title" id="message_title" class="form-control input-md" placeholder="Message Title" value="{{!empty($message->message_title)?$message->message_title:Input::old('message_title')}}">
                                           {!! $errors->first('message_title', '<span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>
                                   
                                                                      
                                </div>
                                <div class="row">                                    
                                    <div class="col-xs-12 col-md-12">
                                        <div class="form-group {{ $errors->first('message_content', 'has-error') }}">
                                            <label class="control-label">Content</label>
                                            <textarea id="message_content" name="message_content" rows="5" col="5" class="form-control input-md" row="4" value="{{!empty($message->message_content)?$message->message_content:Input::old('message_content')}}" placeholder="Message Content">{{!empty($message->message_content)?$message->message_content:Input::old('message_content')}}</textarea>
                                            {!! $errors->first('message_content', '<span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>  
                                <div class="row">
                                    <div class="col-xs-12 col-md-12">
                                        <input type="submit" class="btn btn-primary btn-block btn-md btn-responsive create-btn" value="Create">
                                    </div>                              
                               </div>
                            </form>
                        </div>                        
                    </div>
                </div>                
            </div>
        </div>
        
        <!--row ends-->
    </section>
    <!-- content -->      
@stop

{{-- page level scripts --}}
@section('footer_scripts')    
    <script src="{{ asset('public/assets/vendors/jasny-bootstrap/js/jasny-bootstrap.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('public/assets/vendors/validation/dist/js/bootstrapValidator.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('public/assets/vendors/intl-tel-input/build/js/intlTelInput.min.js') }}"
            type="text/javascript"></script>    
    <script src="{{ asset('public/assets/js/pages/validation.js') }}" type="text/javascript"></script>                 
@stop
