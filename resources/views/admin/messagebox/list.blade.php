@extends('admin.layouts.master')
{{-- Page title --}}
@section('title')
    Message
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')

    <link href="{{ asset('public/assets/css/pages/form2.css') }}" rel="stylesheet"/>
    <link href="{{ asset('public/assets/css/pages/form3.css') }}" rel="stylesheet"/>
    <link href="{{ asset('public/assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('public/assets/vendors/intl-tel-input/build/css/intlTelInput.css') }}" rel="stylesheet"/>
    <link href="{{ asset('public/assets/vendors/validation/dist/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
    <style type='text/css'>
        .form-group.required .control-label:after { 
            content:"*";
            color:red;
        }
    </style>
@stop

{{-- Page content --}}
@section('content')

    <section class="content-header">
        <!--section starts-->
        <h1>Message</h1>
    </section>
    <!--section ends-->
    <section class="content">
    <div class="row">
        <div class="col-md-12">            
            <!-- BEGIN SAMPLE TABLE PORTLET-->
            <div class="panel panel-primary filterable">
                <div class="panel-heading clearfix">
                    <div class="panel-heading clearfix  ">
                        <div class="caption pull-left">
                            <i class="livicon" data-name="list" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                            Message List                        
                        </div>
                        <div class="btn-group pull-right">
                            <!-- <a href="{{route('admin.message.create')}}" id="" name="" class=" btn btn-warning add-anc">Add New Message</a> -->
                        </div>
                    </div>    
                </div>
                <div class="panel-body table-responsive">
                    @if (session('flash_alert_notice'))
                    <div class="flash-message">
                        <div class="alert alert-{{session('flash_action')}}">
                            <p>{{session('flash_alert_notice')}}</p>
                        </div>
                    </div>
                    @endif
                    <form action="{{ route('admin.message.index') }}" method="GET" role="form">                    
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <input type="hidden" name="action" value="" />
                    <div class="row">
                        <div class="col-xs-12 col-md-12">                                        
                            <div class="form-group {{ $errors->first('', 'has-error') }}">
                                    <label class="control-label">Search</label>
                                        <input type="text" name="search_keyword" id="search_keyword" class="form-control" style="width:320px;" placeholder="Search by Message Title or message content" value="{{Input::old('search_keyword')}}">    
                                    <input type="submit" class="btn btn-primary" style="width:120px;margin-top:-58px;margin-left:404px;" value="Search">
                            </div>
                        </div>
                    </div> 
                    </form>                    
                    <table class="table table-striped table-bordered" id="table-entity">
                        <thead>
                            <tr>                                    
                                <th style="width: 30%;text-align:center;">Message Title</th>
                                <th style="width: 30%;text-align:center;">Message Content</th>
                                <th style="width: 30%;text-align:center;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($message_data))                                                               
                            @foreach($message_data as $objEntity)
                            <tr>    

                                <td>{{$objEntity['message_title']}}</td>
                                <td><?php echo htmlspecialchars_decode($objEntity['message_content']);?></td>
                                <td style="width: 30%;text-align:center;">
                                    <a href="{{route('admin.message.edit',$objEntity['id'])}}" id="edit" name="edit" class=" btn btn-warning add-anc">Edit</a>
                                    <a href="javascript:void()"  id="{{ $objEntity['id'] }}" class="btn btn-danger add-anc del-msg">Delete</a>
                                </td>
                            </tr>                                
                           @endforeach                            
                            @endif
                        </tbody>
                    </table>
                    <span class="pul-right">{!! $message_data->render() !!}</span>
                </div>
            </div>
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>     
</div>   
</section>    
@stop

{{-- page level scripts --}}
@section('footer_scripts')    
    <script src="{{ asset('public/assets/vendors/jasny-bootstrap/js/jasny-bootstrap.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('public/assets/vendors/validation/dist/js/bootstrapValidator.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('public/assets/vendors/intl-tel-input/build/js/intlTelInput.min.js') }}"
            type="text/javascript"></script>    
    <script src="{{ asset('public/assets/js/pages/validation.js') }}" type="text/javascript"></script>         
  <script>

  
  </script>
     
     
@stop
