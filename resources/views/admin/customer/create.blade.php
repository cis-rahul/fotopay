@extends('admin.layouts.master')
{{-- Page title --}}
@section('title')
Fotopay | Customers
@parent
@stop

{{-- page level styles --}}
@section('header_styles')

<link href="{{ asset('public/assets/css/pages/form2.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/css/pages/form3.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/vendors/intl-tel-input/build/css/intlTelInput.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/vendors/validation/dist/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
<style type='text/css'>
    .form-group.required .control-label:after { 
        content:"*";
        color:red;
    }
</style>
@stop

{{-- Page content --}}
@section('content')
<?php
    $title = "Create";
    $action = route('admin.customer.store');
    $method = '';
    if(!empty($customer->id)){
        $title = "Edit";
        $action = route('admin.customer.update', $customer->id);
        $method = '<input type="hidden" name="_method" value="PUT" />';
    }
?>


<section class="content-header">
    <!--section starts-->
    <h1>Customer</h1>
</section>
<!--section ends-->
<section class="content">
    <!--main content-->
    <div class="row">            
        <div class="col-md-12">                
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="livicon" data-name="rocket" data-size="16" data-loop="true" data-c="#fff"
                           data-hc="white"></i>
                        {{$title}}
                    </h3>                        
                    <span class="pull-right clickable">
                        <i class="glyphicon glyphicon-chevron-up"></i>
                    </span>

                </div>                    
                <div class="panel-body">
                    <div class="row">
                        @if (session('flash_message'))
                        <div class="flash-message">
                            <div class="alert alert-{{session('flash_action')}}">
                                <p>{{session('flash_message')}}</p>
                            </div>
                        </div>
                        @endif
                        <form action="{{$action}}" method="POST" role="form">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <input type="hidden" name="action" value="{{$title}}" />
                            <?php echo $method; ?>
                            <div class="row">
                                <div class="col-xs-12 col-md-12">                                        
                                    <div class="form-group required {{ $errors->first('fname', 'has-error') }}">                                            
                                        <label class="control-label">First Name</label>
                                        <input type="text" name="fname" id="fname" class="form-control input-md" placeholder="First Name" value="{{!empty($customer->firstname)?$customer->firstname:Input::old('fname')}}">
                                        {!! $errors->first('fname', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                            </div>                                                   
                            <div class="row">
                                <div class="col-xs-12 col-md-12">                                        
                                    <div class="form-group required {{ $errors->first('lname', 'has-error') }}">                                            
                                        <label class="control-label">Last Name</label>
                                        <input type="text" name="lname" id="lname" class="form-control input-md" placeholder="Last Name" value="{{!empty($customer->lastname)?$customer->lastname:Input::old('lname')}}">
                                        {!! $errors->first('lname', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                            </div> 
                            <div class="row">
                                <div class="col-xs-12 col-md-12">                                        
                                    <div class="form-group required {{ $errors->first('email', 'has-error') }}">                                            
                                        <label class="control-label">Email</label>
                                        <input type="text" name="email" id="email" class="form-control input-md" placeholder="Email" value="{{!empty($customer->email)?$customer->email:Input::old('email')}}">
                                        {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                            </div> 
                              <div class="row">
                                <div class="col-xs-12 col-md-12">                                        
                                    <div class="form-group required {{ $errors->first('contact', 'has-error') }}">                                            
                                        <label class="control-label">Contact Number</label>
                                        <input type="text" name="contact" id="contact" class="form-control input-md" placeholder="Contact Number" value="{{!empty($customer->contact)?$customer->contact:Input::old('contact')}}">
                                        {!! $errors->first('contact', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                            </div> 
                            @if(isset($customer->id) && !empty($customer->id))
                            <div class="row">                                    
                                <div class="col-xs-12 col-md-12">
                                    <div class="form-group {{ $errors->first('status', 'has-error')}}">
                                        <label class="control-label">Status</label>                                        
                                        <?php                                        
                                        $active = ($customer->status == '1')?'checked="checked"':"";
                                        $inactive = ($customer->status == '0')?'checked="checked"':"";
                                        ?>                                        
                                        <input name="status" value="1" type="radio" <?php echo $active; ?>/> Active <input type="radio" name="status" value="0" <?php echo $inactive; ?>/> Inactive
                                        {!! $errors->first('status', '<span class="help-block">:message</span>') !!}
                                    </div>                                    
                                </div>
                            </div>
                            @endif
                            <div class="row">                                    
                                
                                    <div class="col-xs-12 col-md-12">
                                        <input type="submit" class="btn btn-primary btn-block btn-md btn-responsive create-btn" value="Create">
                                    </div>                                                                   
                            </div>
                            
                        </form>
                    </div>                        
                </div>
            </div>                
        </div>
    </div>

    <!--row ends-->
</section>
<!-- content -->
</aside>
<!-- right-side -->
</div>
<a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Return to top"
   data-toggle="tooltip" data-placement="left">
    <i class="livicon" data-name="plane-up" data-size="18" data-loop="true" data-c="#fff" data-hc="white"></i>
</a>
<!-- global js -->
@stop

{{-- page level scripts --}}
@section('footer_scripts')    
<script src="{{ asset('public/assets/vendors/jasny-bootstrap/js/jasny-bootstrap.min.js') }}"
type="text/javascript"></script>
<script src="{{ asset('public/assets/vendors/validation/dist/js/bootstrapValidator.min.js') }}"
type="text/javascript"></script>
<script src="{{ asset('public/assets/vendors/intl-tel-input/build/js/intlTelInput.min.js') }}"
type="text/javascript"></script>    
<script src="{{ asset('public/assets/js/pages/validation.js') }}" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){    
    $("#gen_unique_code").click(function(){ 
        if($("#name").val() != ""){
            var code = "SC";
            code = code + $("#name").val().substring(0, 2).toUpperCase();
            code = code + (Math.random()+' ').substring(4,10);
            $("#code").val(code);
        }else{
            alert("Please fill the school name first!");
        }
    });        
});
</script>
@stop
