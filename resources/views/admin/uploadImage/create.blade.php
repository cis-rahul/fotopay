@extends('admin.layouts.master')
{{-- Page title --}}
@section('title')
UploadImage 
@parent
@stop

{{-- page level styles --}}
@section('header_styles')

<link href="{{ asset('public/assets/css/pages/form2.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/css/pages/form3.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/vendors/intl-tel-input/build/css/intlTelInput.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/vendors/validation/dist/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/vendors/enyo/dropzone/dist/dropzone.css') }}" rel="stylesheet"/>

<style type='text/css'>
    .form-group.required .control-label:after { 
        content:"*";
        color:red;
    }
</style>
@stop

{{-- Page content --}}
@section('content')
<?php
$title = "Create";
$action = route('admin.uploadImage.store');
$method = '';
if (!empty($uploadImage->id)) {
    $title = "Edit";
    $action = route('admin.uploadImage.update', $uploadImage->id);
    $method = '<input type="hidden" name="_method" value="PUT" />';
}
?>


<section class="content-header">
    <!--section starts-->
    <h1>Upload Image</h1>
</section>
<!--section ends-->
<section class="content">
    <!--main content-->
    <div class="row">            
        <div class="col-md-12">                
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="livicon" data-name="rocket" data-size="16" data-loop="true" data-c="#fff"
                           data-hc="white"></i>
                        {{$title}}
                    </h3>                        
                    <span class="pull-right clickable">
                        <i class="glyphicon glyphicon-chevron-up"></i>
                    </span>

                </div>                    
                <div class="panel-body">
                    <div class="row">
                        @if (session('flash_message'))
                        <div class="flash-message">
                            <div class="alert alert-{{session('flash_action')}}">
                                <p>{{session('flash_message')}}</p>
                            </div>
                        </div>
                        @endif
                        <form action="{{$action}}" method="POST" role="form">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <input type="hidden" name="action" value="{{$title}}" />                            
                            <?php echo $method; ?>
                            <div class="row">
                                <div class="col-xs-12 col-md-12">                                        
                                    <div class="form-group required {{ $errors->first('school_id', 'has-error') }}">                                            
                                        <label class="control-label">School</label>
                                        <select class="form-control input-md" id="school_id" name="school_id">
                                            <option value="">Please Select</option>                                            
                                            @if(!empty($schools))
                                            @foreach($schools as $objSchool)
                                            @if($objSchool->id == Input::old('school_id') || (isset($uploadImage->school_id) && $objSchool->id == $uploadImage->school_id))
                                            <option value="{{$objSchool->id}}" selected="selected">{{$objSchool->name}}</option>
                                            @else
                                            <option value="{{$objSchool->id}}">{{$objSchool->name}}</option>
                                            @endif
                                            @endforeach
                                            @endif
                                        </select>                                        
                                        {!! $errors->first('school_id', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12">                                        
                                    <div class="form-group {{ $errors->first('grade_id', 'has-error') }}">                                            
                                        <label class="control-label">Grade</label>
                                        <select class="form-control input-md" id="grade_id" name="grade_id">
                                            <option value="">Please Select</option>
                                        </select>
                                        {!! $errors->first('grade_id', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12">                                        
                                    <div class="form-group {{ $errors->first('student_id', 'has-error') }}">
                                        <label class="control-label">Student</label>
                                        <select class="form-control input-md" id="student_id" name="student_id">
                                            <option value="">Please Select</option>
                                        </select>
                                        {!! $errors->first('student_id', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                            </div>                                                        
                            @if(isset($uploadImage->id) && !empty($uploadImage->id))
                            <div class="row">                                    
                                <div class="col-xs-12 col-md-12">
                                    <div class="form-group {{ $errors->first('status', 'has-error')}}">
                                        <label class="control-label">Status</label>                                        
                                        <?php
                                        $active = ($uploadImage->status == '1') ? 'checked="checked"' : "";
                                        $inactive = ($uploadImage->status == '0') ? 'checked="checked"' : "";
                                        ?>                                        
                                        <input name="status" value="1" type="radio" <?php echo $active; ?>/> Active <input type="radio" name="status" value="0" <?php echo $inactive; ?>/> Inactive
                                        {!! $errors->first('status', '<span class="help-block">:message</span>') !!}
                                    </div>                                    
                                </div>
                            </div>
                            @endif
                            <div class="row">
                                <div class="col-xs-12 col-md-12">                                        
                                    <div class="dropzone" id="myId">
                                        <div class="fallback">
                                            <input id="files" multiple="true" name="files" type="file">
                                        </div>
                                        <div id="uploadimages">

                                        </div>
                                    </div>                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12">
                                    <input type="submit" class="btn btn-primary btn-block btn-md btn-responsive create-btn" value="Create">
                                </div>                                                                   
                            </div>
                        </form>
                    </div>                        
                </div>
            </div>                
        </div>
    </div>

    <!--row ends-->
</section>
<!-- content -->
@stop

{{-- page level scripts --}}
@section('footer_scripts')    
<script src="{{ asset('public/assets/vendors/jasny-bootstrap/js/jasny-bootstrap.min.js') }}"
type="text/javascript"></script>
<script src="{{ asset('public/assets/vendors/validation/dist/js/bootstrapValidator.min.js') }}"
type="text/javascript"></script>
<script src="{{ asset('public/assets/vendors/intl-tel-input/build/js/intlTelInput.min.js') }}"
type="text/javascript"></script>    
<script src="{{ asset('public/assets/js/pages/validation.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/vendors/enyo/dropzone/dist/dropzone.js') }}" type="text/javascript"></script>
<script type="text/javascript">

$(document).ready(function() {
    /*
     * Get the Grade list by school id
     */
    var school_id = "{{!empty($uploadImage->school_id)?$uploadImage->school_id:Input::old('school_id')}}";
    var grade_id = "{{!empty($uploadImage->grade_id)?$uploadImage->grade_id:Input::old('grade_id')}}";
    var student_id = "{{!empty($uploadImage->student_id)?$uploadImage->student_id:Input::old('student_id')}}";
    $("#school_id").change(function() {
        $("#grade_id").html("<option value=''>Please Select</option>");
        $("#student_id").html("<option value=''>Please Select</option>");
        var url = "{{url('admin/grade/gradelist/')}}";
        url = url + "/" + $(this).val();
        var htmloption = "<option value=''>Please Select</option>";
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'JSON',
            success: function(res) {
                $.each(res['data'], function(key, val) {
                    if (val['id'] == grade_id) {
                        htmloption += "<option value='" + val['id'] + "' selected='selected'>" + val['title'] + "</option>";
                    } else {
                        htmloption += "<option value='" + val['id'] + "'>" + val['title'] + "</option>";
                    }
                });
                $("#grade_id").html(htmloption);
            },
            error: function(e) {
                alert("System Error!");
            }
        });
    });
    if (school_id != "") {
        $("#school_id").change();
    }
    /*
     * Get the student list by grade id
     */
    $("#grade_id").change(function() {
        $("#student_id").html("<option value=''>Please Select</option>");
        var url = "{{url('admin/student/studentlist')}}";
        url = url + "/" + ((grade_id == "") ? $(this).val() : grade_id);
        var htmloption = "<option value=''>Please Select</option>";
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'JSON',
            success: function(res) {
                $.each(res['data'], function(key, val) {
                    if (val['id'] == student_id) {
                        htmloption += "<option value='" + val['id'] + "' selected='selected'>" + val['firstname'] + " " + val['lastname'] + "</option>";
                    } else {
                        htmloption += "<option value='" + val['id'] + "'>" + val['firstname'] + " " + val['lastname'] + "</option>";
                    }
                });
                $("#student_id").html(htmloption);
            },
            error: function(e) {
                alert("System Error!");
            }
        });
    });
    if (grade_id != "") {
        $("#grade_id").change();
    }
    $("div#myId").dropzone({
        url: "{{route('admin.uploadImage.upload')}}",
        addRemoveLinks: true,
        dictDefaultMessage: '<span class="text-center"><span class="font-lg visible-xs-block visible-sm-block visible-lg-block"><span class="font-lg"><i class="fa fa-caret-right text-danger"></i> Drop files <span class="font-xs">to upload</span></span><span>&nbsp&nbsp<h4 class="display-inline"> (Or Click)</h4></span>',
        dictResponseError: 'Error uploading file!',
        init: function() {            
            //Call the action method to load the images from the server
            var thisDropzone = this;
            $.getJSON("/fotopay/admin/uploadImage/getImages/<?php echo $uploadImage->id ?>").done(function(data) {
                if (data.Data != '') {
                    $.each(data.Data, function(index, item) {
                        //// Create the mock file:
                        var mockFile = {
                            name: item.FileName,
                            size: 12345,                            
                        };
                        
                        console.log(mockFile);
                        // Call the default addedfile event handler
                        thisDropzone.emit("addedfile", mockFile);
                        // And optionally show the thumbnail of the file:
                        this.emit("thumbnail", mockFile, item.Path);
                        // If you use the maxFiles option, make sure you adjust it to the
                        // correct amount:
                        //var existingFileCount = 1; // The number of files already uploaded
                        //myDropzone.options.maxFiles = myDropzone.options.maxFiles - existingFileCount;
                    });
                }
            });
        },
        sending: function(file, xhr, formData) {
            formData.append('_token', "{{csrf_token()}}");
            formData.append('student_id', $("#student_id").val());
        },
        success: function(file, newfilename) {
            file.newfilename = newfilename;
            $("#uploadimages").append("<input type='hidden' name='uploadimages[]' value='" + newfilename + "'>");

        },
        removedfile: function(file) {
            removeImage(file.newfilename);
            $("input[value='" + file.newfilename + "']").remove();
            var _ref;
            return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
        },
    });


});
function removeImage(imgNm) {
    $.ajax({
        url: "{{route('admin.uploadImage.remove')}}",
        type: 'POST',
        dataType: 'JSON',
        data: ({imgNm: imgNm, _token: "{{csrf_token()}}"}),
        success: function(response) {
            console.log(response);
        },
        error: function(response) {

        }
    });
}
</script>
@stop
