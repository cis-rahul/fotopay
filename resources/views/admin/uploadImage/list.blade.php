@extends('admin.layouts.master')
{{-- Page title --}}
@section('title')
UploadImage
@parent
@stop
{{-- page level styles --}}
@section('header_styles')

<link href="{{ asset('public/assets/css/pages/form2.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/css/pages/form3.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/vendors/intl-tel-input/build/css/intlTelInput.css') }}" rel="stylesheet"/>
<link href="{{ asset('public/assets/vendors/validation/dist/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
<style type='text/css'>
    .form-group.required .control-label:after { 
        content:"*";
        color:red;
    }
</style>
@stop

{{-- Page content --}}
@section('content')

<section class="content-header">
    <!--section starts-->
    <h1>UploadImage Management</h1>

</section>
<!--section ends-->
<section class="content">
    <div class="row">
        <div class="col-md-12">            
            <!-- BEGIN SAMPLE TABLE PORTLET-->
            <div class="panel panel-primary filterable">
                <div class="panel-heading clearfix">
                    <div class="panel-heading clearfix  ">
                        <div class="caption pull-left">
                            <i class="livicon" data-name="list" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                            UploadImage List                        
                        </div>
                        <div class="btn-group pull-right">
                            <a href="{{route('admin.uploadImage.create')}}" class="btn btn-warning add-anc">Add New UploadImage</a>
                        </div>
                    </div>    
                </div>
                <div class="panel-body table-responsive">
                    @if (session('flash_message'))
                    <div class="flash-message">
                        <div class="alert alert-{{session('flash_action')}}">
                            <p>{{session('flash_message')}}</p>
                        </div>
                    </div>
                    @endif                    
                    <table class="table table-striped table-bordered" id="table-entity">
                        <thead>
                            <tr>
                                <th style="text-align: center;width: 5%;">#</th>
                                <th style="width: 20%;text-align: center;">School Name</th>
                                <th style="width: 20%;text-align: center;">Grade Name</th>                                
                                <th style="width: 20%;text-align: center;">Student Name</th>
                                <th style="width: 10%;text-align: center;">Created Date</th>
                                <th style="width: 10%;text-align: center;">Modification Date</th>
                                <th style="width: 10%;text-align: center;">Status</th>
                                <th style="text-align: center;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($uploadImages))                                                               
                            @foreach($uploadImages as $objUploadImage)
                            <tr>
                                <td style="text-align: center;">{{$objUploadImage['id']}}</td>
                                <td style="text-align: center;">{{$objUploadImage['school_name']}}</td>
                                <td style="text-align: center;">{{$objUploadImage['grade_name']}}</td>
                                <td style="text-align: center;">{{$objUploadImage['firstname'] . " ". $objUploadImage['lastname']}}</td>
                                <td style="text-align: center;">{{date('d M, Y', strtotime($objUploadImage['created_at']))}}</td>
                                <td style="text-align: center;">{{date('d M, Y', strtotime($objUploadImage['updated_at']))}}</td>
                                <td style="text-align: center;">{{($objUploadImage['status']=='1')?'Active':'Inactive'}}</td>
                                <td style="text-align: center;">
                                    <a href="{{route('admin.uploadImage.edit',$objUploadImage['id'])}}" id="edit" name="edit" class=" btn btn-warning add-anc">Edit</a>
                                    <a data-method="delete" href="{{route('admin.uploadImage.destroy', $objUploadImage['id'])}}" id="delete" name="delete" class=" btn btn-danger add-anc" onclick="return confirm('Do you realy want to delete!')">Delete</a>
                                </td>
                            </tr>                                
                            @endforeach                            
                            @endif

                        </tbody>
                    </table>
                    <span class="pull-right">{!! $uploadImages->render() !!}</span>
                </div>
            </div>
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>     
</div>   
</section>
@stop
{{-- page level scripts --}}
@section('footer_scripts')    
<script src="{{ asset('public/assets/vendors/jasny-bootstrap/js/jasny-bootstrap.min.js') }}"
type="text/javascript"></script>
<script src="{{ asset('public/assets/vendors/validation/dist/js/bootstrapValidator.min.js') }}"
type="text/javascript"></script>
<script src="{{ asset('public/assets/vendors/intl-tel-input/build/js/intlTelInput.min.js') }}"
type="text/javascript"></script>

<script src="{{ asset('public/assets/js/pages/validation.js') }}" type="text/javascript"></script>

@stop
