<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>
            Fotopay
        </title>
        
        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('public/assets/images/favicons/apple-touch-icon.png') }}"/>
        <link rel="icon" type="image/png" href="{{ asset('public/assets/images/favicons/favicon-32x32.png') }}" sizes="32x32"/>
        <link rel="icon" type="image/png" href="{{ asset('public/assets/images/favicons/favicon-16x16.png') }}" sizes="16x16"/>
        <link rel="manifest" href="{{ asset('public/assets/images/favicons/manifest.json') }}"/>
        <link rel="mask-icon" href="{{ asset('public/assets/images/favicons/safari-pinned-tab.svg') }}" color="#5bbad5">
        <meta name="theme-color" content="#ffffff">
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>       
        <meta name="robots" content="noindex">
        <meta name="googlebot" content="noindex">
        <link href="{{ asset('public/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('public/assets/vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css"/>        
        <link href="{{ asset('public/assets/css/styles/black.css') }}" rel="stylesheet" type="text/css" id="colorscheme"/>
        <link href="{{ asset('public/assets/css/panel.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('public/assets/css/metisMenu.css') }}" rel="stylesheet" type="text/css"/> 
        <style>             
            .sidebar-nav{       
                margin-top: 30px;       
            }       
        </style>
<!--        <script src="{{ asset('http://code.jquery.com/jquery-1.12.3.js') }}" type="text/javascript"></script>        -->
        <script src="{{ asset('public/assets/js/jquery-1.11.1.min.js') }}" type="text/javascript"></script>
        <script type="text/javascript" src="http://code.jquery.com/ui/1.10.1/jquery-ui.min.js"></script>
        <!-- end of global css -->
        <!--page level css-->       
        @yield('header_styles')
        <!--end of page level css-->
    </head>

    <body class="skin-josh">
        <header class="header">
            <span class="logo">                
                <a href="{{ route('admin.dashboard') }}" ><img width="180px;" src="{{ asset('public/assets/images/logo.png') }}" alt="logo"></a>               
            </span>
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <div>
                    <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                        <div class="responsive_nav"></div>
                    </a>
                </div>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <li class="dropdown messages-menu">
                            <!-- <a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
                                <i class="livicon"
                                   data-name="message-flag"
                                   data-loop="true" data-color="#42aaca"
                                   data-hovercolor="#42aaca"
                                   data-size="28"></i>
                                <span class="label label-success">4</span>
                            </a> -->
                            <ul class="dropdown-menu dropdown-messages pull-right">
                                <li class="dropdown-title">4 New Messages</li>
                                <li class="unread message">
                                    <a href="javascript:;" class="message"> <i class="pull-right" data-toggle="tooltip"
                                                                               data-placement="top" title="Mark as Read"><span
                                                class="pull-right ol livicon" data-n="adjust" data-s="10"
                                                data-c="#287b0b"></span></i>
                                        <img src="{{ asset('public/assets/img/authors/avatar.jpg') }}"
                                             class="img-responsive message-image" alt="icon">

                                        <div class="message-body">
                                            <strong>Riot Zeast</strong>
                                            <br>Hello, You there?
                                            <br>
                                            <small>8 minutes ago</small>
                                        </div>
                                    </a>
                                </li>
                                <li class="unread message">
                                    <a href="javascript:;" class="message">
                                        <i class="pull-right" data-toggle="tooltip" data-placement="top"
                                           title="Mark as Read"><span class="pull-right ol livicon" data-n="adjust" data-s="10"
                                                                   data-c="#287b0b"></span></i>
                                        <img src="{{ asset('public/assets/img/authors/avatar1.jpg') }}"class="img-responsive message-image" alt="icon">

                                        <div class="message-body">
                                            <strong>John Kerry</strong>
                                            <br>Can we Meet ?
                                            <br>
                                            <small>45 minutes ago</small>
                                        </div>
                                    </a>
                                </li>
                                <li class="unread message">
                                    <a href="javascript:;" class="message">
                                        <i class="pull-right" data-toggle="tooltip" data-placement="top" title="Mark as Read">
                                            <span class="pull-right ol livicon" data-n="adjust" data-s="10"
                                                  data-c="#287b0b"></span>
                                        </i>
                                        <img src="{{ asset('public/assets/img/authors/avatar5.jpg') }}"
                                             class="img-responsive message-image" alt="icon">

                                        <div class="message-body">
                                            <strong>Jenny Kerry</strong>
                                            <br>Dont forgot to call...
                                            <br>
                                            <small>An hour ago</small>
                                        </div>
                                    </a>
                                </li>
                                <li class="unread message">
                                    <a href="javascript:;" class="message">
                                        <i class="pull-right" data-toggle="tooltip" data-placement="top" title="Mark as Read">
                                            <span class="pull-right ol livicon" data-n="adjust" data-s="10"
                                                  data-c="#287b0b"></span>
                                        </i>
                                        <img src="{{ asset('public/assets/img/authors/avatar4.jpg') }}"
                                             class="img-responsive message-image" alt="icon">

                                        <div class="message-body">
                                            <strong>Ronny</strong>
                                            <br>Hey! sup Dude?
                                            <br>
                                            <small>3 Hours ago</small>
                                        </div>
                                    </a>
                                </li>
                                <li class="footer">
                                    <a href="#">View all</a>
                                </li>
                            </ul>
                        </li>                        
                        <li class="dropdown user user-menu">
                            <?php
                                $userinfo = Auth::user();                                
                            ?>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false">
                                <img width="35" height="35" alt="35x35" class="img-circle img-responsive pull-left" style="width: 35px; height: 35px;" src="{{ asset('public/assets/img/authors/avatar.jpg') }}">
                                <div class="riot">
                                    <div>
                                        {{ucfirst(@$userinfo->firstname)}}
                                        <span>
                                            <i class="caret"></i>
                                        </span>
                                    </div>
                                </div>
                            </a>
                            
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header bg-light-blue">
                                    <img alt="90x90" class="img-responsive img-circle" style="width: 90px; height: 90px;" src="{{ asset('public/assets/img/authors/avatar.jpg') }}">
                                    <p class="topprofiletext">{{ucfirst(@$userinfo->firstname) . " " . ucfirst(@$userinfo->lastname)}} </p>
                                </li>
                                <!-- Menu Body -->
                                <li>
                                    <a href="javascript:void(0)">
                                        <i data-s="18" data-name="user" class="livicon" id="livicon-21" style="width: 18px; height: 18px;"></i>
                                        My Profile
                                    </a>
                                </li>
                                <li role="presentation"></li>                                                           
                                <!-- Menu Footer-->
                                <li>                                                                        
                                    <a href="{{route('admin.logout')}}">
                                        <i data-s="18" data-name="sign-out" class="livicon" id="livicon-24" style="width: 18px; height: 18px;"></i>
                                        Logout
                                    </a>                                    
                                </li>
                            </ul>                            
                        </li>
                    </ul>
                </div>
            </nav>
        </header>

        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas left-menu ">
                <section class="sidebar ">
                    <div class="page-sidebar  sidebar-nav">                        
                        <div class="clearfix"></div>
                        <div class="nav_icons">     
                            <ul class="sidebar_threeicons">     
                                <li>        
                                    <a href="javascript:void(0)">       
                                        <i class="livicon" data-name="hammer" title="Form Builder 1" data-loop="true" data-color="#42aaca" data-hc="#42aaca" data-s="25"></i>       
                                    </a>        
                                </li>       
                                <li>        
                                    <a href="javascript:void(0)">       
                                        <i class="livicon" data-name="list-ul" title="Form Builder 2" data-loop="true" data-color="#e9573f" data-hc="#e9573f" data-s="25"></i>      
                                    </a>        
                                </li>       
                                <li>        
                                    <a href="javascript:void(0)">       
                                        <i class="livicon" data-name="vector-square" title="Button Builder" data-loop="true" data-color="#f6bb42" data-hc="#f6bb42" data-s="25"></i>        
                                    </a>        
                                </li>       
                                <li>        
                                    <a href="javascript:void(0)">       
                                        <i class="livicon" data-name="new-window" title="Form Builder 1" data-loop="true" data-color="#37bc9b" data-hc="#37bc9b" data-s="25"></i>       
                                    </a>        
                                </li>       
                            </ul>       
                        </div>      
                        <div class="clearfix"></div>
                        <!-- BEGIN SIDEBAR MENU -->                         
                        <!-- Left Menu Start-->                        
                        <ul id="menu" class="page-sidebar-menu">                                                        
                            <li {!! (Request::is('admin/dashboard') ? 'class="active"' : '') !!}>
                                <a href="{{ route('admin.dashboard') }}">
                                    <i class="livicon" data-name="home" data-size="18" data-c="#418BCA" data-hc="#418BCA"
                                       data-loop="true"></i>
                                    <span class="title">Dashboard</span>
                                </a>
                            </li>
                            <li {!! ((Request::is('admin/package') || Request::is('admin/package/*') || Request::is('admin/packageprofile') || Request::is('admin/packageprofile/*')) ? 'class="active"' : '') !!}>
                                <a href="{{ route('admin.packageprofile.index') }}">
                                    <i class="livicon" data-name="sitemap" data-size="18" data-c="#00bc8c" data-hc="#00bc8c"
                                       data-loop="true"></i>
                                    <span class="title">Package Profile</span>
                                </a>
                            </li>
                            
                            <!-- <li {!! ((Request::is('admin/school') || Request::is('admin/school/*')) ? 'class="active"' : '') !!}>
                                <a href="{{ route('admin.school.index') }}">
                                    <i class="livicon" data-name="skype" data-size="18" data-c="#EF6F6C" data-hc="#EF6F6C"
                                       data-loop="true"></i>
                                    <span class="title">School Management</span>
                                </a>
                            </li>
                            <li {!! ((Request::is('admin/grade') || Request::is('admin/grade/*')) ? 'class="active"' : '') !!}>
                                <a href="{{ route('admin.grade.index') }}">
                                    <i class="livicon" data-name="sitemap" data-size="18" data-c="#00bc8c" data-hc="#00bc8c"
                                       data-loop="true"></i>
                                    <span class="title">Grade Management</span>
                                </a>
                            </li>
                            <li {!! ((Request::is('admin/student') || Request::is('admin/student/*')) ? 'class="active"' : '') !!}>
                                <a href="{{ route('admin.student.index') }}">
                                    <i class="livicon" data-name="user" data-size="18" data-c="#418BCA" data-hc="#418BCA"
                                       data-loop="true"></i>
                                    <span class="title">Student Management</span>
                                </a>
                            </li> -->
                            
<!--                            <li {!! ((Request::is('admin/job') || Request::is('admin/job/*')) ? 'class="active"' : '') !!}>
                                <a href="{{ route('admin.job.index') }}">
                                    <i class="livicon" data-name="folder-open" data-size="18" data-c="#00bc8c" data-hc="#00bc8c"
                                       data-loop="true"></i>
                                    <span class="title">Prepay Job</span>
                                </a>
                            </li>-->
                            <li {!! ((Request::is('admin/prepayjob') || Request::is('admin/prepayjob/*')) ? 'class="active"' : '') !!}>
                                <a href="{{ route('admin.prepayjob.index') }}">
                                    <i class="livicon" data-name="folder-open" data-size="18" data-c="#00bc8c" data-hc="#00bc8c"
                                       data-loop="true"></i>
                                    <span class="title">Prepay Job</span>
                                </a>
                            </li>
                             <li {!! ((Request::is('admin/miscellaneousjob') || Request::is('admin/miscellaneousjob/*')) ? 'class="active"' : '') !!}>
                                <a href="{{ route('admin.miscellaneousjob.index') }}">
                                    <i class="livicon" data-name="job" data-size="18" data-c="#00bc8c" data-hc="#00bc8c"
                                       data-loop="true"></i>
                                    <span class="title">Miscellaneous Job</span>
                                </a>
                            </li>
                            <li {!! ((Request::is('admin/reorderjob') || Request::is('admin/reorderjob/*')) ? 'class="active"' : '') !!}>
                                <a href="{{ route('admin.reorderjob.index') }}">
                                    <i class="livicon" data-name="folder-open" data-size="18" data-c="#00bc8c" data-hc="#00bc8c"
                                       data-loop="true"></i>
                                    <span class="title">Reorder Job</span>
                                </a>
                            </li>
                            <li {!! ((Request::is('admin/message') || Request::is('admin/message/*'))? 'class="active"' : '') !!}>
                                <a href="{{ route('admin.message.index') }}">
                                    <i class="livicon" data-name="mail" data-size="18" data-c="#F89A14" data-hc="#F89A14"
                                       data-loop="true"></i>
                                    <span class="title">Message Box</span>
                                </a>
                            </li>
                           <li {!! ((Request::is('admin/customer') || Request::is('admin/customer/*'))? 'class="active"' : '') !!}>
                                <a href="{{ route('admin.customer.index') }}">
                                    <i class="livicon" data-name="user" data-size="18" data-c="#F89A14" data-hc="#F89A14"
                                       data-loop="true"></i>
                                    <span class="title">Customer</span>
                                </a>
                            </li>
                            <li {!! ((Request::is('admin/fotolink') || Request::is('admin/fotolink/*'))? 'class="active"' : '') !!}>
                                <a href="{{ route('admin.message.index') }}">
                                    <i class="livicon" data-name="image" data-size="18" data-c="#F89A14" data-hc="#F89A14"
                                       data-loop="true"></i>
                                    <span class="title">Fotolink</span>
                                </a>
                            </li>
                            <li {!! ((Request::is('admin/family') || Request::is('admin/family/*'))? 'class="active"' : '') !!}>
                                <a href="{{ route('admin.message.index') }}">
                                    <i class="livicon" data-name="users" data-size="18" data-c="#F89A14" data-hc="#F89A14"
                                       data-loop="true"></i>
                                    <span class="title">Family</span>
                                </a>
                            </li>
                            <li {!! ((Request::is('admin/admin') || Request::is('admin/admin/*'))? 'class="active"' : '') !!}>
                                <a href="{{ route('admin.message.index') }}">
                                    <i class="livicon" data-name="lock" data-size="18" data-c="#F45911" data-hc="#F45911"
                                       data-loop="true"></i>
                                    <span class="title">Admin</span>
                                </a>
                            </li>
                            <li {!! ((Request::is('admin/sale') || Request::is('admin/sale/*'))? 'class="active"' : '') !!}>
                                <a href="{{ route('admin.message.index') }}">
                                    <i class="livicon" data-name="money" data-size="18" data-c="#F45911" data-hc="#F45911"
                                       data-loop="true"></i>
                                    <span class="title">Sales</span>
                                </a>
                            </li>
                            <li {!! ((Request::is('admin/contact') || Request::is('admin/contact/*')) ? 'class="active"' : '') !!}>
                                <a href="{{ route('admin.contact.create') }}">
                                    <i class="livicon" data-name="phone" data-size="18" data-c="#F45911" data-hc="#F45911"
                                       data-loop="true"></i>
                                    <span class="title">Contact Management</span>
                                </a>
                            </li>
                              <li {!! ((Request::is('admin/profile') || Request::is('admin/profile/*')) ? 'class="active"' : '') !!}>
                                <a href="{{ route('admin.profile.index') }}">
                                        <i data-s="18" data-name="user" class="livicon" id="livicon-40" style="width: 18px; height: 18px;"></i>
                                        My Profile
                                    </a>
                            </li>
                            <li {!! ((Request::is('admin/logout') || Request::is('admin/logout/*')) ? 'class="active"' : '') !!}>
                                <a href="{{route('admin.logout')}}">
                                        <i data-s="18" data-name="sign-out" class="livicon" id="livicon-29" style="width: 18px; height: 18px;"></i>
                                        <span class="title">Logout</span>
                                    </a>
                            </li>
                          
                          
                            
                            <!-- <li {!! ((Request::is('admin/image') || Request::is('admin/image/*')) ? 'class="active"' : '') !!}>
                                <a href="{{ route('admin.image.index') }}">
                                    <i class="livicon" data-name="brush" data-size="18" data-c="#F89A14" data-hc="#F89A14"
                                       data-loop="true"></i>
                                    <span class="title">Image Management</span>
                                </a>
                            </li>
                            <li {!! ((Request::is('admin/item') || Request::is('admin/item/*')) ? 'class="active"' : '') !!}>
                                <a href="{{ route('admin.item.index') }}">
                                    <i class="livicon" data-name="italic" data-size="18" data-c="#00bc8c" data-hc="#00bc8c"
                                       data-loop="true"></i>
                                    <span class="title">Item Management</span>
                                </a>
                            </li> -->
                            
                            <!-- <li {!! ((Request::is('admin/uploadImage') || Request::is('admin/uploadImage/*')) ? 'class="active"' : '') !!}>
                                <a href="{{ route('admin.uploadImage.index') }}">
                                    <i class="livicon" data-name="phone" data-size="18" data-c="#F89A14" data-hc="#F89A14"
                                       data-loop="true"></i>
                                    <span class="title">Upload Image Management</span>
                                </a>
                            </li> -->
                        </ul>                        
                        <!-- END SIDEBAR MENU -->
                    </div>
                </section>
            </aside>            
            <aside class="right-side">
                <!-- Notifications -->
                <!-- Content -->
                @yield('content')

            </aside>
            <!-- right-side -->
        </div>
        <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Return to top"
           data-toggle="tooltip" data-placement="left">
            <i class="livicon" data-name="plane-up" data-size="18" data-loop="true" data-c="#fff" data-hc="white"></i>
        </a>        
        <!-- global js -->        
        @if (Request::is('admin/form_builder2') || Request::is('admin/gridmanager') || Request::is('admin/portlet_draggable') || Request::is('admin/calendar'))
        <script src="{{ asset('public/assets/vendors/form_builder1/js/jquery.ui.min.js') }}"></script>
        @endif
        <script src="{{ asset('public/assets/js/bootstrap.min.js') }}" type="text/javascript"></script>
        <!--livicons-->
        <script src="{{ asset('public/assets/vendors/livicons/minified/raphael-min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('public/assets/vendors/livicons/minified/livicons-1.4.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('public/assets/js/josh.js') }}" type="text/javascript"></script>
        <script src="{{ asset('public/assets/js/metisMenu.js') }}" type="text/javascript"></script>
        <script src="{{ asset('public/assets/vendors/holder-master/holder.js') }}" type="text/javascript"></script>                                  
        <script type="text/javascript">
            $(document).ready(function(){                
                setTimeout(function(){
                    $(".flash-message").hide();
                },15000);                
                $(".nav_icons").remove();                
            });
        </script>
        <!-- end of global js -->
        <!-- begin page level js -->        
        @yield('footer_scripts')
        <!-- end page level js -->
    </body>
</html>
