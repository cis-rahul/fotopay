@extends('layouts/master')
{{-- Page title --}}
@section('title')
Admin Dashboard
@parent
@stop
{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/vendors/x-editable/css/bootstrap-editable.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/vendors/magnifier/css/bootstrap-magnify.min.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/vendors/iCheck/skins/all.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/css/pages/user_profile.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/vendors/datatables/extensions/ColReorder/css/colReorder.bootstrap.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/vendors/datatables/extensions/Scroller/css/scroller.bootstrap.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/vendors/datatables/extensions/RowReorder/css/rowReorder.bootstrap.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/vendors/datatables/extensions/TableTools/css/dataTables.tableTools.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/vendors/datatables/extensions/Responsive/css/responsive.dataTables.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/vendors/datatables/extensions/bootstrap/dataTables.bootstrap.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/css/pages/tables.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/vendors/multiselect/css/bootstrap-multiselect.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/css/bootstrap-datetimepicker.css') }}"/>
<input type="hidden" id="csrf_token" name="csrf_token" value="{{ csrf_token() }}"/>
<style type="text/css">
    #inactive_user_table_filter .input-sm, #inactive_user_table2_filter .input-sm, #inactive_user_table3_filter .input-sm, #pending_task_filter .input-sm{
        width: 85px !important;
    }
    .paging_numbers, .dataTables_info {        
        text-align: center !important;
    }
</style>
@stop
{{-- Page content --}}
@section('content')
<?php
$userlist = (session('tab') == "Userlist") ? 'active' : "";
$entity = (session('tab') == "Entity") ? 'active' : "";
if (empty($entity)) {
    $userlist = 'active';
}
?>
<section class="content-header">    
    <h1>Admin Dashboard</h1>
</section>
<section class="content">
    <div  class="row ">
        <div class="col-md-12">
            <div class="row ">                
                <div class="col-md-12">
                    <ul class="nav nav-tabs ul-edit responsive">                        
                        <li class="{{$userlist}}">
                            <a href="#tab-userlist" data-toggle="tab">
                                User List
                            </a>
                        </li>
                        <li class="{{$entity}}">
                            <a href="#tab-entity" data-toggle="tab">
                                Entity
                            </a>
                        </li>                        
                    </ul>                    
                    <div class="tab-content">
                        <div id="tab-userlist" class="tab-pane fade in {{$userlist}}">
                            @if (session('flash_alert_notice'))
                            <div class="flash-message">
                                <div class="alert alert-{{session('flash_action')}}">
                                    <p>{{session('flash_alert_notice')}}</p>
                                </div>
                            </div>
                            @endif
                            <?php if (!empty(Session::get('role_id')) && Session::get('role_id') == '99') { ?>        
                                <div class="row top10">                                    
                                    <div class="col-md-4 col-sm-4">
                                        <div class="panel panel-border">                    
                                            <div class="panel-heading">
                                                <h3 class="panel-title">
                                                    <i class="livicon" data-name="user" data-size="20" data-loop="true" data-c="#F89A14"
                                                       data-hc="#F89A14"></i>
                                                    Inactive User
                                                </h3>
                                            </div>
                                            <div class="panel-body">
                                                <div class="portlet box primary">                                                    
                                                    <div class="portlet-body">                                                        
                                                        <table class="table table-hover" id="inactive_user_table">
                                                            <thead>
                                                                <tr> 
                                                                    <td>Id</td>
                                                                    <td>User Name</td>
                                                                    <td>Email</td>                                                
                                                                </tr>
                                                            </thead>                                        
                                                            <tbody>                                                                    
                                                                @foreach($inactiveUserList as $objinactiveUserList)
                                                                <tr>
                                                                    <?php
                                                                    $email = "";
                                                                    $username = "";
                                                                    if (!empty($objinactiveUserList->email)) {
                                                                        $emaillen = strlen($objinactiveUserList->email);
                                                                        $email = $objinactiveUserList->email;
                                                                        if ($emaillen > 22) {
                                                                            $email = substr($objinactiveUserList->email, 0, 19) . "...";
                                                                        }
                                                                    }
                                                                    if (!empty($objinactiveUserList->username)) {
                                                                        $usernamelen = strlen($objinactiveUserList->username);
                                                                        $username = $objinactiveUserList->username;
                                                                        if ($usernamelen > 15) {
                                                                            $username = substr($objinactiveUserList->username, 0, 12) . "...";
                                                                        }
                                                                    }
                                                                    ?>
                                                                    <td>{{$objinactiveUserList->id}}</td>
                                                                    <td><a href="{{route('userprofile.edit', $objinactiveUserList->id)}}" id="client_login" class="username" url="{{route('user_info', $objinactiveUserList->id)}}">{{$username}}</a></td>
                                                                    <td>{{$email}}</td>                                                
                                                                </tr>                                                                    
                                                                @endforeach
                                                            </tbody>
                                                        </table>                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                        <div class="panel panel-border">                    
                                            <div class="panel-heading">
                                                <h3 class="panel-title">
                                                    <i class="livicon" data-name="user" data-size="20" data-loop="true" data-c="#F89A14"
                                                       data-hc="#F89A14"></i>
                                                    Active User
                                                </h3>
                                            </div>
                                            <div class="panel-body">
                                                <div class="portlet box primary">                                                    
                                                    <div class="portlet-body">
                                                        <table class="table table-hover" id="inactive_user_table2">
                                                            <thead>
                                                                <tr>
                                                                    <td>Id</td>
                                                                    <td>User Name</td>
                                                                    <td>Email</td>                                                
                                                                </tr>
                                                            </thead>
                                                            <tbody>                                                                
                                                                @foreach($activeUserList as $objactiveUserList)
                                                                <tr>
                                                                    <?php
                                                                    if (!empty($objactiveUserList->email)) {
                                                                        $emaillen = strlen($objactiveUserList->email);
                                                                        $email = $objactiveUserList->email;
                                                                        if ($emaillen > 22) {
                                                                            $email = substr($objactiveUserList->email, 0, 19) . "...";
                                                                        }
                                                                    }
                                                                    if (!empty($objactiveUserList->username)) {
                                                                        $usernamelen = strlen($objactiveUserList->username);
                                                                        $username = $objactiveUserList->username;
                                                                        if ($usernamelen > 15) {
                                                                            $username = substr($objactiveUserList->username, 0, 12) . "...";
                                                                        }
                                                                    }
                                                                    ?>
                                                                    <td>{{$objactiveUserList->id}}</td>
                                                                    <td><a href="{{route('userprofile.edit', $objactiveUserList->id)}}" class="username" url="{{route('user_info', $objactiveUserList->id)}}">{{$username}}</a></td>
                                                                    <td>{{$email}}</td>                                                
                                                                </tr>                                                                
                                                                @endforeach
                                                            </tbody>
                                                        </table>                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                        <div class="panel panel-border">                    
                                            <div class="panel-heading">
                                                <h3 class="panel-title">
                                                    <i class="livicon" data-name="user" data-size="20" data-loop="true" data-c="#F89A14"
                                                       data-hc="#F89A14"></i>
                                                    Deleted User
                                                </h3>
                                            </div>
                                            <div class="panel-body">
                                                <div class="portlet box primary">                                                    
                                                    <div class="portlet-body">                                                        
                                                        <table class="table table-hover" id="inactive_user_table3">
                                                            <thead>
                                                                <tr>
                                                                    <td>Id</td>
                                                                    <td>User Name</td>
                                                                    <td>Email</td>                                                
                                                                </tr>
                                                            </thead>                                        

                                                            <tbody>                                                                
                                                                @foreach($deletedUserList as $objdeletedUserList)
                                                                <tr>
                                                                    <?php
                                                                    $email = "";
                                                                    $username = "";
                                                                    if (!empty($objdeletedUserList->email)) {
                                                                        $emaillen = strlen($objdeletedUserList->email);
                                                                        $email = $objdeletedUserList->email;
                                                                        if ($emaillen > 22) {
                                                                            $email = substr($objdeletedUserList->email, 0, 19) . "...";
                                                                        }
                                                                    }
                                                                    if (!empty($objdeletedUserList->username)) {
                                                                        $usernamelen = strlen($objdeletedUserList->username);
                                                                        $username = $objdeletedUserList->username;
                                                                        if ($usernamelen > 15) {
                                                                            $username = substr($objdeletedUserList->username, 0, 12) . "...";
                                                                        }
                                                                    }
                                                                    ?>
                                                                    <td>{{$objdeletedUserList->id}}</td>
                                                                    <td><a href="{{route('userprofile.edit', $objdeletedUserList->id)}}" class="username" url="{{route('user_info', $objdeletedUserList->id)}}">{{$username}}</a></td>
                                                                    <td>{{$email}}</td>                                                
                                                                </tr>                                                                
                                                                @endforeach
                                                            </tbody>
                                                        </table>                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>      
                            <?php } ?>
                        </div>
                        <div id="tab-entity" class="tab-pane fade in {{$entity}}">
                            <?php if (!empty(Session::get('role_id')) && Session::get('role_id') == '99') { ?>
                                <div class="row top10">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="panel panel-border">                    
                                            <div class="panel-heading">
                                                <h3 class="panel-title">
                                                    <i class="livicon" data-name="user" data-size="20" data-loop="true" data-c="#F89A14"
                                                       data-hc="#F89A14"></i>
                                                    Entity
                                                </h3>
                                            </div>
                                            <div class="panel-body">
                                                <div class="portlet box primary">                                                    
                                                    <div class="portlet-body">                                                        
                                                        <table class="table table-hover" id="inactive_user_table4">
                                                            <thead>
                                                                <tr>                                                
                                                                    <td>User Name</td>
                                                                    <td>Entity Name</td>
                                                                    <td>Email</td>
                                                                </tr>
                                                            </thead>
                                                            <tbody>                                                                
                                                                @foreach($entityList as $objUser)
                                                                <tr>
                                                                    <?php
                                                                    $email = "";
                                                                    $username = "";
                                                                    if (!empty($objUser->email)) {
                                                                        $emaillen = strlen($objUser->email);
                                                                        $email = $objUser->email;
                                                                        if ($emaillen > 22) {
                                                                            $email = substr($objUser->email, 0, 19) . "...";
                                                                        }
                                                                    }
                                                                    if (!empty($objUser->username)) {
                                                                        $usernamelen = strlen($objUser->username);
                                                                        $username = $objUser->username;
                                                                        if ($usernamelen > 15) {
                                                                            $username = substr($objUser->username, 0, 12) . "...";
                                                                        }
                                                                    }
                                                                    ?>                                                
                                                                    <td><a href="javascript:void(0)" class="client_login" entity_id="{{$objUser->entity_id}}">{{$username}}</a></td>
                                                                    <td>{{$objUser->entity_name}}</td>
                                                                    <td>{{$email}}</td>
                                                                </tr>                                                                
                                                                @endforeach
                                                            </tbody>
                                                        </table>                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                    
                                </div>      
                            <?php } ?>
                        </div>                                               
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div id="myModalTask" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Task</h4>
            </div>                                    
            <form id="frm_task" action="{{route('task.store')}}" method="POST" role="form" enctype="multipart/form-data">
                <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}" />                                
                <input type="hidden" name="associated_id" id="associated_id" class="token" value="{{Session::get('bus_id')}}" />
                <input type="hidden" name="assoc_id" id="assoc_id" class="token" value="3" />
                <div class="modal-body">                      
                    <div id="task_validation_msg" name="task_validation_msg"></div>
                    <div class="row uploaddoc">
                        <div class="col-xs-12 col-md-12 col-sm-12">                                
                            <div class="form-group {{ $errors->first('assign_to_user', 'has-error') }}">                                            
                                <label class="control-label">Assigned to User</label>                                
                                <div class="input-group  col-md-12 col-sm-12">                                    
                                    <select id="assign_to_user" name="assign_to_user" class="form-control">
                                        <option value="">Please Select</option>
                                        @if(!empty($bus_users))
                                        @foreach($bus_users as $objBusUser)
                                        <option value="{{$objBusUser->userbus_user_id}}">{{$objBusUser->fullname}}</option>
                                        @endforeach
                                        @endif
                                    </select> 
                                </div>
                                {!! $errors->first('assign_to_user', '<span class="help-block">:message</span>') !!}                                                        
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">                                                        
                            <div class="form-group required {{ $errors->first('task_desc', 'has-error') }}">                                            
                                <label class="control-label">Description</label>
                                <div class="input-group  col-md-12 col-sm-12">
                                    <textarea id="task_desc" name="task_desc" class="form-control" rows="2" style="resize: none;"></textarea>
                                </div>
                                {!! $errors->first('task_desc', '<span class="help-block">:message</span>') !!}                                                                                      
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">                                                        
                            <div class="form-group required {{ $errors->first('start_date_time', 'has-error') }}">                                            
                                <label class="control-label">Start Date and Time</label>
                                <div class="input-group  col-md-12 col-sm-12">
                                    <input type="text" class="form-control" id="start_date_time" name="start_date_time" placeholder="Start Date and time"/>
                                    <p style="color:grey;font-size: 11px;margin-left: 10px;">e.g. "yyyy-mm-dd hh:mm" on a 24hour clock</p>
                                </div>
                                {!! $errors->first('start_date_time', '<span class="help-block">:message</span>') !!}                                                                                      
                            </div>
                        </div>
                    </div>                                                                                                                
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">                                                        
                            <div class="form-group {{ $errors->first('location', 'has-error') }}">                                            
                                <label>Location</label>
                                <div class="input-group  col-md-12 col-sm-12">
                                    <input type="text" class="form-control" id="location" name="location" placeholder="Location"/>
                                    <p style="color:grey;font-size: 11px;margin-left: 10px;">e.g. "George" or "Western Cape" or "RSA."</p>
                                </div>

                                {!! $errors->first('location', '<span class="help-block">:message</span>') !!}                                                                                      
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">                                                        
                            <div class="form-group {{ $errors->first('priority', 'has-error') }}">                                            
                                <label>Priority</label>
                                <div class="input-group  col-md-12 col-sm-12">
                                    <input type="radio" id="priority1" name="priority" value="1" selected="selected"/> High
                                    <input type="radio" id="priority2" name="priority" value="2"/> Medium
                                    <input type="radio" id="priority3" name="priority" value="3"/> Low
                                </div>
                                {!! $errors->first('priority', '<span class="help-block">:message</span>') !!}                                                                                      
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">                                                        
                            <div class="form-group required {{ $errors->first('event_status', 'has-error') }}">                                            
                                <label class="control-label">Event Status</label>
                                <div class="input-group  col-md-12 col-sm-12">                                    
                                    <input type="radio" id="event_status1" name="event_status" value="0" selected="selected"/> Pending
                                    <input type="radio" id="event_status2" name="event_status" value="1"/> Done
                                    <input type="radio" id="event_status3" name="event_status" value="2" disabled="disabled"/> Repeat
                                </div>
                                {!! $errors->first('event_status', '<span class="help-block">:message</span>') !!}                                                                                      
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" type="submit" id="btn_upload_document" name="btn_upload_document">Upload</button>                
                </div>
            </form>
        </div>        
    </div>
</div>
<div id="myModalTaskEdit" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Task</h4>
            </div>                                    
            <form id="frm_task_edit" action="{{route('update_task')}}" method="POST" role="form" enctype="multipart/form-data">
                <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}" />                                
                <input type="hidden" name="task_id" id="task_id" value="" />
                <input type="hidden" name="redirect_url" id="redirect_url" value="dashboard" />
                <input type="hidden" name="associated_id" id="associated_id" class="token" value="{{Session::get('bus_id')}}" />
                <input type="hidden" name="assoc_id" id="assoc_id" class="token" value="3" />
                <div class="modal-body">                      
                    <div id="task_validation_msg" name="task_validation_msg"></div>
                    <div class="row uploaddoc">
                        <div class="col-xs-12 col-md-12 col-sm-12">                                
                            <div class="form-group {{ $errors->first('assign_to_user', 'has-error') }}">                                            
                                <label class="control-label">Assigned to User</label>                                
                                <div class="input-group  col-md-12 col-sm-12">                                    
                                    <select id="assign_to_user" name="assign_to_user" class="form-control" readonly="readonly">
                                        <option value="">Please Select</option>
                                        @if(!empty($bus_users))
                                        @foreach($bus_users as $objBusUser)
                                        <option value="{{$objBusUser->userbus_user_id}}">{{$objBusUser->fullname}}</option>
                                        @endforeach
                                        @endif
                                    </select> 
                                </div>
                                {!! $errors->first('assign_to_user', '<span class="help-block">:message</span>') !!}                                                        
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">                                                        
                            <div class="form-group required {{ $errors->first('task_desc', 'has-error') }}">                                            
                                <label class="control-label">Description</label>
                                <div class="input-group  col-md-12 col-sm-12">
                                    <textarea id="task_desc" name="task_desc" class="form-control" rows="2" style="resize: none;" readonly="readonly"></textarea>
                                </div>
                                {!! $errors->first('task_desc', '<span class="help-block">:message</span>') !!}                                                                                      
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">                                                        
                            <div class="form-group required {{ $errors->first('start_date_time', 'has-error') }}">                                            
                                <label class="control-label">Start Date and Time</label>
                                <div class="input-group  col-md-12 col-sm-12">
                                    <input type="text" class="form-control" id="start_date_time" name="start_date_time" placeholder="Start Date and time" readonly="readonly"/>
                                    <p style="color:grey;font-size: 11px;margin-left: 10px;">e.g. "yyyy-mm-dd hh:mm" on a 24hour clock</p>
                                </div>
                                {!! $errors->first('start_date_time', '<span class="help-block">:message</span>') !!}                                                                                      
                            </div>
                        </div>
                    </div>                                                                                                                
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">                                                        
                            <div class="form-group {{ $errors->first('location', 'has-error') }}">                                            
                                <label>Location</label>
                                <div class="input-group  col-md-12 col-sm-12">
                                    <input type="text" class="form-control" id="location" name="location" placeholder="Location" disabled="disabled"/>
                                    <p style="color:grey;font-size: 11px;margin-left: 10px;">e.g. "George" or "Western Cape" or "RSA."</p>
                                </div>

                                {!! $errors->first('location', '<span class="help-block">:message</span>') !!}                                                                                      
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">                                                        
                            <div class="form-group {{ $errors->first('priority', 'has-error') }}">                                            
                                <label>Priority</label>
                                <div class="input-group  col-md-12 col-sm-12">
                                    <input type="radio" id="priority1" name="priority" value="1" disabled="disabled"/> High
                                    <input type="radio" id="priority2" name="priority" value="2" disabled="disabled"/> Medium
                                    <input type="radio" id="priority3" name="priority" value="3" disabled="disabled"/> Low
                                </div>
                                {!! $errors->first('priority', '<span class="help-block">:message</span>') !!}                                                                                      
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12">                                                        
                            <div class="form-group required {{ $errors->first('event_status', 'has-error') }}">                                            
                                <label class="control-label">Event Status</label>
                                <div class="input-group  col-md-12 col-sm-12">                                    
                                    <input type="radio" id="event_status0" name="event_status" value="0"/> Pending
                                    <input type="radio" id="event_status1" name="event_status" value="1"/> Done
                                    <input type="radio" id="event_status2" name="event_status" value="2" disabled="disabled"/> Repeat
                                </div>
                                {!! $errors->first('event_status', '<span class="help-block">:message</span>') !!}                                                                                      
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" type="submit" id="btn_upload_document" name="btn_upload_document">Upload</button>                
                </div>
            </form>
        </div>        
    </div>
</div>
<div id="myModalRegistration" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Client Registration</h4>
            </div>

            <div class="modal-body">
                <form action="{{route('signup')}}" method="POST">
                    <!-- CSRF Token -->
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />                    
                    <div class="form-group {{ $errors->first('username', 'has-error') }}">
                        <label class="sr-only"> Username</label>
                        <input type="text" class="form-control" id="username" name="username" placeholder="Username" value="{!! Input::old('username') !!}" required>
                        {!! $errors->first('username', '<span class="help-block">:message</span>') !!}
                    </div>
                    <div class="form-group {{ $errors->first('email', 'has-error') }}">
                        <label class="sr-only"> Email</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{!! Input::old('email') !!}" required>
                        {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                    </div>
                    <div class="form-group {{ $errors->first('password', 'has-error') }}">
                        <label class="sr-only"> Password</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
                        {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
                    </div>
                    <div class="form-group {{ $errors->first('password_confirm', 'has-error') }}">
                        <label class="sr-only"> Confirm Password</label>
                        <input type="password" class="form-control" id="password_confirmation" name="password_confirmation"
                               placeholder="Confirm Password" required>
                        {!! $errors->first('password_confirm', '<span class="help-block">:message</span>') !!}
                    </div>
                    @if(!empty(Session::get('bus_id')) && !empty(Session::get('entity_id')))
                    <div class="checkbox">
                        <label class="sr-only"> Status</label>
                        <input type='radio' name='user_status' value='1' checked="checked"/> Active
                        <input type='radio' name='user_status' value='0'/> Inactive
                        {!! $errors->first('password_confirm', '<span class="help-block">:message</span>') !!}
                    </div>
                    @endif
                    <input type="submit" class="btn btn-block btn-primary" value="Registration" name="submit">
                    <!--                    Already have an account? Please <a href="{{route('signin')}}"> Sign In</a>-->
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div id="myModalRegShow" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Registered Client </h4>
            </div>            
            <div class="modal-body">
                <form action="{{route('update_manage_user_status')}}" method="POST">
                    <input type="hidden" id="user_id" name="user_id"/>
                    <input type="hidden" id="entity_id" name="entity_id"/>
                    <input type="hidden" id="_token" name="_token" value="{{csrf_token()}}"/>
                    <input type="hidden" id="email" name="email" value=""/>
                    <input type="hidden" id="page" name="page" value="admin"/>
                    <!-- CSRF Token -->                    
                    <div class="form-group {{ $errors->first('username', 'has-error') }}">
                        <label class="sr-only"> Username</label>
                        <span class="form-control" id="username" placeholder="Username"></span>                        
                    </div>
                    <div class="form-group {{ $errors->first('user_email', 'has-error') }}">
                        <label class="sr-only"> Email</label>
                        <input type="email" class="form-control" id="user_email" name="user_email" placeholder="Email"/>
                    </div>
                    <div class="form-group {{ $errors->first('user_pass', 'has-error') }}">
                        <label class="sr-only"> Email</label>
                        <input type="password" class="form-control" id="user_pass" name="user_pass" placeholder="Password"/>
                    </div>
                    <div class="checkbox">
                        <label class="sr-only"> Status</label>
                        <input type='radio' name='user_status' id="active" value='1' checked="checked"/> Active
                        <input type='radio' name='user_status' id="inactive" value='0'/> Inactive                        
                    </div>                    
                    <input type="submit" class="btn btn-block btn-primary" value="Update" name="submit">
                    <!--                    Already have an account? Please <a href="{{route('signin')}}"> Sign In</a>-->
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@stop
{{-- page level scripts --}}
@section('footer_scripts')
<script type="text/javascript" src="{{ asset('public/assets/vendors/multiselect/js/bootstrap-multiselect.js') }}" ></script>
<script type="text/javascript" src="{{ asset('public/assets/js/bootstrap-datepicker.js')}}" ></script>
<script type="text/javascript" src="{{ asset('public/assets/js/moment-with-locales.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/assets/js/bootstrap-datetimepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/assets/vendors/datatables/js/jquery.dataTables.min.js') }}" ></script>
<script type="text/javascript" src="{{ asset('public/assets/vendors/datatables/extensions/TableTools/js/dataTables.tableTools.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/assets/vendors/datatables/extensions/Scroller/js/dataTables.scroller.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/assets/vendors/datatables/extensions/RowReorder/js/dataTables.rowReorder.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/assets/vendors/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/assets/vendors/datatables/extensions/bootstrap/dataTables.bootstrap.min.js') }}" ></script>
<script type="text/javascript" src="{{ asset('public/assets/vendors/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js') }}"></script>

<script type="text/javascript">
$(function() {
    $("#inactive_user_table").dataTable({
        "dom": ' <"search"f><"top"l>rt<"bottom"ip><"clear">',
        "pageLength": 10,
        'pagingType': 'numbers',
        "columnDefs": [
            {
                "targets": [0],
                "visible": false,
                "searchable": false
            },
        ],
        "order": [0, 'desc'],
    });
    $("#inactive_user_table2").dataTable({
        "dom": ' <"search"f><"top"l>rt<"bottom"ip><"clear">',
        "pageLength": 10,
        'pagingType': 'numbers',
        "columnDefs": [
            {
                "targets": [0],
                "visible": false,
                "searchable": false
            },
        ],
        "order": [0, 'desc']
    });
    $("#inactive_user_table3").dataTable({
        "dom": ' <"search"f><"top"l>rt<"bottom"ip><"clear">',
        "pageLength": 10,
        'pagingType': 'numbers',
        "columnDefs": [
            {
                "targets": [0],
                "visible": false,
                "searchable": false
            },
        ],
        "order": [0, 'desc']
    });
    $("#inactive_user_table4").dataTable({
        "dom": ' <"search"f><"top"l>rt<"bottom"ip><"clear">',
        "pageLength": 10,
        "order": [0, 'desc'],
        'pagingType': 'numbers',
    });
    $(".client_login").click(function() {
        var url = "{{route('get_manage_user_byid')}}";
        var entity_id = $(this).attr('entity_id');
        var csrf_token = "{{csrf_token()}}";
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'JSON',
            data: ({_token: csrf_token, entity_id: entity_id}),
            success: function(res) {
                if (res['status'] == true) {
                    $("#myModalRegShow #user_id").val(res['data']['userbus_user_id']);
                    $("#myModalRegShow #entity_id").val(entity_id);
                    $("#myModalRegShow #username").html(res['data']['username']);
                    $("#myModalRegShow #user_email").val(res['data']['email']);
                    $("#myModalRegShow #email").val(res['data']['email']);
                    $("#myModalRegShow #user_pass").val("");
                    if (res['data']['status'] == '1') {
                        $("#myModalRegShow #active").prop('checked', true);
                        $("#myModalRegShow #inactive").prop('checked', false);
                    } else {
                        $("#myModalRegShow #active").prop('checked', false);
                        $("#myModalRegShow #inactive").prop('checked', true);
                    }
                    $("#myModalRegShow").modal('show');
                } else {
                    $("#myModalRegistration").modal('show');
                }
            },
            error: function(res) {
                alert('error');
            }
        });
    });
});
</script>
<style type="text/css">
    #pending_task_wrapper{
        margin-top: 10px;
    }
    .additional_search{
        text-align: right;
    }
</style>
@stop
