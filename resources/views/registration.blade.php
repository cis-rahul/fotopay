<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Register | Collectit</title>
        <!--global css starts-->
        <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/css/bootstrap.min.css') }}">
        <link rel="shortcut icon" href="{{ asset('public/assets/images/favicon.png') }}" type="image/x-icon">
        <link rel="icon" href="{{ asset('public/assets/images/favicon.png') }}" type="image/x-icon">
        <!--end of global css-->
        <!--page level css starts-->
        <link type="text/css" rel="stylesheet" href="{{asset('public/assets/vendors/iCheck/skins/all.css')}}" />
        <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/css/frontend/register.css') }}">
        <!--end of page level css-->
    </head>
    <body>
        <div class="container">
            <!--Content Section Start -->
            <div class="row">
                <div class="box animation flipInX">
                    <img width="160px;" src="{{ asset('public/assets/images/collectit_logo.png') }}" alt="logo" class="img-responsive mar">
                    <h3 class="text-primary">Register</h3>
                    <!-- Notifications -->                        
                    <form action="{{route('signup')}}" method="POST">
                        <!-- CSRF Token -->
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />                
<!--                        <div class="form-group {{ $errors->first('firstname', 'has-error') }}">
                            <label class="sr-only"> Username</label>
                            <input type="text" class="form-control" id="firstname" name="firstname" placeholder="First Name" value="{!! Input::old('first_name') !!}" required>
                            {!! $errors->first('firstname', '<span class="help-block">:message</span>') !!}
                        </div>
                        <div class="form-group {{ $errors->first('lastname', 'has-error') }}">
                            <label class="sr-only"> Username</label>
                            <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Last Name" value="{!! Input::old('last_name') !!}" required>
                            {!! $errors->first('firstname', '<span class="help-block">:message</span>') !!}
                        </div>-->
                        <div class="form-group {{ $errors->first('username', 'has-error') }}">
                            <label class="sr-only"> Username</label>
                            <input type="text" class="form-control" id="username" name="username" placeholder="Username" value="{!! Input::old('username') !!}" required>
                            {!! $errors->first('username', '<span class="help-block">:message</span>') !!}
                        </div>
                        <div class="form-group {{ $errors->first('email', 'has-error') }}">
                            <label class="sr-only"> Email</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{!! Input::old('email') !!}" required>
                            {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                        </div>
                        <div class="form-group {{ $errors->first('password', 'has-error') }}">
                            <label class="sr-only"> Password</label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
                            {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
                        </div>
                        <div class="form-group {{ $errors->first('password_confirm', 'has-error') }}">
                            <label class="sr-only"> Confirm Password</label>
                            <input type="password" class="form-control" id="password_confirmation" name="password_confirmation"
                                   placeholder="Confirm Password" required>
                            {!! $errors->first('password_confirm', '<span class="help-block">:message</span>') !!}
                        </div>
                        <input type="hidden" id="user_status" name="user_status" value="0"/>
                        <input type="submit" class="btn btn-block btn-primary" value="Sign up" name="submit">
                        Already have an account? Please <a href="{{route('signin')}}"> Sign In</a>
                    </form>
                </div>
            </div>
            <!-- //Content Section End -->
        </div>
        <!--global js starts-->
        <script type="text/javascript" src="{{ asset('public/assets/js/jquery.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('public/assets/js/bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('public/assets/vendors/iCheck/icheck.min.js') }}"></script>
        <!--global js end-->
        <script>
        $(document).ready(function() {
            $("input[type='checkbox'],input[type='radio']").iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });
        });
        </script>
    </body>
</html>
