<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="robots" content="noindex">
<meta name="googlebot" content="noindex">
<title>:: Fotopay ::</title>
<!--fonts css-->
<link href="{{ asset('public/assets/css/frontend/home/fonts.css') }}" rel="stylesheet" type="text/css">
<!--plugin css-->
<link href="{{ asset('public/assets/css/frontend/home/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('public/assets/css/frontend/home/animate.css') }}" rel="stylesheet" type="text/css">

<!--custom css-->
<link href="{{ asset('public/assets/css/frontend/home/style.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('public/assets/css/frontend/home/responsive.css') }}" rel="stylesheet" type="text/css">
<script src="https://use.fontawesome.com/42e88e9aa4.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
@yield('header_styles')
</head>
<body>


<!--wrapper start-->
<div class="wrapper">

    <!--header start-->
    <div class="header">
    
        
        <!--header top start--> 
        <div class="header-top clearfix">
        <div class="container">
        <div class="row">
            <div class="logo">
                <a href="javascript:void(0)"><img src="{{ asset('public/assets/images/logo1.png') }}" alt="" /></a>
            </div>
            
            <div class="menu">
                <a href="javascript:void(0)">Contact Us</a>
            </div>
        </div>
        </div>
        </div>
        <!--header top end--> 
        
        <div class="banner">
        <div class="container">
        <div class="row">
            Nemo enim ipsam <span class="typewriter"> voluptatem</span> quia voluptas sit<br/> aspernatur aut odit aut fugit
        </div>
        </div>
        </div>
        
        <div class="down-arrow">
            <a class="scroll" href="#main-content-box">
                <span>Scroll down to know more</span>
                <img src="{{ asset('public/assets/images/arrow.png') }}" alt="" />
            </a>
        </div>
    
    </div>
    <!--header end-->
    
    
    <!--content start-->
    <div class="content" id="main-content-box">
        @yield('content')

    </div>
    <!--content end-->
    
    <!--footer start-->
    <div class="footer">
    <div class="container">
    <div class="row">
    
        <div class="pull-left copyright-box">
            @  Copyright 2016  |  All rights reserved  |  <a href="javascript:void(0)">Privacy Policy</a>
        </div>
        
        <div class="pull-right">
            <ul class="social-icon">
                <li><a href="javascript:void(0)"><i class="fa fa-facebook"></i></a></li>
                <li><a href="javascript:void(0)"><i class="fa fa-twitter"></i></a></li>
                <li><a href="javascript:void(0)"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="javascript:void(0)"><i class="fa fa-youtube-play"></i></a></li>
            </ul>
        </div>
    
    </div>
    </div>
    </div>
    <!--footer end-->

    

</div>
<!--wrapper end-->


<!--libs js-->

<!--plugin js-->
<script src="{{ asset('public/assets/js/frontend/home/bootstrap.min.js') }}"></script>
<script src="{{ asset('public/assets/js/frontend/home/typewriter.js') }}"></script>
<script src="{{ asset('public/assets/js/frontend/home/wow.min.js') }}"></script>

<!--custom js-->
<script src="{{ asset('public/assets/js/frontend/home/custom.js') }}"></script>

<script>
    /*jQuery(function() {
        jQuery(".typewriter").typewriter({'speed':150});    
    });*/
    
    jQuery(function(){
      jQuery(".typewriter").typed({
        strings: ["First sentence.", "Second sentence."],
        typeSpeed: 150
      });
  });
</script>
@yield('footer_scripts')
</body>
</html>