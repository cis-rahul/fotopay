<!DOCTYPE html>
<html lang="en">
    <head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<title>:: Fotopay ::</title>
	<!--fonts css-->
	<script src="https://use.fontawesome.com/42e88e9aa4.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
	<!--plugin css-->
	<link href="{{ asset('public/assets/css/frontend/home/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('public/assets/css/frontend/home/animate.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('public/assets/css/frontend/home/jquery.mCustomScrollbar.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('public/assets/css/frontend/home/lightbox.css') }}" rel="stylesheet" type="text/css">

	<!--custom css-->
	<link href="{{ asset('public/assets/css/frontend/home/style.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('public/assets/css/frontend/home/responsive.css') }}" rel="stylesheet" type="text/css">
    </head>
    <body>
	<!--wrapper start-->
	<div class="wrapper">
	    <!--header start-->
	    <div class="inner-header">   
		<div class="container">
		    <div class="row">
			<div class="logo">
			    <a href="{{ route('prepay') }}"><img src="{{ asset('public/assets/images/logo1.png') }}" alt="" /></a>
			</div>

			<div class="menu">
			    <a href="javascript:void(0)">Contact Us</a>
			</div>
		    </div>
		</div>
	    </div>
	    @yield('content')

	    <!--footer start-->
	    <div class="footer">
		<div class="container">
		    <div class="row">

			<div class="pull-left copyright-box">
			    @  Copyright 2016  |  All rights reserved  |  <a href="javascript:void(0)">Privacy Policy</a>
			</div>

			<div class="pull-right">
			    <ul class="social-icon">
				<li><a href="javascript:void(0)"><i class="fa fa-facebook"></i></a></li>
				<li><a href="javascript:void(0)"><i class="fa fa-twitter"></i></a></li>
				<li><a href="javascript:void(0)"><i class="fa fa-google-plus"></i></a></li>
				<li><a href="javascript:void(0)"><i class="fa fa-youtube-play"></i></a></li>
			    </ul>
			</div>

		    </div>
		</div>
	    </div>
	    <!--footer end-->
	</div>
	<!--wrapper end-->
	<!--libs js-->
	<!--plugin js-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
	<script src="{{ asset('public/assets/js/frontend/home/bootstrap.min.js') }}"></script>
	<script src="{{ asset('public/assets/js/frontend/home/wow.min.js') }}"></script>
	<script src="{{ asset('public/assets/js/frontend/home/wow.min.js') }}"></script>
	<script src="{{ asset('public/assets/js/frontend/home/lightbox.js') }}"></script>
	<!--custom js-->
	<script src="{{ asset('public/assets/js/frontend/home/custom.js') }}"></script>
	<script type="text/javascript">
            $(document).ready(function(){                
                setTimeout(function(){
                    $(".flash-message").hide();
                },15000);                
                $(".nav_icons").remove();                
            });
        </script>
	<script>
	    $(function() {

		$(".numbers-box").append('<div class="inc button"><i class="fa fa-plus" aria-hidden="true"></i></div><div class="dec button"><i class="fa fa-minus" aria-hidden="true"></i></div>');

		$(".button").on("click", function() {

		    var $button = $(this);
		    var oldValue = $button.parent().find("input").val();

		    if ($button.text() == "+") {
			var newVal = parseFloat(oldValue) + 1;
		    } else {
			// Don't allow decrementing below zero
			if (oldValue > 0) {
			    var newVal = parseFloat(oldValue) - 1;
			} else {
			    newVal = 0;
			}
		    }

		    $button.parent().find("input").val(newVal);

		});

	    });
	</script>
	 @yield('footer_scripts')

    </body>
</html>